!********************************************************************
module ihc
  implicit none
!********************************************************************

  integer,private :: ngrids

  type ptr_to_integer2D
     real, pointer :: arr(:,:)
  end type ptr_to_integer2D

  type ptr_to_real2D
     real, pointer :: arr(:,:)
  end type ptr_to_real2D

  type(ptr_to_integer2D),allocatable,private :: cell_bc(:),ichk(:)
  type(ptr_to_real2D),allocatable,private    :: vold(:),volr(:)

  integer,pointer,private :: jmax(:),kmax(:)
  real,pointer,private    :: x(:,:,:),y(:,:,:)
  integer,pointer,private :: iblank(:,:,:)

  character*40,pointer,private :: bc_file(:)

  integer,allocatable,private :: check_alloc(:)

  real,private :: overlap_factor

  private :: init_ihc,read_bc,boundconnect,recvconnect,holeconnect,metfv, &
             finddonor,find_fractions,bilinearinterp,matrixinv2x2

contains

!********************************************************************
  subroutine init_ihc(jmx,kmx,ngrds,bcfile)
!********************************************************************
    integer :: ngrds
    integer :: jmx(:),kmx(:)
    character*40 :: bcfile(:)

!...local variables
    integer :: n

    ngrids = ngrds

    if(allocated(cell_bc)) deallocate(cell_bc)
    if(allocated(ichk)) deallocate(ichk)
    if(allocated(vold)) deallocate(vold)
    if(allocated(volr)) deallocate(volr)
    if(allocated(check_alloc)) deallocate(check_alloc)

    nullify(jmax,kmax)
    nullify(bc_file)
    nullify(x,y)

    allocate(jmax(ngrids),kmax(ngrids))
    allocate(bc_file(ngrids))

    jmax = jmx
    kmax = kmx
    bc_file = bcfile

    allocate(vold(ngrids),volr(ngrids))
    allocate(cell_bc(ngrids),ichk(ngrids))

    do n = 1,ngrids
      allocate(vold(n)%arr(jmax(n),kmax(n)))
      allocate(volr(n)%arr(jmax(n),kmax(n)))
      allocate(cell_bc(n)%arr(jmax(n),kmax(n)))
      allocate(ichk(n)%arr(jmax(n),kmax(n)))
    enddo

    !overlap_factor = 0.95
    !asitav
    overlap_factor = 0.8

    !allocate(x(jm,km,ngrids),y(jm,km,ngrids))
    !allocate(iblank(jm,km,ngrids))

    allocate(check_alloc(1))

    call read_bc
 
  end subroutine init_ihc

!********************************************************************
  subroutine read_bc
!..bc types
!...1 --> not a receiver 
!...2 --> iblank = 0  
!...3 --> is a receiver 
!...4 --> iblank.ne.0  
!...5 --> wall point 
!********************************************************************
    integer :: j,k,n,ib
    integer :: js,je,ks,ke
    integer :: nbc,ibtyp(25),ibdir(25)
    integer :: jbcs(25),kbcs(25)
    integer :: jbce(25),kbce(25)
    integer :: ibproc(25)

    namelist/ihcbcinp/ nbc,ibtyp,ibdir,jbcs,jbce,kbcs,kbce,ibproc

    do n = 1,ngrids
      cell_bc(n)%arr = 0
    enddo

    do n = 1,ngrids
      open(unit=21,file=bc_file(n),status='unknown')
      read(21,ihcbcinp)
      close(21)

      do ib = 1,nbc
        js = jbcs(ib)
        je = jbce(ib)
        ks = kbcs(ib)
        ke = kbce(ib)
        if(js.lt.0) js = jmax(n) + js + 1
        if(ks.lt.0) ks = kmax(n) + ks + 1
        if(je.lt.0) je = jmax(n) + je + 1
        if(ke.lt.0) ke = kmax(n) + ke + 1

        do j = js,je
        do k = ks,ke
          cell_bc(n)%arr(j,k) = ibtyp(ib)
        enddo
        enddo
      enddo 
    enddo

  end subroutine read_bc

!*********************************************************************
  subroutine do_connectihc(xy,iblnk,jmx,kmx,imesh,idonor,frac,&
              ibc,ndonor,nfringe,iisptr,iieptr,idsize,Njmx,Nkmx, &
              ngrds,init)

    integer,optional :: init
    integer :: ngrds,Njmx,Nkmx,idsize
    integer :: jmx(:),kmx(:)
    integer :: ndonor(:),nfringe(:)
    real,target    :: xy(:,:,:,:)
    integer,target :: iblnk(:,:,:)
    integer :: imesh(:,:,:),idonor(:,:,:)
    integer :: iisptr(:),iieptr(:)
    real    :: frac(:,:,:) 
    integer :: ibc(:,:)
    
    integer :: n,ng,ngd,nf,nd,is,ie
    character*40 :: integer_string

    real,pointer :: x1(:,:,:),y1(:,:,:)
    character*40,allocatable :: bcfile(:)

    x1 => xy(1,:,:,:)
    y1 => xy(2,:,:,:)

    allocate(bcfile(ngrds))

    do n=1,ngrds
      write(integer_string,*) n-1
      integer_string = adjustl(integer_string)
      bcfile(n) = 'ihcbc.'//trim(integer_string)
    enddo

    call connect2d(x1,y1,iblnk,idonor,frac,imesh,ibc,&
              iisptr,iieptr,ndonor,nfringe,jmx,kmx,ngrds,bcfile,init)

  end subroutine do_connectihc

!*********************************************************************
  subroutine connect2d(x1,y1,iblnk,idonor,frac,imesh,ibc,&
              iisptr,iieptr,ndonor,nfringe,jmx,kmx,ngrds,bcfile,init)
    use octree
!********************************************************************

    integer,optional :: init
    integer :: ngrds
    integer :: jmx(:),kmx(:)
    integer :: ndonor(:),nfringe(:)
    integer :: iisptr(:),iieptr(:)
    real,target :: x1(:,:,:),y1(:,:,:)
    integer,target :: iblnk(:,:,:)
    integer :: imesh(:,:,:),idonor(:,:,:)
    real    :: frac(:,:,:) 
    integer :: ibc(:,:)
    character*40 :: bcfile(:)

    integer jk
    integer j,k,j1,k1,m,n,nn,n1
    integer idsize,count,llmin,llmax
    integer,allocatable :: nholept(:),iholept(:,:,:)
    integer,allocatable :: ibctmp(:,:),idontmp(:,:)
    real,allocatable :: diholept(:,:,:)
    logical,allocatable :: ioverlap(:,:)

!...allocate variables under user request

    if(present(init)) then
      if (init.eq.1) call init_ihc(jmx,kmx,ngrds,bcfile)
    endif

!...allocate variables if not initiated yet

    if(.not.allocated(check_alloc)) call init_ihc(jmx,kmx,ngrds,bcfile)
     
!...set pointers

    x => x1 ! x,y set as pointers to avoid additional memory usage
    y => y1 ! have to be careful not to change x,y in any of the
            ! subroutines associated with this module 

    iblank => iblnk ! iblank values do change in this module

!...some allocatation

    idsize = 0
    do n = 1,ngrids
      jk = jmax(n)*kmax(n)
      if(jk.gt.idsize) idsize = jk 
    enddo

    allocate(nholept(ngrids))
    allocate(iholept(idsize,5,ngrids))
    allocate(diholept(idsize,2,ngrids))
    allocate(ibctmp(idsize,ngrids),idontmp(idsize,ngrids))

    allocate(ioverlap(ngrids,ngrids))

!...calculating volume of the cells

    call metfv

!...reassign cell volume based on the bc

    do n = 1,ngrids
      do j = 1,jmax(n)
      do k = 1,kmax(n)
        if (cell_bc(n)%arr(j,k).eq.1) volr(n)%arr(j,k) = 0.
        if (cell_bc(n)%arr(j,k).eq.1) vold(n)%arr(j,k) = 0.
        if (cell_bc(n)%arr(j,k).eq.2) volr(n)%arr(j,k) = -1.e30
        if (cell_bc(n)%arr(j,k).eq.2) vold(n)%arr(j,k) = 1.e30
        if (cell_bc(n)%arr(j,k).eq.3) volr(n)%arr(j,k) = 1.e30
        if (cell_bc(n)%arr(j,k).eq.3) vold(n)%arr(j,k) = 1.e30
        if (cell_bc(n)%arr(j,k).eq.5) volr(n)%arr(j,k) = 0.
        if (cell_bc(n)%arr(j,k).eq.5) vold(n)%arr(j,k) = 0.
      enddo
      enddo
    enddo

!...generate octree data
    llmin = 2
    llmax = 5
    call make_octree(x,y,jmax,kmax,ngrids,llmin,llmax)

!...initialize iblank array

    do n = 1,ngrids
      do j = 1,jmax(n)
      do k = 1,kmax(n)
        iblank(j,k,n) = 1
        if (cell_bc(n)%arr(j,k).eq.2) iblank(j,k,n) = 0
      enddo
      enddo
    enddo

    do n = 1,ngrids
      ichk(n)%arr = 0
    enddo

!...check for overlapping meshes 

    ioverlap = .false.

    do n = 1,ngrids
      call boundconnect(n,ioverlap)
    enddo

!...do connectivity for forced receivers first

    nholept = 0

    do n = 1,ngrids
      call recvconnect(n,ioverlap,nholept,iholept,diholept,idsize)
    enddo

!...do connectivity for all other points

    do n = 1,ngrids
      call holeconnect(n,ioverlap,nholept,iholept,diholept,idsize)
    enddo

!... get connectivity info....
    ndonor = 0
    nfringe = 0

    do n=1,ngrids
     do m=1,nholept(n)
       n1 = iholept(m,3,n)

       j = iholept(m,1,n)
       k = iholept(m,2,n)
       if (iblank(j,k,n).ne.0) then
         j1 = iholept(m,4,n)
         k1 = iholept(m,5,n)
         nfringe(n) = nfringe(n) + 1
         imesh(nfringe(n),1,n) = j
         imesh(nfringe(n),2,n) = k

         ndonor(n1) = ndonor(n1) + 1
         ibctmp(nfringe(n),n) = ndonor(n1)
         idontmp(nfringe(n),n) = n1

         idonor(ndonor(n1),1,n1) = j1
         idonor(ndonor(n1),2,n1) = k1
         frac(ndonor(n1),1,n1) = diholept(m,1,n)
         frac(ndonor(n1),2,n1) = diholept(m,2,n)
         iblank(j,k,n) = -1
       endif
     enddo
    enddo
    
    !global donor cell ptrs
    iisptr = 0
    iieptr = 0
    iisptr(1) = 1
    iieptr(1) = ndonor(1)
    do n=2,ngrids
      iisptr(n) = iisptr(n-1) + ndonor(n-1) 
      iieptr(n) = iisptr(n) + ndonor(n) -1 
    end do 

    do n=1,ngrids
     do j=1,nfringe(n)
       n1 = idontmp(j,n)
       count = 0
       if(n1.gt.1) then
        do nn=2,n1
         count = count+ndonor(nn-1)
        end do
       end if
       ibc(j,n) = count+ibctmp(j,n)
     end do
    end do

  end subroutine connect2d

!********************************************************************
  subroutine boundconnect(ng,ioverlap)
    use octree
!********************************************************************

    integer :: ng
    logical :: ioverlap(ngrids,ngrids)

    integer :: j,k,n,nface
    integer :: lvl,nout
    integer :: jl,kl,js,ks,jstep,kstep

    real    :: xbar(2),xp(2),frac1(2)
    real,allocatable :: xg(:,:,:)

    do n = 1,ngrids

    if (ioverlap(n,ng)) ioverlap(ng,n) = .true.

    if (n.ne.ng.and..not.ioverlap(ng,n)) then
      allocate(xg(jmax(n),kmax(n),2))

      do j = 1,jmax(n)
        do k = 1,kmax(n)
          xg(j,k,1) = x(j,k,n)
          xg(j,k,2) = y(j,k,n)
        enddo
      enddo

      jl = 2; kl = 2
      js = -1; ks = -1

      j = 1; k = 1
      jstep = 1; kstep = 1
      
      nface = 1
      do while (.true.) !loop for all boundary points 

        if (nface.eq.1) j = 1
        if (nface.eq.2) j = jmax(ng)
        if (nface.eq.3) k = 1
        if (nface.eq.4) k = kmax(ng)

        xbar(1) = (x(j,k,ng)-xs(1,n))/scale(1,n)
        xbar(2) = (y(j,k,ng)-xs(2,n))/scale(2,n)

        if (xbar(1).ge.0.and.xbar(2).ge.0.and. &
            xbar(1).lt.1.and.xbar(2).lt.1) then

          xp(1) = x(j,k,ng)
          xp(2) = y(j,k,ng)

          js = jl
          ks = kl
          call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)

          do lvl = lvlmin,lvlmax
            call boxindex(xbar,lvl,nout,2)

            if (.not.empty_flag(lvl,nout+1,n)) then

              if (js.le.0) then
                js = istart(lvl,nout+1,1,n)
                ks = istart(lvl,nout+1,2,n)
                call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)
              endif

              if (js.le.0) then
                js = iend(lvl,nout+1,1,n)
                ks = iend(lvl,nout+1,2,n)
                call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)
              endif

            endif 
          enddo

          if (js.gt.0) then
            jl = js
            kl = ks
            ioverlap(ng,n) = .true.
            ioverlap(n,ng) = .true.
            exit
          endif
        endif

        if (nface.eq.1.or.nface.eq.3) then
          k = k + kstep
          if (k.gt.kmax(ng).or.k.lt.1) then
            nface = nface + 1
            kstep = -1*kstep
            k = k + kstep
            cycle
          endif
        endif

        if (nface.eq.2.or.nface.eq.4) then
          j = j + jstep 
          if (j.gt.jmax(ng).or.j.lt.1) then
            nface = nface + 1
            jstep = -1*jstep
            j = j + jstep
            cycle
          endif
        endif

        if (nface.eq.5) exit
      enddo

      deallocate(xg)
    endif

    enddo

  end subroutine boundconnect

!********************************************************************
  subroutine recvconnect(ng,ioverlap,nholept,iholept,diholept,idsize)
    use octree
!********************************************************************

    integer :: ng,idsize
    integer :: nholept(ngrids)
    integer :: iholept(idsize,5,ngrids)
    real    :: diholept(idsize,2,ngrids)
    logical :: ioverlap(ngrids,ngrids)

    integer :: j,k,jm1,km1,jp1,kp1,n,nc
    integer :: lvl,nout,ncount,nbodycross
    integer :: jl,kl,js,ks,jstep,kstep
    real    :: voldonor
    real    :: scalei1,scalei2

    real    :: xbar(2),xp(2),frac1(2)
    integer,allocatable :: idonorptr(:,:)
    real,allocatable :: xg(:,:,:)
    real,allocatable :: volrecv(:,:)
 
    allocate(volrecv(jmax(ng),kmax(ng)))
    allocate(idonorptr(jmax(ng),kmax(ng)))

    volrecv(:,:) = overlap_factor*volr(ng)%arr(:,:)

    idonorptr = 0

    ncount = nholept(ng)

    do n = 1,ngrids
    if (n.ne.ng.and.ioverlap(ng,n)) then
      allocate(xg(jmax(n),kmax(n),2))

      do j = 1,jmax(n)
        do k = 1,kmax(n)
          xg(j,k,1) = x(j,k,n)
          xg(j,k,2) = y(j,k,n)
        enddo
      enddo

      jl = 2; kl = 2
      js = -1; ks = -1

      j = 1; k = 1
      jstep = 1; kstep = 1
      
      scalei1 = 1./scale(1,n)
      scalei2 = 1./scale(2,n)

      do while (.true.) !loop for all points 

        nbodycross = 0

        if (cell_bc(ng)%arr(j,k).ne.3) go to 10

        xbar(1) = (x(j,k,ng)-xs(1,n))*scalei1
        xbar(2) = (y(j,k,ng)-xs(2,n))*scalei2

        if (volrecv(j,k).ge.0..and. &
            xbar(1).ge.0.and.xbar(2).ge.0.and. &
            xbar(1).lt.1.and.xbar(2).lt.1) then

          xp(1) = x(j,k,ng)
          xp(2) = y(j,k,ng)

          js = jl
          ks = kl
          call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)

          do lvl = lvlmin,lvlmax
            call boxindex(xbar,lvl,nout,2)

            if (.not.empty_flag(lvl,nout+1,n)) then

              if (js.le.0) then
                if (cell_bc(n)%arr(abs(js),abs(ks)).eq.5) then
                  nbodycross = nbodycross + 1
                endif
                js = istart(lvl,nout+1,1,n)
                ks = istart(lvl,nout+1,2,n)
                call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)
              endif

              if (js.le.0) then
                if (cell_bc(n)%arr(abs(js),abs(ks)).eq.5) then
                  nbodycross = nbodycross + 1
                endif
                js = iend(lvl,nout+1,1,n)
                ks = iend(lvl,nout+1,2,n)
                call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)
              endif

            endif 
          enddo

          if (js.le.0.and.cell_bc(n)%arr(abs(js),abs(ks)).eq.5) then
            nbodycross = nbodycross + 1
          endif

          if (js.gt.0) then

            voldonor = vold(n)%arr(js,ks)
            if (cell_bc(n)%arr(js  ,ks  ).eq.3 .or. &
                cell_bc(n)%arr(js  ,ks+1).eq.3 .or. &
                cell_bc(n)%arr(js+1,ks  ).eq.3 .or. &
                cell_bc(n)%arr(js+1,ks+1).eq.3) voldonor = 1.e30

            if (voldonor.lt.volrecv(j,k)) then
              volrecv(j,k) = voldonor
              if (idonorptr(j,k).eq.0) then
                ncount = ncount + 1
                nc = ncount
                nholept(ng) = ncount
                idonorptr(j,k) = ncount
              else
                nc = idonorptr(j,k)
              endif
              iholept(nc,1,ng) = j
              iholept(nc,2,ng) = k
              iholept(nc,3,ng) = n
              iholept(nc,4,ng) = js
              iholept(nc,5,ng) = ks
              jm1 = max(j-1,1)
              km1 = max(k-1,1)
              vold(ng)%arr(jm1, km1) = 1.e30
              vold(ng)%arr(jm1, k  ) = 1.e30
              vold(ng)%arr(j  , km1) = 1.e30
              vold(ng)%arr(j  , k  ) = 1.e30
              call find_fractions(xp,xg,frac1,js,ks, &
                                          jmax(n),kmax(n))
              diholept(nc,1,ng) = frac1(1)
              diholept(nc,2,ng) = frac1(2)
            endif

            jl = js
            kl = ks
          endif

          if (js.le.0.and.nbodycross.ge.lvlmax-lvlmin+1) then
            if (cell_bc(ng)%arr(j,k).ne.4) then
              iblank(j,k,ng) = 0
              volrecv(j,k) = -1.e30
            endif
          endif

          ichk(ng)%arr(j,k) = 1
        endif

 10     k = k + kstep
        if (k.gt.kmax(ng).or.k.lt.1) then
          kstep = -1*kstep
          k = k + kstep
          j = j + jstep
        endif
        if (j.gt.jmax(ng)) exit
      enddo

      deallocate(xg)
    endif
    enddo

    do nc = 1,ncount
      n  = iholept(nc,3,ng)
      js = iholept(nc,4,ng)
      ks = iholept(nc,5,ng)
      jp1 = min(js+1,jmax(n))
      kp1 = min(ks+1,kmax(n))
      volr(n)%arr(js , ks ) = 0.
      volr(n)%arr(js , kp1) = 0.
      volr(n)%arr(jp1, ks ) = 0.
      volr(n)%arr(jp1, kp1) = 0.
    enddo

  end subroutine recvconnect

!********************************************************************
  subroutine holeconnect(ng,ioverlap,nholept,iholept,diholept,idsize)
    use octree
!********************************************************************

    integer :: ng,idsize
    integer :: nholept(ngrids)
    integer :: iholept(idsize,5,ngrids)
    real    :: diholept(idsize,2,ngrids)
    logical :: ioverlap(ngrids,ngrids)

    integer :: j,k,jm1,km1,jp1,kp1,n,nc
    integer :: lvl,nout,ncount,nbodycross
    integer :: jl,kl,js,ks,jstep,kstep
    real    :: voldonor
    real    :: scalei1,scalei2

    real    :: xbar(2),xp(2),frac1(2)
    integer,allocatable :: idonorptr(:,:)
    real,allocatable :: xg(:,:,:)
    real,allocatable :: volrecv(:,:)

    allocate(volrecv(jmax(ng),kmax(ng)))
    allocate(idonorptr(jmax(ng),kmax(ng)))

    volrecv(:,:) = overlap_factor*volr(ng)%arr(:,:)

    idonorptr = 0

    ncount = nholept(ng)

    do n = 1,ngrids
    if (n.ne.ng.and.ioverlap(ng,n)) then
      allocate(xg(jmax(n),kmax(n),2))

      do j = 1,jmax(n)
        do k = 1,kmax(n)
          xg(j,k,1) = x(j,k,n)
          xg(j,k,2) = y(j,k,n)
        enddo
      enddo

      jl = 2; kl = 2
      js = -1; ks = -1

      j = 1; k = 1
      jstep = 1; kstep = 1
      
      scalei1 = 1./scale(1,n)
      scalei2 = 1./scale(2,n)

      do while (.true.) !loop for all points 

        nbodycross = 0

        if (volrecv(j,k).lt.0..or.ichk(ng)%arr(j,k).eq.1) go to 10

        xbar(1) = (x(j,k,ng)-xs(1,n))*scalei1
        xbar(2) = (y(j,k,ng)-xs(2,n))*scalei2

        if (xbar(1).ge.0.and.xbar(2).ge.0.and. &
            xbar(1).lt.1.and.xbar(2).lt.1) then

          lvl = lvlmin
          call boxindex(xbar,lvl,nout,2)
          if (empty_flag(lvl,nout+1,n)) go to 10

          xp(1) = x(j,k,ng)
          xp(2) = y(j,k,ng)

          js = jl
          ks = kl
          call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)

          if (js.le.0) then
            do lvl = lvlmin,lvlmax
              call boxindex(xbar,lvl,nout,2)

              if (.not.empty_flag(lvl,nout+1,n)) then

                if (js.le.0) then
                  if (cell_bc(n)%arr(abs(js),abs(ks)).eq.5) then
                    nbodycross = nbodycross + 1
                  endif
                  js = istart(lvl,nout+1,1,n)
                  ks = istart(lvl,nout+1,2,n)
                  call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)
                endif

                if (js.le.0) then
                  if (cell_bc(n)%arr(abs(js),abs(ks)).eq.5) then
                    nbodycross = nbodycross + 1
                  endif
                  js = iend(lvl,nout+1,1,n)
                  ks = iend(lvl,nout+1,2,n)
                  call finddonor(xp,xg,js,ks,jmax(n),kmax(n),100)
                endif

              endif 
            enddo
          endif

          if (js.le.0.and.cell_bc(n)%arr(abs(js),abs(ks)).eq.5) then
            nbodycross = nbodycross + 1
          endif

          if (js.gt.0) then

            voldonor = vold(n)%arr(js,ks)
            if (cell_bc(n)%arr(js  , ks  ).eq.3.or. &
                cell_bc(n)%arr(js  , ks+1).eq.3.or. &
                cell_bc(n)%arr(js+1, ks  ).eq.3.or. &
                cell_bc(n)%arr(js+1, ks+1).eq.3) voldonor = 1.e30

            if (voldonor.lt.volrecv(j,k)) then
              volrecv(j,k) = voldonor
              if (idonorptr(j,k).eq.0) then
                ncount = ncount + 1
                nc = ncount
                nholept(ng) = ncount
                idonorptr(j,k) = ncount
              else
                nc = idonorptr(j,k)
              endif
              iholept(nc,1,ng) = j
              iholept(nc,2,ng) = k
              iholept(nc,3,ng) = n
              iholept(nc,4,ng) = js
              iholept(nc,5,ng) = ks
              jm1 = max(j-1,1)
              km1 = max(k-1,1)
              vold(ng)%arr(jm1, km1) = 1.e30
              vold(ng)%arr(jm1, k  ) = 1.e30
              vold(ng)%arr(j  , km1) = 1.e30
              vold(ng)%arr(j  , k  ) = 1.e30
              call find_fractions(xp,xg,frac1,js,ks, &
                                      jmax(n),kmax(n))
              diholept(nc,1,ng) = frac1(1)
              diholept(nc,2,ng) = frac1(2)
            endif

            jl = js
            kl = ks
          endif

          if (js.le.0.and.nbodycross.ge.lvlmax-lvlmin+1) then
            if (cell_bc(ng)%arr(j,k).ne.4) then
              iblank(j,k,ng) = 0
              volrecv(j,k) = -1.e30
            endif
          endif
        endif

  10    k = k + kstep
        if (k.gt.kmax(ng).or.k.lt.1) then
          kstep = -1*kstep
          k = k + kstep
          j = j + jstep
        endif
        if (j.gt.jmax(ng)) exit
      enddo

      deallocate(xg)
    endif
    enddo

!    print*,'Number of fringe points in mesh',ng,'is', ncount

    do nc = 1,ncount
      n  = iholept(nc,3,ng)
      js = iholept(nc,4,ng)
      ks = iholept(nc,5,ng)
      jp1 = min(js+1,jmax(n))
      kp1 = min(ks+1,kmax(n))
      volr(n)%arr(js , ks ) = 0.
      volr(n)%arr(js , kp1) = 0.
      volr(n)%arr(jp1, ks ) = 0.
      volr(n)%arr(jp1, kp1) = 0.
    enddo

  end subroutine holeconnect

!********************************************************************
   subroutine metfv
!********************************************************************
      
    integer :: j,k,n,jm,km,jp1,kp1,jmx,kmx
    integer :: kk,km1,jj,jm1,nneg
    real :: dx11,dy11,dx22,dy22
    real :: fac1

    integer,allocatable :: jjp(:),jjr(:),kkp(:),kkr(:)
    real,allocatable :: a3(:,:)

    jm = 0
    km = 0
    do n = 1,ngrids
      if(jmax(n).gt.jm) jm = jmax(n) 
      if(kmax(n).gt.km) km = kmax(n) 
    enddo

    allocate(jjp(jm),jjr(jm),kkp(km),kkr(km))
    allocate(a3(jm,km))

    do n = 1,ngrids
      jmx = jmax(n)
      kmx = kmax(n)

      fac1    = 0.5

      do  j = 1,jmx
        jjp(j) = j + 1
        jjr(j) = j - 1
      enddo
      jjp(jmx) = jmx
      jjr(1) = 1

      do  k = 1,kmx
        kkp(k) = k + 1
        kkr(k) = k - 1
      enddo
      kkp(kmx) = kmx
      kkr(1) = 1

!
!..area = 0.5*P X Q, where P and Q are the diagonals
!
      do k = 1,kmx-1
        kp1 = kkp(k)
        do j = 1,jmx-1
          jp1 = jjp(j)
          dx11 = (x(jp1,kp1,n) - x(j,k,n))
          dy11 = (y(jp1,kp1,n) - y(j,k,n))
          dx22 = (x(j,kp1,n) - x(jp1,k,n))
          dy22 = (y(j,kp1,n) - y(jp1,k,n))

          a3(j,k) = fac1*( dx11*dy22 - dx22*dy11 )
        enddo
      enddo
    
!..finite-difference conventions
!
      do kk = 1,kmx
        k  = min(kk,kmx-1)
        km1 = kkr(kk)
        do jj = 1,jmx
          j  = min(jj,jmx-1)
          jm1 = jjr(jj)
          vold(n)%arr(jj,kk) = 0.25*( a3(j,k) +a3(jm1,km1) &
                       +a3(jm1,k) + a3(j,km1) )
          volr(n)%arr(jj,kk) = vold(n)%arr(jj,kk)
        enddo
      enddo

!
!..check for negative jacobians
!
      nneg = 0
      do k = 1, kmx
      do j = 1, jmx
        if(vold(n)%arr(j,k).le.0.) nneg = nneg + 1
      enddo
      enddo

      if(nneg .ne. 0) then
        write(6,*) nneg, ' negative jacobians in block'
        do k = 1,kmx
        do j = 1,jmx
          if( vold(n)%arr(j,k).lt.0.0 ) then
            write(6,603) vold(n)%arr(j,k), j, k
          endif
        enddo
        enddo
      endif
    enddo


603 format( ' ',10x,'negative jacobian = ',1p,e10.3,1x,'at j,k =', &
                    3i5,5x)

    end subroutine metfv

!********************************************************************
      subroutine finddonor(xp,x,js,ks,jmax,kmax,maxsearch)
!*********************************************************************
      integer :: js,ks,jmax,kmax,maxsearch
      real :: x(jmax,kmax,2)
      real :: xp(2)

      integer :: jj,kk,j,k,m,n,nsearch
      real    :: dum
      logical :: Inside,Outside,alter
      integer :: movej,movek
      integer :: movejp,movekp
      integer,allocatable :: i_(:)
      real,allocatable :: xc(:,:)
      real,allocatable :: p(:),q(:),r(:),pqr(:)
      integer :: mo(4),ma(4),mb(4)

      allocate(i_(2))
      allocate(xc(4,2))
      allocate(p(2),q(2),r(2),pqr(4)) ! 2**Ndim

      DATA mo(1),mo(2),mo(3),mo(4) /1,1,2,3/
      DATA ma(1),ma(2),ma(3),ma(4) /3,2,4,4/
      DATA mb(1),mb(2),mb(3),mb(4) /2,3,1,1/

!..Get starting cell index (previous cell) j,k for the search
        j = min(js,jmax-1)
        k = min(ks,kmax-1)

        Inside = .FALSE.                    !assumed false to enter the loop
        Outside = .FALSE.                   !assumed false initially
        nsearch = 0
        if (maxsearch.eq.0) maxsearch = 60

        movejp = 0; movekp = 0
        do while (.not.Inside.and..not.Outside)
          Inside = .TRUE.                   !assumed true intially
          jj = j
          kk = k

          do n = 1,2
            xc(1,n) = x(jj,  kk  ,n)
            xc(2,n) = x(jj+1,kk  ,n)
            xc(3,n) = x(jj,  kk+1,n)
            xc(4,n) = x(jj+1,kk+1,n)
          enddo

          movej  = 0; movek  = 0
!..Cross+dot product pqr=(OPxOQ).OR for in/outside cell test of point xp
          DO m=1,4                       !2*Ndim = number of cell faces
            DO n=1,2
              dum = xc(mo(m),n)
              p(n) = xc(ma(m),n) - dum
              q(n) = xc(mb(m),n) - dum
              r(n) = xp(n) - dum
            ENDDO
            pqr(m) = (p(1)*q(2)-p(2)*q(1))*(p(1)*r(2)-p(2)*r(1))
            IF(pqr(m) .lt. 0.) THEN  !If outside, get neighboring cell index
              Inside = .FALSE.
              IF(m .eq. 1) then
                j = j-1
                movej = movej-1
              endif
              IF(m .eq. 2) then
                k = k-1
                movek = movek-1
              endif
              IF(m .eq. 3) then
                j = j+1
                movej = movej+1
              endif
              IF(m .eq. 4) then
                k = k+1
                movek = movek+1
              endif
            ENDIF
          ENDDO

          alter = .false.
          if ((j.lt.1.or.j.ge.jmax).and.movek.ne.0) then
            j = j - movej
            movej = 0
            alter = .true.
          endif
          if ((k.lt.1.or.k.ge.kmax).and.movej.ne.0) then
            k = k - movek
            movek = 0
            alter = .true.
          endif

          if ((movejp+movej).eq.0.and.(movekp+movek).eq.0.and. &
              alter) then
            movej = 0
            movek = 0
          endif

          if (movej.eq.0.and.movek.eq.0.and..not.inside) &
            outside = .true.

          movejp = movej
          movekp = movek

          if (j.lt.1.or.k.lt.1.or.j.ge.jmax.or.k.ge.kmax) Outside = .TRUE.

          if(nsearch .ge. 3) then
            if(3*int((nsearch-1)/3) .eq. nsearch-1) then
              i_(1) = j
              i_(2) = k
            else
              if(i_(1).eq.j .and. i_(2).eq.k) &
                inside = .true.
            endif
          endif
          nsearch = nsearch + 1
          if (nsearch.gt.maxsearch) outside = .true.

        enddo

        if (Inside.and..not.outside) then
          js = j
          ks = k
        else
          js = -abs(j-min(movej,0))
          ks = -abs(k-min(movek,0))
        endif

      end subroutine finddonor

!********************************************************************
      subroutine find_fractions(xp,x,frac,jd,kd,jmax,kmax)
!..Interpolate coordinates frac in cell jd,kd by using Newton iteration
!********************************************************************
        integer :: jd,kd,jmax,kmax
        real :: x(jmax,kmax,2),frac(2)
        real :: xp(2)

        integer :: j,k,jj,kk,m,n,niter
        real    :: a2,a3,a4,resid
        integer,allocatable :: i_(:)
        real,allocatable :: xx(:,:,:),xc(:,:),xnew(:),B(:,:)

        allocate(i_(2))
        allocate(xx(2,2,2),xc(4,2),xnew(2),B(2,3))

!..get coordinates of stencil points for interpolation

        do kk = 1,2
        do jj = 1,2
          i_(1) = jd - 1 + jj
          i_(2) = kd - 1 + kk
          do n=1,2
            xx(jj,kk,n) = x(i_(1),i_(2),n)
          enddo
        enddo
        enddo

       do n = 1,2
         xc(1,n) = x(jd,  kd  ,n)
         xc(2,n) = x(jd+1,kd  ,n)
         xc(3,n) = x(jd,  kd+1,n)
         xc(4,n) = x(jd+1,kd+1,n)
       enddo

!..initial frac guess (linear with no cross terms)

        do n=1,2
          b(n,1) = xc(2,n) - xc(1,n)
          b(n,2) = xc(3,n) - xc(1,n)
          b(n,3) = xp(n) - xc(1,n)
        enddo

        call matrixinv2x2(b)

        do n=1,2
          frac(n) = b(n,3)
          if((frac(n)-2)*(frac(n)+1) .gt. 0.) frac(n) = .5
        enddo

!..second frac guess (linear with cross terms)

        do n=1,2
          a2 = xc(2,n) - xc(1,n)
          a3 = xc(3,n) - xc(1,n)
          a4 = xc(1,n) - xc(2,n) - xc(3,n) + xc(4,n)
          b(n,1) = a2 + a4*frac(2)
          b(n,2) = a3 + a4*frac(1)
          b(n,3) = xc(1,n)-xp(n) + a2*frac(1) + a3*frac(2) &
               + a4*frac(1)*frac(2)
        enddo
        call matrixinv2x2(b)
        do n=1,2
          frac(n) = frac(n) - b(n,3)
          if((frac(n)-1.5)*(frac(n)+.5) .gt. 0.) frac(n) = .5
        enddo

        niter = 0
        resid = 1.e10

        do while (niter.lt.10.and.resid.gt.1e-8)

!..solve f(frac) =0 =func(frac)-xp for frac. [dfdxi(frac)]*deltafrac = -f(frac)
          do n=1,2
            do m=1,2
              call bilinearinterp(m,frac,n, xx, b(n,m))
            enddo
            call bilinearinterp(0,frac,n, xx, xnew(n))
            b(n,3) = xnew(n) - xp(n)
          enddo

          call matrixinv2x2(b)

          do n=1,2
            frac(n) = frac(n) - b(n,3)
          enddo

          resid = 0.
          do n=1,2
            resid = resid + (xnew(n)-xp(n))**2
          enddo

          niter = niter + 1
        enddo

!...fix fractions if they don't fall in [0-1] range.

        if((frac(1)+.0001)*(frac(1)-1.0001).gt.0. .or. &
          (frac(2)+.0001)*(frac(2)-1.0001).gt.0. ) then
          if(frac(1) .lt. -.0001) frac(1) = 0.01
          if(frac(2) .lt. -.0001) frac(2) = 0.01
          if(frac(1) .gt. 1.0001) frac(1) = 0.99
          if(frac(2) .gt. 1.0001) frac(2) = 0.99
        end if

      end subroutine find_fractions

!********************************************************************
        subroutine bilinearinterp(ideriv,dpsi,n,f,fint)

!!  bilinear interpolation
!!  (returns fint)

!********************************************************************
        real dpsi(2),f(2,2,2),fint
        integer j,k,m, n,ideriv
        real g(2,2)

        do m=1,2
        if(m .ne. ideriv) then
          g(1,m) = 1 - dpsi(m)
          g(2,m) = dpsi(m)
        else
          g(1,m) = -1
          g(2,m) = 1
        endif
        enddo

        fint = 0.
        do k=1,2
        do j=1,2
          fint = fint + f(j,k,n) * g(j,1)*g(k,2)
        enddo
        enddo

        end subroutine bilinearinterp

!********************************************************************
        subroutine matrixinv2x2(a)
!
!  cramer's method to solve ax=b
!  (both utilizes & returns a)
!!
!! a(2,3) --- matrix to be inverted
!! 
!! a(:,3) contains the right hand side vector. the final solution is also stored
!! in a(:,3).
!!

!********************************************************************

        real a(2,3),x,y,det
!
        det = a(1,1)*a(2,2) - a(1,2)*a(2,1)
        x = a(1,3)/det
        y = a(2,3)/det
        a(1,3) =   x*a(2,2) - y*a(1,2)
        a(2,3) = - x*a(2,1) + y*a(1,1)
!
        end subroutine matrixinv2x2

!********************************************************************

end module ihc

!********************************************************************
