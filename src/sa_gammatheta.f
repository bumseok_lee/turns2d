c***********************************************************************
      subroutine sa_gammatheta(q,turmu,vort,x,y,xx,xy,yx,yy,ug,vg,
     >                          jd,kd,tscale,iblank,im)
c  correlation-based transition model - Langtry and Menter. 
c  Ref{ AIAA Journal, Vol. 47, No. 12}.
c
c***********************************************************************
      use params_global
      use rough_trans
c***********************************************************************
      implicit none
c***********************************************************************
      
      integer im,jd,kd
      real q(jd,kd,nq), turmu(jd,kd),vort(jd,kd)
      real tscale(jd,kd), x(jd,kd), y(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real ug(jd,kd), vg(jd,kd) 
      integer iblank(jd,kd)

      ! local variables
      real,allocatable :: itmc(:,:),ret(:,:)
      real,allocatable :: sn(:,:), sn_n(:,:,:)
      real,allocatable :: bjmat(:,:,:),sjmat(:,:,:)
      real,allocatable :: aj(:,:,:),cj(:,:,:)
      real,allocatable :: ak(:,:,:),ck(:,:,:)
      real,allocatable :: itmcsep(:,:),rho(:,:),u(:,:),v(:,:),vmag(:,:)
      real,allocatable :: vmul(:,:),strain(:,:),du_ds(:,:)
      real,allocatable :: tauw(:), kpp(:), f_ar(:,:)
      
      integer k,j,n,jloc,kloc
      real relfac,resitmc,resret,resmax,oat,tscal,dtpseudo_turb
      real itmclim,retlim

      integer lgturb

      allocate(itmc(jd,kd),ret(jd,kd))
      allocate(sn(jd,kd))
      allocate(bjmat(jd,kd,2),sjmat(jd,kd,2))
      allocate(aj(jd,kd,2),cj(jd,kd,2))
      allocate(ak(jd,kd,2),ck(jd,kd,2))
      allocate(itmcsep(jd,kd),rho(jd,kd),u(jd,kd),v(jd,kd),vmag(jd,kd))
      allocate(vmul(jd,kd),strain(jd,kd),du_ds(jd,kd))
      allocate(tauw(jmax),kpp(jmax),f_ar(jmax,kmax))

c***  first executable statement

! set global values
      alpha_global = 0.62
		flen_global = 0.1
!
      itmclim = 1e-20
      retlim = 1e-20
c...laminar co-efficient of viscosity calculation
      
      call lamvis(q,vmul,jd,kd)

      do  k = 1,kmax
      do  j = 1,jmax
        rho(j,k) = q(j,k,1)*q(j,k,nq)
        u(j,k)  = q(j,k,2)/q(j,k,1)
        v(j,k)  = q(j,k,3)/q(j,k,1)
        vmag(j,k) = sqrt(u(j,k)*u(j,k) + v(j,k)*v(j,k))
      enddo
      enddo

c...calculate wall distance and wall normal vectors
      call sa_gammatheta_dist(sn,x,y,jd,kd)     

c...apply boundary condition

      call sa_gammathetabc(q,rho,u,v,ug,vg,xx,xy,yx,yy,im)

c...initiate local working variable for intermittency and Re-theta

      do  k = 1,kmax
      do  j = 1,jmax
        itmc(j,k) = q(j,k,nv-1)
        ret(j,k) = q(j,k,nv)
      enddo
      enddo

c...compute strain rate

      call sa_calc_strain_du_ds(q,strain,du_ds,
     & u,v,vmag,xx,xy,yx,yy,jd,kd)


      tauw = 0.0; kpp=0.0; f_ar=0.0
      if(irough) then
        call wall_shear(q,xx,xy,yx,yy,tauw,kpp,vmul,turmu,jd,kd)
        call amp_rough(kpp,f_ar,rho,vort,vmul,turmu,jd,kd,
     &       u,v,ug,vg,xx,xy,yx,yy,tscale,iblank,im)
      endif

c...compute rhs and lhs

      call sa_gammathetarhslhs(q,itmc,ret,itmcsep,vmul,turmu,vort,strain,
     &                      du_ds,rho,u,v,vmag,ug,vg,xx,xy,yx,yy,
     &                      sn,aj,bjmat,cj,ak,ck,sjmat,f_ar,jd,kd)

c...invert using DDADI

c..set time-accuracy
      oat = 1.0
      if (ntac.eq.-2) oat = 0.5
      if (ntac.ge.2 .and. istep.gt.1) oat = 2./3.
      if (ntac.eq.3 .and. istep.gt.2) oat = 6./11.

      resitmc=0.0
      resret=0.0
      do k = 1,kmax
      do j = 1,jmax

        tscal = tscale(j,k)
        if(timeac.eq.1) then
          dtpseudo_turb=10.0
          tscal = max(iblank(j,k),0)*( 1.0 + 0.002*sqrt(q(j,k,nq)))
     <                       /(1.+sqrt(q(j,k,nq)))
          tscal = tscal*dtpseudo_turb
          tscal = tscal/(1.+tscal/h/oat)
        endif

        do n = 1,2
          aj(j,k,n) = aj(j,k,n)*tscal
          cj(j,k,n) = cj(j,k,n)*tscal
          ak(j,k,n) = ak(j,k,n)*tscal
          ck(j,k,n) = ck(j,k,n)*tscal
          bjmat(j,k,n) = 1.+ bjmat(j,k,n)*tscal
          sjmat(j,k,n)  = sjmat(j,k,n)*tscal
        enddo
      enddo
      enddo

      call lsolvej2(aj,bjmat,cj,sjmat,jd,kd)

      do k = 1,kmax
      do j = 1,jmax
        sjmat(j,k,1) = sjmat(j,k,1)*bjmat(j,k,1)
        sjmat(j,k,2) = sjmat(j,k,2)*bjmat(j,k,2)
      enddo
      enddo

      call lsolvek2(ak,bjmat,ck,sjmat,jd,kd)

      relfac = 1.
      resmax = 0.
      do k = 1,kmax
      do j = 1,jmax
        sjmat(j,k,1) = relfac*sjmat(j,k,1)
        sjmat(j,k,2) = relfac*sjmat(j,k,2)

        itmc(j,k) = itmc(j,k) + sjmat(j,k,1)*max(iblank(j,k),0)
        ret(j,k) = ret(j,k) + sjmat(j,k,2)*max(iblank(j,k),0)

        itmc(j,k) = max(itmc(j,k),itmclim)
        ret(j,k) = max(ret(j,k),retlim)

        ! CBSL test
        !resitmc = resitmc + sjmat(j,k,1)**2
        resret = resret + sjmat(j,k,2)**2
        if (max(resitmc,resret).gt.resmax) then
          jloc = j
          kloc = k
          resmax = max(resitmc,resret)
        endif

c...Modification in intermittency for separation-induced transition
!        itmc(j,k) = max(itmc(j,k),rho(j,k)*itmcsep(j,k))

		  itmc(j,k) = min(max(itmc(j,k),itmclim),1.0)

        ! CBSL
        resitmc = resitmc + (itmc(j,k)-q(j,k,nv-1))**2

      enddo
      enddo

      resitmc = sqrt(resitmc/jmax/kmax)
      resret = sqrt(resret/jmax/kmax)
      resmax = sqrt(resmax/jmax/kmax)
      if( mod(istep,npnorm).eq.0) then
         write(1333+im,61) float(istep0),resitmc,resret
      endif

 61   FORMAT (3(x,E14.6))

!..update the global variables
      do  k = 1,kmax
      do  j = 1,jmax
        q(j,k,nv-1) = itmc(j,k)
        q(j,k,nv) = ret(j,k)
      enddo
      enddo

c...apply boundary condition again

      call sa_gammathetabc(q,rho,u,v,ug,vg,xx,xy,yx,yy,im)

      return
      end

c*************************************************************
      subroutine sa_calc_strain_du_ds(q,strain,du_ds,
     & u,v,vmag,xx,xy,yx,yy,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real q(jd,kd,nq),strain(jd,kd),du_ds(jd,kd)
      real u(jd,kd),v(jd,kd),vmag(jd,kd)
      real xx(jd,kd),xy(jd,kd),yx(jd,kd),yy(jd,kd)

! local variables
      integer j,k,jm1,km1,jp,kp
      real usi,vsi,ueta,veta
      real ux,uy,vx,vy
      real sxx,sxy,syy,tx

      do j = 2,jd-1 
        jp = j + 1
        jm1 = j - 1
        do k = 2,kd-1
          kp = k + 1
          km1 = k - 1

          usi  = 0.5*(u(jp,k)-u(jm1,k))
          vsi  = 0.5*(v(jp,k)-v(jm1,k))
          
          ueta = 0.5*(u(j,kp)-u(j,km1))
          veta = 0.5*(v(j,kp)-v(j,km1))
            
          ux = xx(j,k)*usi + yx(j,k)*ueta
          uy = xy(j,k)*usi + yy(j,k)*ueta

          vx = xx(j,k)*vsi + yx(j,k)*veta
          vy = xy(j,k)*vsi + yy(j,k)*veta

          sxx = ux
          sxy = 0.5*(uy + vx)
          syy = vy

          strain(j,k) = sqrt(2.*(sxx*sxx + 2.*sxy*sxy + syy*syy))
          du_ds(j,k) = (u(j,k)*u(j,k)*sxx + 2.*u(j,k)*v(j,k)*sxy + 
     &                  v(j,k)*v(j,k)*syy)/(vmag(j,k)*vmag(j,k))

        enddo
      enddo

      return
      end


c*************************************************************
      subroutine sa_gammatheta_dist(sn,x,y,jd,kd)
c
c     Note
c     The wall distance is calculated again to avoid interference
c     with the modifications of the wall distance in DDES
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real sn(jd,kd)
      real x(jd,kd),y(jd,kd)
c*************************************************************
      integer j,k

c***  first executable statement
 
      k=1
      do j=jtail1,jtail2
      sn(j,k)=0.0
      enddo
   
      do j=jtail1,jtail2
      do k=2,kmax
        sn(j,k)     = sqrt((x(j,k)-x(j,1))**2+(y(j,k)-y(j,1))**2)
      enddo
      enddo
   
      if(jtail1.ne.1) then
        do j=1,jtail1-1
        do k=1,kmax
          sn(j,k)=sqrt((x(j,k)-x(jtail1,1))**2
     &                +(y(j,k)-y(jtail1,1))**2)

        enddo
        enddo
      endif
     
      if(jtail2.ne.jmax) then
        do j=jtail2+1,jmax
        do k=1,kmax
          sn(j,k)=sqrt((x(j,k)-x(jtail2,1))**2
     &                +(y(j,k)-y(jtail2,1))**2)
        enddo
        enddo
      endif
    
      return
      end     


c***********************************************************************
      subroutine sa_gammathetarhslhs(q,itmc,ret,itmcsep,vmul,turmu,vort,strain,
     &                        du_ds,rho,u,v,vmag,ug,vg,xx,xy,yx,yy,sn,
     &                        aj,bjmat,cj,ak,ck,sjmat,f_ar,jd,kd)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************

      integer jd,kd
      real q(jd,kd,nq), itmc(jd,kd), ret(jd,kd), itmcsep(jd,kd)
      real rho(jd,kd), vmul(jd,kd), turmu(jd,kd)
      real vort(jd,kd),strain(jd,kd),du_ds(jd,kd)
      real u(jd,kd),v(jd,kd),vmag(jd,kd),ug(jd,kd),vg(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real sn(jd,kd), f_ar(jd,kd)
      real aj(jd,kd,2),bjmat(jd,kd,2),cj(jd,kd,2)
      real ak(jd,kd,2),ck(jd,kd,2),sjmat(jd,kd,2)

      ! local variables
      integer j,k,n,jp,kp,jm1,km1
      real uu,vv,fwd,bck
      real,allocatable :: up(:),um(:),vp(:),vm(:)

      allocate(up(mdim),um(mdim),vp(mdim),vm(mdim))

c***  first executable statement

      do k = 1,kmax
      do j = 1,jmax
        do n = 1,2
          aj(j,k,n)=0.0
          bjmat(j,k,n)=0.0
          cj(j,k,n)=0.0
          ak(j,k,n)=0.0
          ck(j,k,n)=0.0
          sjmat(j,k,n) = 0.
        enddo
      enddo
      enddo

c...lhs contribution from the convection term

      do k=2,kmax-1
        do j=1,jmax
          uu=xx(j,k)*(u(j,k)-ug(j,k))+xy(j,k)*(v(j,k)-vg(j,k))
          up(j) = 0.5*(uu+abs(uu))
          um(j) = 0.5*(uu-abs(uu))
        enddo

        do j = 2,jmax-1
          jp = j + 1
          jm1 = j - 1

          if(up(j).gt.1.0e-12) then
            fwd=1.0
          else
            fwd=0.0
          endif

          if(um(j).lt.-1.0e-12) then
            bck=1.0
          else
            bck=0.0
          endif

          sjmat(j,k,1) = sjmat(j,k,1) - up(j)*(itmc(j,k) - itmc(jm1,k)) -
     &                                  um(j)*(itmc(jp,k) - itmc(j,k))
          sjmat(j,k,2) = sjmat(j,k,2) - up(j)*(ret(j,k) - ret(jm1,k)) - 
     &                                  um(j)*(ret(jp,k) - ret(j,k))
          do n = 1,2
            aj(j,k,n) = aj(j,k,n) - up(j)!fwd*(up(jm1)+um(jm1))
            cj(j,k,n) = cj(j,k,n) + um(j)!bck*(up(jp)+um(jp))
            bjmat(j,k,n) = bjmat(j,k,n) + up(j)- um(j)
          enddo
        enddo
      enddo

      do j=2,jmax-1
        do k=1,kmax
          vv = yx(j,k)*(u(j,k)-ug(j,k))+yy(j,k)*(v(j,k)-vg(j,k))
          vp(k) = 0.5*(vv+abs(vv))
          vm(k) = 0.5*(vv-abs(vv))
        enddo

        do k = 2,kmax-1
          kp = k + 1
          km1 = k - 1

          if(vp(k).gt.1.0e-12) then
            fwd=1.0
          else
            fwd=0.0
          endif

          if(vm(k).lt.-1.0e-12) then
            bck=1.0
          else
            bck=0.0
          endif

          sjmat(j,k,1) = sjmat(j,k,1) - vp(k)*(itmc(j,k) - itmc(j,km1)) -
     &                                  vm(k)*(itmc(j,kp) - itmc(j,k))
          sjmat(j,k,2) = sjmat(j,k,2) - vp(k)*(ret(j,k) - ret(j,km1)) -
     &                                  vm(k)*(ret(j,kp) - ret(j,k))
          do n = 1,2
            ak(j,k,n) = ak(j,k,n) - vp(k) !fwd*(vp(km1)+vm(km1))
            ck(j,k,n) = ck(j,k,n) + vm(k) !bck*(vp(kp)+vm(kp))
            bjmat(j,k,n) = bjmat(j,k,n) + vp(k) - vm(k)
          enddo
        enddo
      enddo
       
c...rhs and lhs contribution from the source term

      call sa_gammathetasource(q,itmc,ret,itmcsep,rho,vmul,turmu,vort,strain,du_ds,vmag,sn,
     &                  aj,bjmat,cj,ak,ck,sjmat,f_ar,jd,kd)


c...rhs contribution from the diffusion term

      call sa_gammathetadiffus(q,itmc,ret,rho,vmul,turmu,
     &                      xx,xy,yx,yy,aj,bjmat,cj,ak,ck,sjmat,jd,kd)

      return
      end

c***********************************************************************
      subroutine sa_gammathetadiffus(q,itmc,ret,rho,vmul,turmu,
     &                            xx,xy,yx,yy,aj,bjmat,cj,ak,ck,sjmat,jd,kd)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
      integer jd,kd
      real q(jd,kd,nq),itmc(jd,kd),ret(jd,kd),rho(jd,kd)
      real vmul(jd,kd),turmu(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real aj(jd,kd,2),bjmat(jd,kd,2),cj(jd,kd,2)
      real ak(jd,kd,2),ck(jd,kd,2),sjmat(jd,kd,2)
      
    ! local variables
      integer j,k,j1,k1

      real,allocatable :: chp(:,:),dnuhp(:,:)
      real,allocatable :: dmxh(:,:),dmyh(:,:)

      real rei,vnulh,vnuh,dxp,dxm,c2,dcp,dcm,ax,cx,dyp,dym,ay,cy

      include 'gammatheta.h'

      allocate(chp(jd,kd),dnuhp(jd,kd))
      allocate(dmxh(jd,kd),dmyh(jd,kd))

c***  first executable statement

        rei = 1./rey
c       compute j direction differences.

c       compute half-point co-efficients

        do k=1,kmax
        do j=1,jmax-1
          dmxh(j,k) = 0.5*(xx(j,k)+xx(j+1,k))
          dmyh(j,k) = 0.5*(xy(j,k)+xy(j+1,k))
        enddo
        enddo

c     j-direction diffusion for itmc-equation

        do k=1,kmax
        do j=1,jmax-1
          vnulh     = 0.5*(vmul(j,k)+vmul(j+1,k))
          vnuh     = 0.5*(turmu(j,k)+turmu(j+1,k))
          chp(j,k) = rei*(vnulh+vnuh/sigmaf)/rho(j,k)
          dnuhp(j,k) =itmc(j+1,k)-itmc(j,k)
        enddo
        enddo

        do k=1,kmax
        do j=2,jmax-1

          dxp=dmxh(j,k)*xx(j,k)+dmyh(j,k)*xy(j,k)
          dxm=dmxh(j-1,k)*xx(j,k)+dmyh(j-1,k)*xy(j,k)

c         enforce positivity (as suggested by overflow)
          
          dcp    = dxp*(chp(j,k))
          dcm    = dxm*(chp(j-1,k))
          ax=0.0
          cx=0.0

          if(k.ne.kmax.and.k.ne.1) then
            ax       = max(dcm,0.0)
            cx       = max(dcp,0.0)
          endif
c          compute fluxes.

          sjmat(j,k,1)=sjmat(j,k,1)-ax*dnuhp(j-1,k)+cx*dnuhp(j,k)

c          jacobian terms

          aj(j,k,1) = aj(j,k,1) - ax
          cj(j,k,1) = cj(j,k,1) - cx
          bjmat(j,k,1) = bjmat(j,k,1)+ (ax + cx)

        enddo
        enddo

c      j-direction diffusion for ret-equation

        do k=1,kmax
        do j=1,jmax-1
          vnulh     = 0.5*(vmul(j,k)+vmul(j+1,k))
          vnuh     = 0.5*(turmu(j,k)+turmu(j+1,k))
          chp(j,k) = rei*sigmat*(vnulh+vnuh)/rho(j,k)
          dnuhp(j,k) =ret(j+1,k)-ret(j,k)
        enddo
        enddo

        do k=1,kmax
        do j=2,jmax-1

          dxp=dmxh(j,k)*xx(j,k)+dmyh(j,k)*xy(j,k)
          dxm=dmxh(j-1,k)*xx(j,k)+dmyh(j-1,k)*xy(j,k)

c         enforce positivity (as suggested by overflow)
          
          dcp    = dxp*(chp(j,k))
          dcm    = dxm*(chp(j-1,k))
          ax=0.0
          cx=0.0

          if(k.ne.kmax.and.k.ne.1) then
            ax       = max(dcm,0.0)
            cx       = max(dcp,0.0)
          endif
c          compute fluxes.

          sjmat(j,k,2)=sjmat(j,k,2)-ax*dnuhp(j-1,k)+cx*dnuhp(j,k)

c          jacobian terms

          aj(j,k,2) = aj(j,k,2) - ax
          cj(j,k,2) = cj(j,k,2) - cx
          bjmat(j,k,2) = bjmat(j,k,2)+ (ax + cx)

        enddo
        enddo

c       compute k direction differences.

c       compute half-point co-efficients

        do k=1,kmax-1
        do j=1,jmax
          dmxh(j,k) = 0.5*(yx(j,k)+yx(j,k+1))
          dmyh(j,k) = 0.5*(yy(j,k)+yy(j,k+1))
       enddo
       enddo

c     k-direction diffusion for itmc-equation

        do k=1,kmax-1
        do j=1,jmax
          vnulh     = 0.5*(vmul(j,k)+vmul(j,k+1))
          vnuh     =  0.5*(turmu(j,k)+turmu(j,k+1))
          chp(j,k) =rei*(vnulh+vnuh/sigmaf)/rho(j,k)  
          dnuhp(j,k) =itmc(j,k+1)-itmc(j,k)
       enddo
       enddo

       do k=2,kmax-1
       do j=1,jmax

         dyp=dmxh(j,k)*yx(j,k)+dmyh(j,k)*yy(j,k)
         dym=dmxh(j,k-1)*yx(j,k)+dmyh(j,k-1)*yy(j,k)

c      enforce positivity (as suggested by overflow)
       
          dcp    = dyp*(chp(j,k))
          dcm    = dym*(chp(j,k-1))

          ay=0.0
          cy=0.0
          if(j.ne.1.and.j.ne.jmax) then
            ay       = max(dcm,0.0)
            cy       = max(dcp,0.0)
          endif

c        compute fluxes.

          sjmat(j,k,1)=sjmat(j,k,1)-ay*dnuhp(j,k-1)+cy*dnuhp(j,k)

c        jacobian terms

          ak(j,k,1) = ak(j,k,1) - ay
          ck(j,k,1) = ck(j,k,1) - cy
          bjmat(j,k,1) = bjmat(j,k,1)+ (ay + cy)

        enddo
        enddo

c     k-direction diffusion for ret-equation

        do k=1,kmax-1
        do j=1,jmax
          vnulh      = 0.5*(vmul(j,k)+vmul(j,k+1))
          vnuh       = 0.5*(turmu(j,k)+turmu(j,k+1))
          chp(j,k)   = rei*sigmat*(vnulh+vnuh)/rho(j,k)     
          dnuhp(j,k) = ret(j,k+1)-ret(j,k)
       enddo
       enddo

       do k=2,kmax-1
       do j=1,jmax

         dyp=dmxh(j,k)*yx(j,k)+dmyh(j,k)*yy(j,k)
         dym=dmxh(j,k-1)*yx(j,k)+dmyh(j,k-1)*yy(j,k)

c      enforce positivity (as suggested by overflow)
       
          dcp    = dyp*(chp(j,k))
          dcm    = dym*(chp(j,k-1))

          ay=0.0
          cy=0.0
          if(j.ne.1.and.j.ne.jmax) then
            ay       = max(dcm,0.0)
            cy       = max(dcp,0.0)
          endif

c        compute fluxes.

          sjmat(j,k,2)=sjmat(j,k,2)-ay*dnuhp(j,k-1)+cy*dnuhp(j,k)

c        jacobian terms

          ak(j,k,2) = ak(j,k,2) - ay
          ck(j,k,2) = ck(j,k,2) - cy
          bjmat(j,k,2) = bjmat(j,k,2)+ (ay + cy)

        enddo
        enddo

      return
      end

c***********************************************************************
      subroutine sa_gammathetasource(q,itmc,ret,itmcsep,rho,vmul,turmu,
     &                            vort,strain,du_ds,vmag,
     &                            sn,aj,bjmat,cj,ak,ck,sjmat,f_ar,jd,kd)
c***********************************************************************
        use params_global
c***********************************************************************
        implicit none
        integer jd,kd
        real q(jd,kd,nq), itmc(jd,kd), ret(jd,kd), itmcsep(jd,kd) 
        real rho(jd,kd), vort(jd,kd),strain(jd,kd),du_ds(jd,kd),vmag(jd,kd)
        real vmul(jd,kd),sn(jd,kd),turmu(jd,kd)
        real aj(jd,kd,2),bjmat(jd,kd,2),cj(jd,kd,2)
        real ak(jd,kd,2),ck(jd,kd,2),sjmat(jd,kd,2)
        real f_ar(jd,kd)
        
        ! local variables
        integer j,k,iter
        real rei
        real rey_t,rey_tc,re_theta,re_theta_lim
        real rey_t_lim
        real flen,fsublayer,rw,re_v,re_omega,r_t
        real f_onset,f_onset1,f_onset2,f_onset3
        real f_turb,f_lambda,f_wake,f_reattach
        real tu,theta,theta_bl,lambda
        real time_scale,delta,delta_bl
        real prod, des,itmc_max
        real var1, var2
		  real fdes,rdes,fdes1,rdes1
		  real,allocatable :: f_theta(:,:)
		  real,allocatable :: f_on(:,:),onset(:)
		  real n_onset,on
		  real blend_ar

		  integer lgturb 
        
        include 'gammatheta.h'

c**   first executable statement

		  allocate(f_theta(jd,kd))
		  allocate(f_on(jd,kd),onset(jd))

        rei = 1./rey
        pi  = 4.0 * atan( 1.0 )

        re_theta_lim = 20.0

        do k=2,kmax-1
        do j=2,jmax-1

			 !...calculate blending function f_theta
          !theta_bl = rei*ret(j,k)*(vmul(j,k)+turmu(j,k))/(rho(j,k)*vmag(j,k))
          theta_bl = rei*ret(j,k)*vmul(j,k)/(rho(j,k)*vmag(j,k))
          delta_bl = 7.5*theta_bl
          delta = 50.0*vort(j,k)*sn(j,k)*delta_bl/vmag(j,k) + 1.e-20
          f_theta(j,k) = min(exp(-(sn(j,k)/delta)**4),1.)

		  enddo
		  enddo

        do k=2,kmax-1
        do j=2,jtail1-1
          f_theta(j,k) = 0.0
		  enddo
		  enddo

        do k=2,kmax-1
        do j=jtail2+1,jmax-1
          f_theta(j,k) = 0.0
		  enddo
		  enddo

		  !******* CALCULATE SOURCE TERMS *********
        do k=2,kmax-1
        do j=2,jmax-1

c..SOURCE TERM FOR RE_THETA EQUATION

!...limit minimum value of turbulent intensity to 0.027 (as recommended by langtry)
          tu = tuinf
!a          tu = max(100.*sqrt(2./3*q(j,k,5)/rho(j,k))/vmag(j,k),0.027)

!...iterate for value of transition momentum thickness reynolds number, re_theta
!...lambda is the pressure gradient paramter

          f_lambda = 1. ! initial value of lambda taken as f_lambda(lambda=0)

          do iter = 1,5  ! 5 iterations should be sufficient for convergence

				! langtry correlations
!            if(tu.le.1.3) then
!              re_theta = f_lambda * (1173.51-589.428*tu+0.2196/(tu*tu))
!            else
!              re_theta = 331.5 * f_lambda*(tu-0.5658)**(-0.671)
!            endif

				! medida correlations
            re_theta = f_lambda*retinf

            re_theta = max(re_theta,re_theta_lim)
            theta = re_theta*vmul(j,k)*rei/(rho(j,k)*(vmag(j,k)+1.e-20))
            lambda = rey*rho(j,k)*theta*theta*du_ds(j,k)/vmul(j,k)
            lambda = min(max(-0.1,lambda),0.1)

            if(lambda.le.0.) then
              f_lambda = 1. - (-12.986*lambda - 123.66*lambda**2 - 
     &                         405.689*lambda**3)*exp(-(2./3*tu)**1.5)
            else
              f_lambda = 1. + 0.275*(1.-exp(-35.*lambda))*exp(-2.*tu)
            endif
          enddo

			 !...calculate blending function f_theta
          time_scale = 500.0*vmul(j,k)/(rho(j,k)*vmag(j,k)*vmag(j,k))/rey
          var1 = c_theta*(1.-f_theta(j,k))/time_scale

          bjmat(j,k,2) = bjmat(j,k,2) + var1
          sjmat(j,k,2) = sjmat(j,k,2) + var1*(re_theta-ret(j,k))


          if(irough) then
            var2 = c_theta*f_ar(j,k)/time_scale ! roughness far
            blend_ar = 1.0
            if(ret(j,k).le.174.5) then
            blend_ar = (0.5*sin(pi/155.0*ret(j,k)-97.0*pi/155.0)+0.5)**2
            endif
            sjmat(j,k,2) = sjmat(j,k,2) - var2*blend_ar
          endif

		  enddo
		  enddo

		  ! Compute local F_onset_1
        do k=2,kmax-1
        do j=2,jmax-1

!....use dimensional value of re_theta_t in correlation expressions
          rey_t = ret(j,k)

!....re_theta_critical correlation
          rey_tc = alpha_global*rey_t

!....vorticity reynolds number
          re_v = rey*rho(j,k)*sn(j,k)**2*strain(j,k)/vmul(j,k)

!....f_onset controls transition onset location
          f_on(j,k) = re_v/(2.193*rey_tc)

        enddo
        enddo

		  do j = 1,jmax
			 onset(j) = 1.0
        enddo

			! Compute station F_onset
			do j=jtail1,jtail2
			  n_onset = 0.0
			  onset(j) = 0.0
			  do k=2,kmax-1
			    var1 = max(sign(1.0,f_on(j,k)-1.0),0.0)
			    var2 = max(sign(1.0,f_theta(j,k)-0.999),0.0)
			    n_onset = n_onset + var1*var2
			  enddo
			  onset(j) = max(min(n_onset,1.0),0.0)
			enddo


c..SOURCE TERM FOR INTERMITTENCY EQUATION

        do k=2,kmax-1
        do j=2,jmax-1

          rey_t = ret(j,k)

!....re_theta_critical correlation

          rey_tc = alpha_global*rey_t

			 !flen = flen_global
			 !flen = max(vort(j,k)/40.0,0.4)
			 flen = max(vort(j,k)/10.0,0.4) !CBSL

!....f_onset controls transition onset location

          r_t = turmu(j,k)/vmul(j,k)
          f_onset1 = f_on(j,k)
          f_onset2 = min(max(f_onset1,f_onset1**2.0),4.0)
          f_onset3 = max(2.0 - (0.25*r_t)**3.0,0.)
          f_onset = min(max(f_onset2 - f_onset3, 0.0),1.0)

			 itmc_max = 1.0_8
			 on = min(onset(j),1.0)

          prod = f_onset*flen*on
          if(itmc(j,k)>1.0) prod = prod * (itmc_max - itmc(j,k))
          des = itmc(j,k)*vort(j,k)*(1.0-on)

          var1 = f_onset*flen*on + vort(j,k)*(1.0-on)
          bjmat(j,k,1) = bjmat(j,k,1) + var1
          sjmat(j,k,1) = sjmat(j,k,1) + prod - des

        enddo
        enddo

		  deallocate(f_theta,f_on,onset)

        return
        end

c*************************************************************
        subroutine sa_gammathetabc(q,rho,u,v,ug,vg,xx,xy,yx,yy,im)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        real q(jmax,kmax,nq),rho(jmax,kmax)
        real u(jmax,kmax),v(jmax,kmax),ug(jmax,kmax),vg(jmax,kmax)
        real xx(jmax,kmax),xy(jmax,kmax),yx(jmax,kmax),yy(jmax,kmax)
        integer im

c..   local variables
        integer js,je,ks,ke,idir
        integer j,k,ib

        do ib=1,nbc_all(im)
          js = jbcs_all(ib,im)
          je = jbce_all(ib,im)
          ks = kbcs_all(ib,im)
          ke = kbce_all(ib,im)
          if(js.lt.0) js = jmax+js+1
          if(ks.lt.0) ks = kmax+ks+1
          if(je.lt.0) je = jmax+je+1
          if(ke.lt.0) ke = kmax+ke+1
          idir = ibdir_all(ib,im)

c.. outflow bc - crude implementation (shivaji)
        if ((ibtyp_all(ib,im).eq.53). or. (ibtyp_all(ib,im).eq.55)) then
          call sa_gammathetabc_outflow(q,js,je,ks,ke,idir)

c.. inviscid wind tunnel wall bc
          elseif (ibtyp_all(ib,im).eq.3) then
            call sa_gammathetabc_extpt(q,js,je,ks,ke,idir)

c.. wall bc at l = 1 (only interior portion of wall)
          elseif (ibtyp_all(ib,im).eq.4 .or. ibtyp_all(ib,im).eq.5) then
            call sa_gammathetabc_wall(q,js,je,ks,ke,idir)

c.. symmetric bc
          elseif (ibtyp_all(ib,im).eq.11) then
            call sa_gammathetabc_sym(q,js,je,ks,ke,idir)

c.. periodic bc
          elseif (ibtyp_all(ib,im).eq.22) then
            call sa_gammathetabc_periodic(q,js,je,ks,ke,idir)

c.. averaging bc for wake
          elseif (ibtyp_all(ib,im).eq.51) then
            call sa_gammathetabc_wake(q,js,je,ks,ke,idir)

c.. averaging bc for wake of O-grid
          elseif (ibtyp_all(ib,im).eq.52) then
            call sa_gammathetabc_wake_ogrid(q,js,je,ks,ke,idir)

c.. freesream bc
          elseif (ibtyp_all(ib,im).eq.47) then
            call sa_gammathetabc_out(q,rho,u,v,ug,vg,xx,xy,yx,yy,js,je,ks,ke,idir)

          endif
        enddo

        return
        end

c*************************************************************
      subroutine sa_gammathetabc_outflow(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,nv-1) = q(j1,k,nv-1)
          q(j,k,nv) = q(j1,k,nv)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,nv-1) = q(j,k1,nv-1)
          q(j,k,nv) = q(j,k1,nv)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammathetabc_wall(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,nv-1) = q(j1,k,nv-1)
          q(j,k,nv) = q(j1,k,nv)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,nv-1) = q(j,k1,nv-1)
          q(j,k,nv) = q(j,k1,nv)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammathetabc_extpt(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          !q(j,k,7) = q(j1,k,7)
          !q(j,k,8) = q(j1,k,8)
          q(j,k,nv-1) = q(j1,k,nv-1)
          q(j,k,nv) = q(j1,k,nv)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          !q(j,k,7) = q(j,k1,7)
          !q(j,k,8) = q(j,k1,8)
          q(j,k,nv-1) = q(j,k1,nv-1)
          q(j,k,nv) = q(j,k1,nv)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammathetabc_sym(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          !q(j,k,7) = q(j1,k,7)
          !q(j,k,8) = q(j1,k,8)
          q(j,k,nv-1) = q(j1,k,nv-1)
          q(j,k,nv) = q(j1,k,nv)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          !q(j,k,7) = q(j,k1,7)
          !q(j,k,8) = q(j,k1,8)
          q(j,k,nv-1) = q(j,k1,nv-1)
          q(j,k,nv) = q(j,k1,nv)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammathetabc_periodic(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jc,jj,jj1,kc,kk,kk1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(idir.eq.1) then
        jj  = je - js + 1
        do j = js,je
           jj1 = j - js
           jc = jmax - 2*jj + jj1
 
           do k = ks,ke
             !q(j,k,7) = q(jc,k,7)
             !q(j,k,8) = q(jc,k,8)
             q(j,k,nv-1) = q(jc,k,nv-1)
             q(j,k,nv) = q(jc,k,nv)
           enddo
        enddo

      elseif(idir.eq.-1) then
        jj  = je - js + 1
        do j = js,je
           jj1 = je - j
           jc = 1 + 2*jj - jj1
 
           do k = ks,ke
             !q(j,k,7) = q(jc,k,7)
             !q(j,k,8) = q(jc,k,8)
             q(j,k,nv-1) = q(jc,k,nv-1)
             q(j,k,nv) = q(jc,k,nv)
           enddo
        enddo

      elseif(idir.eq.2) then
        kk  = ke - ks + 1
        do k = ks,ke
           kk1 = k - ks
           kc = kmax - 2*kk + kk1
 
           do j = js,je
             !q(j,k,7) = q(j,kc,7)
             !q(j,k,8) = q(j,kc,8)
             q(j,k,nv-1) = q(j,kc,nv-1)
             q(j,k,nv) = q(j,kc,nv)
           enddo
        enddo

      elseif(idir.eq.-2) then
        kk  = ke - ks + 1
        do k = ks,ke
           kk1 = ke - k
           kc = 1 + 2*kk - kk1 
 
           do j = js,je
             !q(j,k,7) = q(j,kc,7)
             !q(j,k,8) = q(j,kc,8)
             q(j,k,nv-1) = q(j,kc,nv-1)
             q(j,k,nv) = q(j,kc,nv)
           enddo
        enddo

      endif

      return
      end

c*************************************************************
      subroutine sa_gammathetabc_wake(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jj,j1,k1,iadd,iadir
      real qav1,qav2

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        print*,'idir = ',idir,' is not implemented in gammathetabc_wake'
      elseif(iadir.eq.2) then
        k  = ks
        k1 = k + iadd
        do j=js,je
          jj = jmax - j + 1
!a          qav1 = 0.5*(q(j,k1,7)+q(jj,k1,7))
!a          qav2 = 0.5*(q(j,k1,8)+q(jj,k1,8))
!a          q(j,k,7)  = qav1
!a          q(j,k,8)  = qav2
!a          q(jj,k,7) = qav1
!a          q(jj,k,8) = qav2
          qav1 = 0.5*(q(j,k1,nv-1)+q(jj,k1,nv-1))
          qav2 = 0.5*(q(j,k1,nv)+q(jj,k1,nv))
          q(j,k,nv-1)  = qav1
          q(j,k,nv)  = qav2
          q(jj,k,nv-1) = qav1
          q(jj,k,nv) = qav2
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammathetabc_wake_ogrid(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jj,j1,iadd,iadir
      real qav1,qav2

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j  = js
        j1 = j + iadd
        jj = jmax - j1 + 1
        do k=ks,ke
!a          qav1 = 0.5*(q(j1,k,7)+q(jj,k,7))
!a          qav2 = 0.5*(q(j1,k,8)+q(jj,k,8))
!a          q(j,k,7) = qav1
!a          q(j,k,8) = qav2
          qav1 = 0.5*(q(j1,k,nv-1)+q(jj,k,nv-1))
          qav2 = 0.5*(q(j1,k,nv)+q(jj,k,nv))
          q(j,k,nv-1) = qav1
          q(j,k,nv) = qav2
        enddo
      elseif(iadir.eq.2) then
      print*,'idir = ',idir,' is not implemented in 
     c                              gammathetabc_wake_ogrid'
      endif

      return
      end

c*************************************************************
      subroutine sa_gammathetabc_out(q,rho,u,v,ug,vg,xx,xy,yx,yy,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq),rho(jmax,kmax),u(jmax,kmax),v(jmax,kmax)
      real ug(jmax,kmax),vg(jmax,kmax)
      real xx(jmax,kmax),xy(jmax,kmax),yx(jmax,kmax),yy(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir
      real uu

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j  = js
        j1 = j + iadd
        do k=ks,ke
          !q(j,k,7)=itmcinf*rho(j,k)
          !q(j,k,8)=retinf*rho(j,k)
          q(j,k,nv-1)=itmcinf!*rho(j,k)
          q(j,k,nv)=retinf!*rho(j,k)
          uu=   (u(j,k)-ug(j,k)+u(j1,k)-ug(j1,k))*xx(j,k)
          uu=uu+(v(j,k)-vg(j,k)+v(j1,k)-vg(j1,k))*xy(j,k)
          uu=uu*iadd
          if(uu.lt.0.) then
            !q(j,k,7)=q(j1,k,7)
            !q(j,k,8)=q(j1,k,8)
            q(j,k,nv-1)=q(j1,k,nv-1)
            q(j,k,nv)=q(j1,k,nv)
          endif
        enddo
      elseif(iadir.eq.2) then
        k  = ks
        k1 = k + iadd
        do j=js,je
          !q(j,k,7)=itmcinf*rho(j,k)
          !q(j,k,8)=retinf*rho(j,k)
          q(j,k,nv-1)=itmcinf!*rho(j,k)
          q(j,k,nv)=retinf!*rho(j,k)
          uu=   (u(j,k)-ug(j,k)+u(j,k1)-ug(j,k1))*yx(j,k)
          uu=uu+(v(j,k)-vg(j,k)+v(j,k1)-vg(j,k1))*yy(j,k)
          uu=uu*iadd
          if(uu.lt.0.) then
            !q(j,k,7)=q(j,k1,7)
            !q(j,k,8)=q(j,k1,8)
            q(j,k,nv-1)=q(j,k1,nv-1)
            q(j,k,nv)=q(j,k1,nv)
          endif
        enddo
      endif

      return
      end

c*************************************************************
