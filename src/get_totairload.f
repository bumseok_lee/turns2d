C..   Assumes 1st element is slat and 2nd element main element (nmesh=2/3)
C..   Otherwise,1st element main element 
C
      subroutine get_totairload(nmesh,cla,cda,cma,chorda,
     >             xlea,ylea,xtea,ytea,x0,y0,
     >             cl_tot,cd_tot,cm_tot,alfa,bg,
     >             onlyslat,theta_col,mainqc,mainch)

      implicit none

      integer :: nmesh
      logical :: bg,onlyslat,mainqc,mainch
      real    :: x0,y0,cl_tot,cd_tot,cm_tot,alfa,theta_col
      real    :: cla(nmesh),cda(nmesh),cma(nmesh),chorda(nmesh),
     >           xlea(nmesh),ylea(nmesh),xtea(nmesh),ytea(nmesh)
      
      !local variables
      real    :: chord_tot,chord_a,cvecx,cvecy,rbax,rbay,rbcx,rbcy,
     >           x0_new,y0_new,rdox,rdoy,fx,fy,pi,x1lea,y1lea,x1tea,y1tea
      
      pi = 2.*asin(1.0)

      !if(nmesh.gt.1) then
      if( (nmesh.eq.3 .and. bg) .or. (nmesh.eq.2.and.(.not.bg))) then
       chord_a = chorda(2); !sqrt((xlea(2)-xtea(2))**2 + (ylea(2)-ytea(2))**2)

       !chord vector of main element
       cvecx = (xlea(2)-xtea(2))/chord_a
       cvecy = (ylea(2)-ytea(2))/chord_a

       !vector connecting main element te to slat le
       rbax = xlea(1)-xtea(2)
       rbay = ylea(1)-ytea(2)
       !component of rba vec along main element chord vector
       rbcx = (rbax*cvecx+rbay*cvecy)*cvecx
       rbcy = (rbax*cvecx+rbay*cvecy)*cvecy

       !combined chord
       chord_tot=sqrt(rbcx**2+rbcy**2)
       if(chord_tot .le.chord_a) chord_tot = chord_a

       !new quarter chord
       x0_new = xtea(2) + rbcx*0.75
       y0_new = ytea(2) + rbcy*0.75

       !test %for vr7
       if(mainch) chord_tot = 1.0 
       if(mainqc) then
        x0_new = x0
        y0_new = y0
       end if
       !test end
    
       !vector connecting old to new quarter chord
       rdox = x0_new-x0
       rdoy = y0_new-y0
       
       !total airloads
       cl_tot = cla(1)*chorda(1)+cla(2)*chorda(2)
       cd_tot = cda(1)*chorda(1)+cda(2)*chorda(2)

       !total airload force vector

       fx = cd_tot*cos(alfa*pi/180.) - cl_tot*sin(alfa*pi/180.)
       fy = cd_tot*sin(alfa*pi/180.) + cl_tot*cos(alfa*pi/180.)

       cm_tot = cma(1)*chorda(1)**2+cma(2)*chorda(2)**2 + 
     >          rdox*fy - rdoy*fx

       cl_tot = cl_tot/chord_tot
       cd_tot = cd_tot/chord_tot
       cm_tot = cm_tot/chord_tot**2

Ca      print*,'x0_new, y0_new, chord_tot, chord_a ',
Ca     >        x0_new, y0_new, chord_tot, chord_a
       !else if(nmesh.eq.1) then

C special case: only slat in bg grid begins.........
      else if(onlyslat) then
       chord_a = 1.0; !chorda(2); main element
 
       !chord vector of main element
       x1lea = -0.25*cos(theta_col*pi/180.)
       y1lea = 0.25*sin(theta_col*pi/180.)
       x1tea = 0.75*cos(theta_col*pi/180.)
       y1tea = -0.75*sin(theta_col*pi/180.)
       cvecx = (x1lea-x1tea)/chord_a
       cvecy = (y1lea-y1tea)/chord_a

       !vector connecting main element te to slat le
       rbax = xlea(1)-x1tea
       rbay = ylea(1)-y1tea
       !component of rba vec along main element chord vector
       rbcx = (rbax*cvecx+rbay*cvecy)*cvecx
       rbcy = (rbax*cvecx+rbay*cvecy)*cvecy

       !combined chord
       chord_tot=sqrt(rbcx**2+rbcy**2)
       if(chord_tot .le.chord_a) chord_tot = chord_a

       !new quarter chord
       x0_new = x1tea + rbcx*0.75
       y0_new = y1tea + rbcy*0.75

       !vector connecting old to new quarter chord
       rdox = x0_new-x0
       rdoy = y0_new-y0
       
       !total airloads
       cl_tot = cla(1)*chorda(1)!+cla(2)*chorda(2)
       cd_tot = cda(1)*chorda(1)!+cda(2)*chorda(2)

       !total airload force vector

       fx = cd_tot*cos(alfa*pi/180.) - cl_tot*sin(alfa*pi/180.)
       fy = cd_tot*sin(alfa*pi/180.) + cl_tot*cos(alfa*pi/180.)

       cm_tot = cma(1)*chorda(1)**2+!cma(2)*chorda(2) + 
     >          rdox*fy - rdoy*fx

       cl_tot = cl_tot/chord_tot
       cd_tot = cd_tot/chord_tot
       cm_tot = cm_tot/chord_tot**2
C special only slat case ends......

      else
       cl_tot = cla(1)
       cd_tot = cda(1)
       cm_tot = cma(1)
      end if
      
      end subroutine get_totairload
