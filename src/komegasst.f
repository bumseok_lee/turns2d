c***********************************************************************
      subroutine komegasst(q,s,turmu,x,y,xx,xy,yx,yy,ug,vg,jd,kd,
     >		tscale,iblank,im,tau_dim,utau_global)
c  turbulent eddy viscosity. model is one equation spalart-
c  allmaras. ref{ aiaa 92-0439}.
c
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      
      integer im,jd,kd
      real q(jd,kd,nq), s(jd,kd,nv), turmu(jd,kd), tscale(jd,kd)
      real x(jd,kd), y(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real ug(jd,kd), vg(jd,kd) 
      integer iblank(jd,kd)

		! friction variables
		integer tau_dim
		real utau_global(tau_dim)

      ! local variables
      real,allocatable :: bjmat(:,:,:),sjmat(:,:,:)
      real,allocatable :: aj(:,:,:),cj(:,:,:)
      real,allocatable :: ak(:,:,:),ck(:,:,:)
      real,allocatable :: rho(:,:),u(:,:),v(:,:)
      real,allocatable :: vmul(:,:),tauxx(:,:),tauxy(:,:),tauyy(:,:)
      real,allocatable :: ux(:,:),uy(:,:),vx(:,:),vy(:,:)
      real,allocatable :: cdkw(:,:),F1(:,:),F2(:,:),sn(:,:)
      real,allocatable :: vort(:,:),strain(:,:)
      
      integer k,j,n,jloc,kloc
      real relfac,restke,restomega,resmax,oat,tscal,dtpseudo_turb
      real tkelim,tomegalim

      allocate(bjmat(jd,kd,2),sjmat(jd,kd,2))
      allocate(aj(jd,kd,2),cj(jd,kd,2))
      allocate(ak(jd,kd,2),ck(jd,kd,2))
      allocate(rho(jd,kd),u(jd,kd),v(jd,kd))
      allocate(vmul(jd,kd),tauxx(jd,kd),tauxy(jd,kd),tauyy(jd,kd))
      allocate(ux(jd,kd),uy(jd,kd),vx(jd,kd),vy(jd,kd))
      allocate(cdkw(jd,kd),F1(jd,kd),F2(jd,kd),sn(jd,kd))
      allocate(vort(jd,kd),strain(jd,kd))

c***  first executable statement

      tkelim = 1e-20
      tomegalim = 1e-20
c...laminar co-efficient of viscosity calculation
      
      call lamvis(q,vmul,jd,kd)

c...compute velocities, turbulent stress and velocity gradients

      do  k = 1,kmax
      do  j = 1,jmax
        rho(j,k) = q(j,k,1)*q(j,k,nq)
        u(j,k)  = q(j,k,2)/q(j,k,1)
        v(j,k)  = q(j,k,3)/q(j,k,1)
        sn(j,k) = 1.e10
      enddo
      enddo

c...apply boundary condition

      call komegabc(q,vmul,rho,x,y,u,v,ug,vg,xx,xy,yx,yy,im,tau_dim,utau_global)

c...compute distance to wall

      if(bodyflag(im) .or. flatplate) call kwdist(sn,x,y,jd,kd)

c...compute vorticity, strain, turbulent stress and cross-diffusion

      call kwvortic(vort,strain,u,v,xx,xy,yx,yy,jd,kd)
      call turbstress(q,turmu,tauxx,tauxy,tauyy,u,v,ux,uy,vx,vy,
     &                xx,xy,yx,yy,jd,kd)
      call crossdiff(q,rho,cdkw,xx,xy,yx,yy,jd,kd)

c...compute blending function F1
 
      call blend(q,rho,vmul,cdkw,sn,F1,F2,jd,kd)

c...compute eddy viscosity

      call findturm(q,rho,strain,F2,turmu,jd,kd)

c...compute intermittancy factor, if transition model used

      if (itrans.eq.1) then
        call sst_gammatheta(q,s,turmu,sn,vort,xx,xy,yx,yy,ug,vg,
     >                        jd,kd,tscale,iblank,im)
      endif

c...compute rhs and lhs

      call komegarhslhs(q,s,vmul,turmu,tauxx,tauxy,tauyy,cdkw,ux,uy,vx,vy,
     & F1,rho,u,v,ug,vg,xx,xy,yx,yy,aj,bjmat,cj,ak,ck,sjmat,jd,kd,im)

c...invert using DDADI

c..set time-accuracy
      oat = 1.0
      if (ntac.eq.-2) oat = 0.5
      if (ntac.ge.2 .and. istep.gt.1) oat = 2./3.
      if (ntac.eq.3 .and. istep.gt.2) oat = 6./11.

      restke=0.0
      restomega=0.0
      do k = 1,kmax
      do j = 1,jmax

        tscal = tscale(j,k)
        if(timeac.eq.1) then
          dtpseudo_turb=10.0
          tscal = max(iblank(j,k),0)*( 1.0 + 0.002*sqrt(q(j,k,nq)))
     <                       /(1.+sqrt(q(j,k,nq)))
          tscal = tscal*dtpseudo_turb
          tscal = tscal/(1.+tscal/h/oat)
        endif

        do n = 1,2
          aj(j,k,n) = aj(j,k,n)*tscal
          cj(j,k,n) = cj(j,k,n)*tscal
          ak(j,k,n) = ak(j,k,n)*tscal
          ck(j,k,n) = ck(j,k,n)*tscal
          bjmat(j,k,n) = 1.+ bjmat(j,k,n)*tscal
          sjmat(j,k,n) = sjmat(j,k,n)*tscal
        enddo
      enddo
      enddo

      call lsolvej2(aj,bjmat,cj,sjmat,jd,kd)

      do k = 1,kmax
      do j = 1,jmax
        sjmat(j,k,1) = sjmat(j,k,1)*bjmat(j,k,1)
        sjmat(j,k,2) = sjmat(j,k,2)*bjmat(j,k,2)
      enddo
      enddo

      call lsolvek2(ak,bjmat,ck,sjmat,jd,kd)

      relfac = 1.
      resmax = 0.
      do k = 1,kmax
      do j = 1,jmax
        s(j,k,5) = relfac*sjmat(j,k,1)
        s(j,k,6) = relfac*sjmat(j,k,2)

        q(j,k,5) = q(j,k,5) + s(j,k,5)*max(iblank(j,k),0)
        q(j,k,6) = q(j,k,6) + s(j,k,6)*max(iblank(j,k),0)

        q(j,k,5) = max(q(j,k,5),tkelim*rho(j,k))
        q(j,k,6) = max(q(j,k,6),tomegalim*rho(j,k))

        restke = restke + s(j,k,5)**2
        restomega = restomega + s(j,k,6)**2
        if (max(restke,restomega).gt.resmax) then
          jloc = j
          kloc = k
          resmax = max(restke,restomega)
        endif
      enddo
      enddo

      restke = sqrt(restke/jmax/kmax)
      restomega = sqrt(restomega/jmax/kmax)
      resmax = sqrt(resmax/jmax/kmax)
      if( mod(istep,npnorm).eq.0) then
         write(1233+im,*),istep0,restke,restomega,resmax
      endif
c
c...apply boundary condition again

      call komegabc(q,vmul,rho,x,y,u,v,ug,vg,xx,xy,yx,yy,im,tau_dim,utau_global)

c...compute eddy viscosity

      call findturm(q,rho,strain,F2,turmu,jd,kd)

      return
      end

c*************************************************************
      subroutine findturm(q,rho,strain,F2,turmu,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real q(jd,kd,nq),rho(jd,kd),strain(jd,kd),F2(jd,kd),turmu(jd,kd)
      ! local variables
      integer j,k
      real a1,rei
 
      data a1 /0.31/
      
      rei = 1./rey
 
      do k   = 1,kmax
      do j   = 1,jmax
        turmu(j,k) = a1*rho(j,k)*q(j,k,5)/
     &                max(a1*q(j,k,6),F2(j,k)*rho(j,k)*strain(j,k)*rei)
        turmu(j,k) = max(turmu(j,k),1e-20)
      enddo
      enddo

      return
      end

c*************************************************************
        subroutine kwdist(sn,x,y,jd,kd)
c
c     calculate distance to solid surface
c     not rigorous now
c     
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        integer jd,kd
        real sn(jd,kd)
        real x(jd,kd),y(jd,kd)

        ! local variables
        integer j,k

c***  first executable statement
       
	if(flatplate) then

		k=1
		do j = jle_flatplate,jmax
			sn(j,k) = 0.0
		enddo

		do k = 1,kmax
		do j = 1,jle_flatplate-1
			sn(j,k) = sqrt((x(j,k)-x(jle_flatplate,1))**2+(y(j,k)-y(jle_flatplate,1))**2)
		enddo
		enddo

		do k = 2,kmax
		do j = jle_flatplate,jmax
			sn(j,k) = sqrt((x(j,k)-x(j,1))**2+(y(j,k)-y(j,1))**2)
		enddo
		enddo

	else

        k=1
        do j=jtail1,jtail2
        sn(j,k)=0.0
        enddo
        
        do j=jtail1,jtail2
        do k=2,kmax
          sn(j,k)=sqrt((x(j,k)-x(j,1))**2+(y(j,k)-y(j,1))**2)
        enddo
        enddo

        do j=1,jtail1-1
        do k=1,kmax
c          sn(j,k)=sqrt((x(j,k)-x(jtail1,1))**2
c     &           +(y(j,k)-y(jtail1,1))**2)
          sn(j,k)=1.e10
        enddo
        enddo

        do j=jtail2+1,jmax
        do k=1,kmax
c          sn(j,k)=sqrt((x(j,k)-x(jtail1,1))**2
c     &           +(y(j,k)-y(jtail1,1))**2)
          sn(j,k)=1.e10
        enddo
        enddo

		 endif

        return
        end

c*************************************************************
        subroutine kwvortic(vort,strain,u,v,xx,xy,yx,yy,jd,kd)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        integer jd,kd
        real vort(jd,kd),strain(jd,kd)
        real xx(jd,kd),xy(jd,kd),yx(jd,kd),yy(jd,kd)
        real u(jd,kd),v(jd,kd)

        ! local variables
        integer j,k,jp,jm1,kp,km1
        real ux,vx,uy,vy
        real usi,vsi,ueta,veta
        real sxx,sxy,syy

c**   first executable statement

        do j = 2,jmax-1 
          jp = j + 1
          jm1 = j - 1
          do k = 2,kmax-1
            kp = k + 1
            km1 = k - 1

            usi  = 0.5*(u(jp,k)-u(jm1,k))
            vsi  = 0.5*(v(jp,k)-v(jm1,k))
            
            ueta = 0.5*(u(j,kp)-u(j,km1))
            veta = 0.5*(v(j,kp)-v(j,km1))
              
            ux = xx(j,k)*usi + yx(j,k)*ueta
            uy = xy(j,k)*usi + yy(j,k)*ueta

            vx = xx(j,k)*vsi + yx(j,k)*veta
            vy = xy(j,k)*vsi + yy(j,k)*veta

            sxx = ux
            sxy = 0.5*(uy + vx)
            syy = vy

            vort(j,k) = abs(uy - vx)
            strain(j,k) = sqrt(2.*(sxx*sxx + 2.*sxy*sxy + syy*syy))
          enddo
        enddo

        return
        end

c*************************************************************
      subroutine turbstress(q,turmu,tauxx,tauxy,tauyy,
     &                   u,v,ux,uy,vx,vy,xx,xy,yx,yy,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real q(jd,kd,nq),turmu(jd,kd)
      real tauxx(jd,kd),tauxy(jd,kd),tauyy(jd,kd)
      real u(jd,kd),v(jd,kd)
      real ux(jd,kd),uy(jd,kd),vx(jd,kd),vy(jd,kd)
      real xx(jd,kd),xy(jd,kd),yx(jd,kd),yy(jd,kd)

! local variables
      integer j,k,jm1,km1,jp,kp
      real usi,vsi,ueta,veta,sfdiv,rei

      rei = 1./rey
      do j = 2,jd-1 
        jp = j + 1
        jm1 = j - 1
        do k = 2,kd-1
          kp = k + 1
          km1 = k - 1

          usi  = 0.5*(u(jp,k)-u(jm1,k))
          vsi  = 0.5*(v(jp,k)-v(jm1,k))
          
          ueta = 0.5*(u(j,kp)-u(j,km1))
          veta = 0.5*(v(j,kp)-v(j,km1))
            
          ux(j,k) = xx(j,k)*usi + yx(j,k)*ueta
          uy(j,k) = xy(j,k)*usi + yy(j,k)*ueta

          vx(j,k) = xx(j,k)*vsi + yx(j,k)*veta
          vy(j,k) = xy(j,k)*vsi + yy(j,k)*veta

          sfdiv = 2./3*(ux(j,k) + vy(j,k))

          tauxx(j,k) = turmu(j,k)* rei * ( 2.0 * ux(j,k) - sfdiv )
     &                            -2./3*q(j,k,5)
          tauyy(j,k) = turmu(j,k)* rei * ( 2.0 * vy(j,k) - sfdiv )
     &                            -2./3*q(j,k,5)
          tauxy(j,k) = turmu(j,k)* rei * ( uy(j,k) + vx(j,k) )

        enddo
      enddo

      return
      end

c***********************************************************************
      subroutine crossdiff(q,rho,cdkw,xx,xy,yx,yy,jd,kd)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      integer jd,kd
      real q(jd,kd,nq),rho(jd,kd),cdkw(jd,kd)
      real xx(jd,kd),xy(jd,kd),yx(jd,kd),yy(jd,kd)

      ! local variables
      integer j,k,n,jp,kp,jm1,km1
      real ksi,keta,omegasi,omegaeta,kx,ky,omegax,omegay

      include 'komega.h'

      do j = 2,jm
        jp = j+1
        jm1 = j-1
        do k = 2,km
          kp = k+1
          km1 = k-1
          ksi = 0.5*(q(jp,k,5)/rho(jp,k)-q(jm1,k,5)/rho(jm1,k))
          keta = 0.5*(q(j,kp,5)/rho(j,kp)-q(j,km1,5)/rho(j,km1))

          omegasi = 0.5*(q(jp,k,6)/rho(jp,k)-q(jm1,k,6)/rho(jm1,k))
          omegaeta = 0.5*(q(j,kp,6)/rho(j,kp)-q(j,km1,6)/rho(j,km1))

          kx = ksi*xx(j,k) + keta*yx(j,k)
          ky = ksi*xy(j,k) + keta*yy(j,k)
          
          omegax = omegasi*xx(j,k) + omegaeta*yx(j,k)
          omegay = omegasi*xy(j,k) + omegaeta*yy(j,k)

          cdkw(j,k) = kx*omegax + ky*omegay
          cdkw(j,k) = cdkw(j,k)*2*sigmaw2*rho(j,k)*rho(j,k)/q(j,k,6)
        enddo
      enddo

      return
      end

c***********************************************************************
      subroutine blend(q,rho,vmul,cdkw,sn,F1,F2,jd,kd)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      integer jd,kd
      real q(jd,kd,nq),rho(jd,kd),vmul(jd,kd),cdkw(jd,kd),sn(jd,kd)
      real F1(jd,kd),F2(jd,kd)

      ! local variables
      integer j,k,n,jp,kp,jm1,km1
      real a1,a2,a3,rei,arg1,arg2,cd
      real Ry,F3

      include 'komega.h'

      rei = 1./rey
      do j = 2,jm
      do k = 2,km
        a1 = rei*sqrt(q(j,k,5)*rho(j,k))/(0.09*q(j,k,6)*sn(j,k))
        a2 = 500*rei*rei*vmul(j,k)/(q(j,k,6)*sn(j,k)**2)
        cd = max(cdkw(j,k),1e-10)
        a3 = 4*q(j,k,5)*sigmaw2/(cd*sn(j,k)**2)
        arg1 = min(max(a1,a2),a3)
        arg2 = max(2*a1,a2)
        F1(j,k) = tanh(arg1**4) 
        F2(j,k) = tanh(arg2**2) 
      enddo
      enddo

c...Modified blending function, F1 when transition model is used

!      if (itrans.eq.1) then
!        do j = 2,jm
!        do k = 2,km
!          Ry = sn(j,k)*sqrt(rho(j,k)*q(j,k,5))*rey/vmul(j,k)
!          F3 = exp(-(Ry/120)**8)
!          F1(j,k) = max(F1(j,k),F3)
!        enddo
!        enddo
!      endif

      return
      end

c***********************************************************************
      subroutine komegarhslhs(q,s,vmul,turmu,tauxx,tauxy,tauyy,cdkw,
     &                        ux,uy,vx,vy,F1,rho,u,v,ug,vg,xx,xy,yx,yy,
     &                        aj,bjmat,cj,ak,ck,sjmat,jd,kd,im)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************

      integer jd,kd,im
      real q(jd,kd,nq), s(jd,kd,nv),rho(jd,kd)
      real vmul(jd,kd), turmu(jd,kd)
      real tauxx(jd,kd),tauxy(jd,kd),tauyy(jd,kd),cdkw(jd,kd)
      real ux(jd,kd),uy(jd,kd),vx(jd,kd),vy(jd,kd)
      real F1(jd,kd)
      real u(jd,kd),v(jd,kd),ug(jd,kd),vg(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real aj(jd,kd,2),bjmat(jd,kd,2),cj(jd,kd,2)
      real ak(jd,kd,2),ck(jd,kd,2),sjmat(jd,kd,2)

      ! local variables
      integer j,k,n,jp,kp,jm1,km1
      real uu,vv,fwd,bck
      real,allocatable :: up(:),um(:),vp(:),vm(:)

      allocate(up(mdim),um(mdim),vp(mdim),vm(mdim))

c***  first executable statement

      do k = 1,kmax
      do j = 1,jmax
        do n = 1,2
          aj(j,k,n)=0.0
          bjmat(j,k,n)=0.0
          cj(j,k,n)=0.0
          ak(j,k,n)=0.0
          ck(j,k,n)=0.0
          sjmat(j,k,n)=s(j,k,n+nmv)
        enddo
      enddo
      enddo

c...lhs contribution from the convection term

      do k=2,kmax-1
        do j=1,jmax
          uu=xx(j,k)*(u(j,k)-ug(j,k))+xy(j,k)*(v(j,k)-vg(j,k))
          up(j) = 0.5*(uu+abs(uu))
          um(j) = 0.5*(uu-abs(uu))
        enddo

        do j = 2,jmax-1
          jp = j + 1
          jm1 = j - 1

          if(up(j).gt.1.0e-12) then
            fwd=1.0
          else
            fwd=0.0
          endif

          if(um(j).lt.-1.0e-12) then
            bck=1.0
          else
            bck=0.0
          endif

          sjmat(j,k,1) = sjmat(j,k,1) - up(j)*(q(j,k,5) - q(jm1,k,5)) - 
     &                          um(j)*(q(jp,k,5) - q(j,k,5))
          sjmat(j,k,2) = sjmat(j,k,2) - up(j)*(q(j,k,6) - q(jm1,k,6)) - 
     &                          um(j)*(q(jp,k,6) - q(j,k,6))
          do n = 1,2
            aj(j,k,n) = aj(j,k,n) - up(j)!fwd*(up(jm1)+um(jm1))
            cj(j,k,n) = cj(j,k,n) + um(j)!bck*(up(jp)+um(jp))
            bjmat(j,k,n) = bjmat(j,k,n) + up(j)- um(j)
          enddo
        enddo
      enddo

      do j=2,jmax-1
        do k=1,kmax
          vv = yx(j,k)*(u(j,k)-ug(j,k))+yy(j,k)*(v(j,k)-vg(j,k))
          vp(k) = 0.5*(vv+abs(vv))
          vm(k) = 0.5*(vv-abs(vv))
        enddo

        do k = 2,kmax-1
          kp = k + 1
          km1 = k - 1

          if(vp(k).gt.1.0e-12) then
            fwd=1.0
          else
            fwd=0.0
          endif

          if(vm(k).lt.-1.0e-12) then
            bck=1.0
          else
            bck=0.0
          endif

          sjmat(j,k,1) = sjmat(j,k,1) - vp(k)*(q(j,k,5) - q(j,km1,5)) -
     &                          vm(k)*(q(j,kp,5) - q(j,k,5))
          sjmat(j,k,2) = sjmat(j,k,2) - vp(k)*(q(j,k,6) - q(j,km1,6)) -
     &                          vm(k)*(q(j,kp,6) - q(j,k,6))
          do n = 1,2
            ak(j,k,n) = ak(j,k,n) - vp(k) !fwd*(vp(km1)+vm(km1))
            ck(j,k,n) = ck(j,k,n) + vm(k) !bck*(vp(kp)+vm(kp))
            bjmat(j,k,n) = bjmat(j,k,n) + vp(k) - vm(k)
          enddo
        enddo
      enddo
   
c...rhs and lhs contribution from the source term

Ca      if(bodyflag(im))
      call komegasource(q,rho,turmu,tauxx,tauxy,tauyy,cdkw,ux,uy,vx,vy,
     &                  F1,aj,bjmat,cj,ak,ck,sjmat,jd,kd)


c...rhs contribution from the diffusion term

      call komegadiffus(q,rho,vmul,turmu,F1,xx,xy,yx,yy,
     &                  aj,bjmat,cj,ak,ck,sjmat,jd,kd)

      return
      end

c***********************************************************************
      subroutine komegadiffus(q,rho,vmul,turmu,F1,xx,xy,yx,yy,
     &                               aj,bjmat,cj,ak,ck,sjmat,jd,kd)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
      integer jd,kd
      real q(jd,kd,nq),rho(jd,kd)
      real vmul(jd,kd),turmu(jd,kd)
      real F1(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real aj(jd,kd,2),bjmat(jd,kd,2),cj(jd,kd,2)
      real ak(jd,kd,2),ck(jd,kd,2),sjmat(jd,kd,2)
      
    ! local variables
      integer j,k,j1,k1

      real,allocatable :: chp(:,:),dnuhp(:,:)
      real,allocatable :: dmxh(:,:),dmyh(:,:)

      real rei,vnulh,vnuh,dxp,dxm,c2,dcp,dcm,ax,cx,dyp,dym,ay,cy
      real sigmak,sigmaw

      include 'komega.h'

      allocate(chp(jd,kd),dnuhp(jd,kd))
      allocate(dmxh(jd,kd),dmyh(jd,kd))

c***  first executable statement

        rei = 1./rey
c       compute j direction differences.

c       compute half-point co-efficients

        do k=1,kmax
        do j=1,jmax-1
          dmxh(j,k) = 0.5*(xx(j,k)+xx(j+1,k))
          dmyh(j,k) = 0.5*(xy(j,k)+xy(j+1,k))
        enddo
        enddo

c     j-direction diffusion for k-equation

        do k=1,kmax
        do j=1,jmax-1
          vnulh     = 0.5*(vmul(j,k)+vmul(j+1,k))
          vnuh     = 0.5*(turmu(j,k)+turmu(j+1,k))
          sigmak   = F1(j,k)*sigmak1 + (1.-F1(j,k))*sigmak2
          chp(j,k) = rei*(vnulh+sigmak*vnuh)       
          dnuhp(j,k) =q(j+1,k,5)/rho(j+1,k)-q(j,k,5)/rho(j,k)
        enddo
        enddo

        do k=1,kmax
        do j=2,jmax-1

          dxp=dmxh(j,k)*xx(j,k)+dmyh(j,k)*xy(j,k)
          dxm=dmxh(j-1,k)*xx(j,k)+dmyh(j-1,k)*xy(j,k)

c         enforce positivity (as suggested by overflow)
          
          dcp    = dxp*(chp(j,k))
          dcm    = dxm*(chp(j-1,k))
          ax=0.0
          cx=0.0

          if(k.ne.kmax.and.k.ne.1) then
            ax       = max(dcm,0.0)
            cx       = max(dcp,0.0)
          endif
c          compute fluxes.

          sjmat(j,k,1)=sjmat(j,k,1)-ax*dnuhp(j-1,k)+cx*dnuhp(j,k)

c          jacobian terms

          aj(j,k,1) = aj(j,k,1) - ax/rho(j-1,k)
          cj(j,k,1) = cj(j,k,1) - cx/rho(j+1,k)
          bjmat(j,k,1) = bjmat(j,k,1)+ (ax + cx)/rho(j,k)

        enddo
        enddo

c      j-direction diffusion for omega-equation

        do k=1,kmax
        do j=1,jmax-1
          vnulh     = 0.5*(vmul(j,k)+vmul(j+1,k))
          vnuh     = 0.5*(turmu(j,k)+turmu(j+1,k))
          sigmaw   = F1(j,k)*sigmaw1 + (1.-F1(j,k))*sigmaw2
          chp(j,k) = rei*(vnulh+sigmaw*vnuh)       
          dnuhp(j,k) =q(j+1,k,6)/rho(j+1,k)-q(j,k,6)/rho(j,k)
        enddo
        enddo

        do k=1,kmax
        do j=2,jmax-1

          dxp=dmxh(j,k)*xx(j,k)+dmyh(j,k)*xy(j,k)
          dxm=dmxh(j-1,k)*xx(j,k)+dmyh(j-1,k)*xy(j,k)

c         enforce positivity (as suggested by overflow)
          
          dcp    = dxp*(chp(j,k))
          dcm    = dxm*(chp(j-1,k))
          ax=0.0
          cx=0.0

          if(k.ne.kmax.and.k.ne.1) then
            ax       = max(dcm,0.0)
            cx       = max(dcp,0.0)
          endif
c          compute fluxes.

          sjmat(j,k,2)=sjmat(j,k,2)-ax*dnuhp(j-1,k)+cx*dnuhp(j,k)

c          jacobian terms

          aj(j,k,2) = aj(j,k,2) - ax/rho(j-1,k)
          cj(j,k,2) = cj(j,k,2) - cx/rho(j+1,k)
          bjmat(j,k,2) = bjmat(j,k,2)+ (ax + cx)/rho(j,k)

        enddo
        enddo

c       compute k direction differences.

c       compute half-point co-efficients

        do k=1,kmax-1
        do j=1,jmax
          dmxh(j,k) = 0.5*(yx(j,k)+yx(j,k+1))
          dmyh(j,k) = 0.5*(yy(j,k)+yy(j,k+1))
       enddo
       enddo

c     k-direction diffusion for k-equation

        do k=1,kmax-1
        do j=1,jmax
          vnulh     = 0.5*(vmul(j,k)+vmul(j,k+1))
          vnuh     =  0.5*(turmu(j,k)+turmu(j,k+1))
          sigmak   = F1(j,k)*sigmak1 + (1.-F1(j,k))*sigmak2
          chp(j,k) =rei*(vnulh+sigmak*vnuh)       
          dnuhp(j,k) =q(j,k+1,5)/rho(j,k+1)-q(j,k,5)/rho(j,k)
       enddo
       enddo

       do k=2,kmax-1
       do j=1,jmax

         dyp=dmxh(j,k)*yx(j,k)+dmyh(j,k)*yy(j,k)
         dym=dmxh(j,k-1)*yx(j,k)+dmyh(j,k-1)*yy(j,k)

c      enforce positivity (as suggested by overflow)
       
          dcp    = dyp*(chp(j,k))
          dcm    = dym*(chp(j,k-1))

          ay=0.0
          cy=0.0
          if(j.ne.1.and.j.ne.jmax) then
            ay       = max(dcm,0.0)
            cy       = max(dcp,0.0)
          endif

c        compute fluxes.

          sjmat(j,k,1)=sjmat(j,k,1)-ay*dnuhp(j,k-1)+cy*dnuhp(j,k)

c        jacobian terms

          ak(j,k,1) = ak(j,k,1) - ay/rho(j,k-1)
          ck(j,k,1) = ck(j,k,1) - cy/rho(j,k+1)
          bjmat(j,k,1) = bjmat(j,k,1)+ (ay + cy)/rho(j,k)

        enddo
        enddo

c     k-direction diffusion for omega-equation

        do k=1,kmax-1
        do j=1,jmax
          vnulh     = 0.5*(vmul(j,k)+vmul(j,k+1))
          vnuh     =  0.5*(turmu(j,k)+turmu(j,k+1))
          sigmaw   = F1(j,k)*sigmaw1 + (1.-F1(j,k))*sigmaw2
          chp(j,k) =rei*(vnulh+sigmaw*vnuh)       
          dnuhp(j,k) =q(j,k+1,6)/rho(j,k+1)-q(j,k,6)/rho(j,k)
       enddo
       enddo

       do k=2,kmax-1
       do j=1,jmax

         dyp=dmxh(j,k)*yx(j,k)+dmyh(j,k)*yy(j,k)
         dym=dmxh(j,k-1)*yx(j,k)+dmyh(j,k-1)*yy(j,k)

c      enforce positivity (as suggested by overflow)
       
          dcp    = dyp*(chp(j,k))
          dcm    = dym*(chp(j,k-1))

          ay=0.0
          cy=0.0
          if(j.ne.1.and.j.ne.jmax) then
            ay       = max(dcm,0.0)
            cy       = max(dcp,0.0)
          endif

c        compute fluxes.

          sjmat(j,k,2)=sjmat(j,k,2)-ay*dnuhp(j,k-1)+cy*dnuhp(j,k)

c        jacobian terms

          ak(j,k,2) = ak(j,k,2) - ay/rho(j,k-1)
          ck(j,k,2) = ck(j,k,2) - cy/rho(j,k+1)
          bjmat(j,k,2) = bjmat(j,k,2)+ (ay + cy)/rho(j,k)

        enddo
        enddo

      return
      end

c***********************************************************************
        subroutine komegasource(q,rho,turmu,tauxx,tauxy,tauyy,cdkw,ux,uy,vx,vy,
     &                          F1,aj,bjmat,cj,ak,ck,sjmat,jd,kd)
c***********************************************************************
        use params_global
c***********************************************************************
        implicit none
        integer jd,kd
        real q(jd,kd,nq),rho(jd,kd),turmu(jd,kd)
        real tauxx(jd,kd),tauxy(jd,kd),tauyy(jd,kd),cdkw(jd,kd)
        real ux(jd,kd),uy(jd,kd),vx(jd,kd),vy(jd,kd)
        real F1(jd,kd)
        real aj(jd,kd,2),bjmat(jd,kd,2),cj(jd,kd,2)
        real ak(jd,kd,2),ck(jd,kd,2),sjmat(jd,kd,2)
        
        ! local variables
        integer j,k
        real rei,prod,kprod,kdes,omegades,omegacrossdiff,ksust,omegasust
        real prodlim,gammap,beta
        
        include 'komega.h'

c**   first executable statement

        rei = 1./rey

        do k=2,kmax-1
        do j=2,jmax-1
          prod = tauxx(j,k)*ux(j,k) + tauxy(j,k)*(uy(j,k) + vx(j,k)) + 
     &           tauyy(j,k)*vy(j,k)
          kdes = betastar*rey*q(j,k,6)*q(j,k,5)/rho(j,k)
          ksust = betastar*rey*tkeinf*tomegainf*rho(j,k)
          prodlim = 20.*kdes
          prod = min(prod,prodlim)
          kprod = prod

          beta = F1(j,k)*beta1 + (1-F1(j,k))*beta2 
          omegades = beta*rey*q(j,k,6)*q(j,k,6)/rho(j,k)
          omegasust = beta*rey*tomegainf**2*rho(j,k)
          omegacrossdiff = (1-F1(j,k))*cdkw(j,k)*rei

c...modification for transition model

          if (itrans.eq.1) then
            kprod = q(j,k,nv-1)*prod
				omegasust = 0.0
            !kdes = min(max(q(j,k,7)/rho(j,k),0.1),1.0)*kdes
            ksust = 0.0 !q(j,k,nv-1)*ksust
          endif

          sjmat(j,k,1) = sjmat(j,k,1) + kprod - kdes + ksust
          gammap = F1(j,k)*gamma1 + (1-F1(j,k))*gamma2 
          sjmat(j,k,2) = sjmat(j,k,2) + gammap*rho(j,k)/turmu(j,k)*prod   
     &                        - omegades + omegacrossdiff + omegasust
          bjmat(j,k,1) = bjmat(j,k,1) + kdes/q(j,k,5) 
          bjmat(j,k,2) = bjmat(j,k,2) + (2*omegades+abs(omegacrossdiff))/q(j,k,6) 
        enddo
        enddo

        return
        end

c*************************************************************
        subroutine komegabc(q,vmul,rho,x,y,u,v,ug,vg,xx,xy,yx,yy,im,tau_dim,utau_global)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        real q(jmax,kmax,nq),vmul(jmax,kmax),rho(jmax,kmax),x(jmax,kmax),y(jmax,kmax)
        real u(jmax,kmax),v(jmax,kmax),ug(jmax,kmax),vg(jmax,kmax)
        real xx(jmax,kmax),xy(jmax,kmax),yx(jmax,kmax),yy(jmax,kmax)
        integer im

		! friction variables
		integer tau_dim
		real utau_global(tau_dim)

c..   local variables
        integer js,je,ks,ke,idir
        integer j,k,ib

        do ib=1,nbc_all(im)
          js = jbcs_all(ib,im)
          je = jbce_all(ib,im)
          ks = kbcs_all(ib,im)
          ke = kbce_all(ib,im)
          if(js.lt.0) js = jmax+js+1
          if(ks.lt.0) ks = kmax+ks+1
          if(je.lt.0) je = jmax+je+1
          if(ke.lt.0) ke = kmax+ke+1
          idir = ibdir_all(ib,im)

c.. crude outflow bc (shivaji)
          if (ibtyp_all(ib,im).eq.53) then
            call komegabc_outflow(q,js,je,ks,ke,idir)

c.. wall bc at l = 1 (only interior portion of wall)
          elseif (ibtyp_all(ib,im).eq.4.or.ibtyp_all(ib,im).eq.5) then
            call komegabc_wall(q,vmul,rho,x,y,js,je,ks,ke,idir,tau_dim,utau_global)

c.. inviscid wind tunnel wall bc
          elseif (ibtyp_all(ib,im).eq.3) then
            call komegabc_extpt(q,js,je,ks,ke,idir)

c.. symmetric bc
          elseif (ibtyp_all(ib,im).eq.11) then
            call komegabc_sym(q,js,je,ks,ke,idir)

c.. periodic bc
          elseif (ibtyp_all(ib,im).eq.22) then
            call komegabc_periodic(q,js,je,ks,ke,idir)

c.. averaging bc for wake
          elseif (ibtyp_all(ib,im).eq.51) then
            call komegabc_wake(q,js,je,ks,ke,idir)

c.. averaging bc for wake of O-grid
          elseif (ibtyp_all(ib,im).eq.52) then
            call komegabc_wake_ogrid(q,js,je,ks,ke,idir)

c.. freesream bc
          elseif (ibtyp_all(ib,im).eq.47) then
            call komegabc_out(q,rho,u,v,ug,vg,xx,xy,yx,yy,js,je,ks,ke,idir)

          endif
        enddo

        return
        end

c*************************************************************
      subroutine komegabc_wall(q,vmul,rho,x,y,js,je,ks,ke,idir,tau_dim,utau_global)
c*************************************************************
      use params_global
      use roughness
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq),vmul(jmax,kmax),rho(jmax,kmax),x(jmax,kmax),y(jmax,kmax)
      integer js,je,ks,ke,idir

		! friction variables
		integer tau_dim
		real utau_global(tau_dim)

c.. local variables

      integer j,k,j1,k1,iadd,iadir
      real rei,dely2,t,scal,qwall5,qwall6
		real hs_plus

      include 'komega.h'

      iadd = sign(1,idir)
      iadir = abs(idir)

      t = (float(istep0)-1.)/30.
      if(t.gt.1..or.iread.gt.0) t=1.
      scal = (10.-15.*t+6.*t*t)*t**3

      rei = 1./rey

		if(iadir.eq.1) then
		  j = js
		  j1 = j + iadd
		  do k=ks,ke
			 dely2 = (x(j1,k)-x(j,k))**2 + (y(j1,k)-y(j,k))**2
			 qwall5 = 0.0
			 qwall6 = 60.*vmul(j,k)*rei*rei/(beta1*dely2)
			 q(j,k,5) = scal*qwall5 + (1.-scal)*q(j,k,5)
			 q(j,k,6) = scal*qwall6 + (1.-scal)*q(j,k,6)
		  enddo
		  if(irough) then
			  print*,'no roughness bc implemented in komega for wall bc_dir = +/-1'
			  print*,'terminating simulation'
			  stop
		  endif
		elseif(iadir.eq.2) then
		  k = ks
		  k1 = k + iadd
		  do j=js,je
		    if(irough.and.hsand(j).gt.0.0) then
            hs_plus = utau_global(j)*hsand(j)*rho(j,k)/vmul(j,k)*sqrt(rey)
            hs_plus = max(hs_plus,1.e-4)
            dely2 = (x(j,k1)-x(j,k))**2 + (y(j,k1)-y(j,k))**2
            qwall5 = 0.0
            if(hs_plus<25.0) then
              qwall6 = 2500.*vmul(j,k)*rei*rei/(rho(j,k)*hsand(j)*hsand(j))
            else
           	  qwall6 = 100.0*utau_global(j)/(hsand(j)*rey**1.5)
            endif
            q(j,k,5) = scal*qwall5 + (1.-scal)*q(j,k,5)
            q(j,k,6) = scal*qwall6 + (1.-scal)*q(j,k,6)
          else !smooth or hsand.eq.0
            dely2 = (x(j,k1)-x(j,k))**2 + (y(j,k1)-y(j,k))**2
            qwall5 = 0.0
            qwall6 = 60.*vmul(j,k)*rei*rei/(beta1*dely2)
            q(j,k,5) = scal*qwall5 + (1.-scal)*q(j,k,5)
            q(j,k,6) = scal*qwall6 + (1.-scal)*q(j,k,6)
			 endif
		  enddo
		endif

      return
      end

c*************************************************************
      subroutine komegabc_outflow(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,5) = q(j1,k,5)
          q(j,k,6) = q(j1,k,6)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,5) = q(j,k1,5)
          q(j,k,6) = q(j,k1,6)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine komegabc_extpt(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,5) = q(j1,k,5)
          q(j,k,6) = q(j1,k,6)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,5) = q(j,k1,5)
          q(j,k,6) = q(j,k1,6)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine komegabc_sym(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,5) = q(j1,k,5)
          q(j,k,6) = q(j1,k,6)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,5) = q(j,k1,5)
          q(j,k,6) = q(j,k1,6)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine komegabc_periodic(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jc,jj,jj1,kc,kk,kk1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(idir.eq.1) then
        jj  = je - js + 1
        do j = js,je
           jj1 = j - js
           jc = jmax - 2*jj + jj1
 
           do k = ks,ke
             q(j,k,5) = q(jc,k,5)
             q(j,k,6) = q(jc,k,6)
           enddo
        enddo

      elseif(idir.eq.-1) then
        jj  = je - js + 1
        do j = js,je
           jj1 = je - j
           jc = 1 + 2*jj - jj1
 
           do k = ks,ke
             q(j,k,5) = q(jc,k,5)
             q(j,k,6) = q(jc,k,6)
           enddo
        enddo

      elseif(idir.eq.2) then
        kk  = ke - ks + 1
        do k = ks,ke
           kk1 = k - ks
           kc = kmax - 2*kk + kk1
 
           do j = js,je
             q(j,k,5) = q(j,kc,5)
             q(j,k,6) = q(j,kc,6)
           enddo
        enddo

      elseif(idir.eq.-2) then
        kk  = ke - ks + 1
        do k = ks,ke
           kk1 = ke - k
           kc = 1 + 2*kk - kk1 
 
           do j = js,je
             q(j,k,5) = q(j,kc,5)
             q(j,k,6) = q(j,kc,6)
           enddo
        enddo

      endif

      return
      end

c*************************************************************
      subroutine komegabc_wake(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jj,j1,k1,iadd,iadir
      real qav1,qav2

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        print*,'idir = ',idir,' is not implemented in komegabc_wake'
      elseif(iadir.eq.2) then
        k  = ks
        k1 = k + iadd
        do j=js,je
          jj = jmax - j + 1
          qav1 = 0.5*(q(j,k1,5)+q(jj,k1,5))
          qav2 = 0.5*(q(j,k1,6)+q(jj,k1,6))
          q(j,k,5)  = qav1
          q(j,k,6)  = qav2
          q(jj,k,5) = qav1
          q(jj,k,6) = qav2
        enddo
      endif

      return
      end

c*************************************************************
      subroutine komegabc_wake_ogrid(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jj,j1,iadd,iadir
      real qav1,qav2

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j  = js
        j1 = j + iadd
        jj = jmax - j1 + 1
        do k=ks,ke
          qav1 = 0.5*(q(j1,k,5)+q(jj,k,5))
          qav2 = 0.5*(q(j1,k,6)+q(jj,k,6))
          q(j,k,5) = qav1
          q(j,k,6) = qav2
        enddo
      elseif(iadir.eq.2) then
      print*,'idir = ',idir,' is not implemented in komegabc_wake_ogrid'
      endif

      return
      end

c*************************************************************
      subroutine komegabc_out(q,rho,u,v,ug,vg,xx,xy,yx,yy,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq),rho(jmax,kmax),u(jmax,kmax),v(jmax,kmax)
      real ug(jmax,kmax),vg(jmax,kmax)
      real xx(jmax,kmax),xy(jmax,kmax),yx(jmax,kmax),yy(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir
      real uu

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j  = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,5)=tkeinf*rho(j,k)
          q(j,k,6)=tomegainf*rho(j,k)
          uu=   (u(j,k)-ug(j,k)+u(j1,k)-ug(j1,k))*xx(j,k)
          uu=uu+(v(j,k)-vg(j,k)+v(j1,k)-vg(j1,k))*xy(j,k)
          uu=uu*iadd
          if(uu.lt.0.) then
            q(j,k,5)=q(j1,k,5)
            q(j,k,6)=q(j1,k,6)
          endif
        enddo
      elseif(iadir.eq.2) then
        k  = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,5)=tkeinf*rho(j,k)
          q(j,k,6)=tomegainf*rho(j,k)
          uu=   (u(j,k)-ug(j,k)+u(j,k1)-ug(j,k1))*yx(j,k)
          uu=uu+(v(j,k)-vg(j,k)+v(j,k1)-vg(j,k1))*yy(j,k)
          uu=uu*iadd
          if(uu.lt.0.) then
            q(j,k,5)=q(j,k1,5)
            q(j,k,6)=q(j,k1,6)
          endif
        enddo
      endif

      return
      end

c*************************************************************
        subroutine lsolvej2(a,b,c,s,jd,kd)
c*************************************************************
        use params_global
c*************************************************************
        
        integer jd,kd

        real a(jd,kd,2),b(jd,kd,2),c(jd,kd,2)
        real s(jd,kd,2)

        ! local variables

        integer j,k,n
        real bb

c***  first executable statement
 
        do n = 1,2
          do k = 2,km
            bb       = 1./b(2,k,n)
            s(2,k,n)  = s(2,k,n)*bb
            c(2,k,n)  = c(2,k,n)*bb
          enddo
 
          do  j = 3,jm
          do  k = 2,km
            bb      = 1./(b(j,k,n) - a(j,k,n)*c(j-1,k,n))
            s(j,k,n)  = (s(j,k,n) - a(j,k,n)*s(j-1,k,n))*bb
            c(j,k,n)  = c(j,k,n)*bb
          enddo
          enddo

          do j = jm-1,2,-1
          do k = 2,km
            s(j,k,n)    = s(j,k,n) - c(j,k,n)*s(j+1,k,n)
          enddo
          enddo
        enddo

        return
        end

c*************************************************************
        subroutine lsolvek2(a,b,c,s,jd,kd)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        integer jd,kd
        real a(jd,kd,2),b(jd,kd,2),c(jd,kd,2)
        real s(jd,kd,2)

        ! local variables

        integer j,k,n
        real bb

c***  first executable statement

        do n = 1,2
          do j = 2,jm
            bb       = 1./b(j,2,n)
            s(j,2,n)  = s(j,2,n)*bb
            c(j,2,n)  = c(j,2,n)*bb
          enddo
 
          do  k = 3,km
          do  j = 2,jm
            bb      = 1./(b(j,k,n) - a(j,k,n)*c(j,k-1,n))
            s(j,k,n)  = (s(j,k,n) - a(j,k,n)*s(j,k-1,n))*bb
            c(j,k,n)  = c(j,k,n)*bb
          enddo
          enddo

          do k = km-1,2,-1
          do j = 2,jm
            s(j,k,n)    = s(j,k,n) - c(j,k,n)*s(j,k+1,n)
          enddo
          enddo
        enddo

         return
         end

c*************************************************************
