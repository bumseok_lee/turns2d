!***********************************************************************
      module rough_trans
      use params_global
      use roughness
      implicit none

      public :: wall_shear
      public :: amp_rough

      real,pointer,public :: ar(:,:)
      integer,allocatable,private :: check_alloc(:)

      contains
!***********************************************************************
      subroutine init_amp_rough
!***********************************************************************
      integer i,k,j,l


      allocate(check_alloc(1))
      allocate(ar(jmax,kmax))
      print*, 'check allocation!', istep0

      ar = 0.0

      end subroutine init_amp_rough

!***********************************************************************
      subroutine amp_rough(kpp,f_ar,rho,vort,vmul,turmu,jd,kd,&
                 u,v,ug,vg,xx,xy,yx,yy,tscale,iblank,im)
!***********************************************************************
      use params_global
!***********************************************************************
      implicit none
!***********************************************************************
      integer jd,kd,im
      real kpp(jd),f_ar(jd,kd),rho(jd,kd),vort(jd,kd)
      real u(jd,kd), v(jd,kd), ug(jd,kd), vg(jd,kd)
      real xx(jd,kd),xy(jd,kd),yx(jd,kd),yy(jd,kd)
      real vmul(jd,kd),turmu(jd,kd),tscale(jd,kd)
      integer iblank(jd,kd)
!***********************************************************************
      ! local variables
      integer j,k,k1
      real cr1, cr2, cr3, car, sigma_ar
      real,allocatable :: bjmat(:,:),sjmat(:,:)
      real,allocatable :: aj(:,:),cj(:,:)
      real,allocatable :: ak(:,:),ck(:,:)

      allocate(bjmat(jd,kd),sjmat(jd,kd))
      allocate(aj(jd,kd),cj(jd,kd))
      allocate(ak(jd,kd),ck(jd,kd))

      if(.not.allocated(check_alloc)) then
      call init_amp_rough
      endif

      cr1 = 8.0
      cr2 = 0.0005
      cr3 = 2.0
      sigma_ar = 10.0
      car = sqrt(cr3/(3.0*cr2))

      ! BC at the wall
      k  = 1
      k1 = k+1
      do j=jtail1,jtail2
        if(hsand(j).gt.0.0) then
        ar(j,k) = kpp(j)*cr1
        else
        ar(j,k) = ar(j,k1)
        endif
      enddo

      ! solve transport eq using DDADI
      call amp_rhslhs(ar,sigma_ar,rho,u,v,ug,vg,xx,xy,yx,yy, &
           vmul,turmu,aj,bjmat,cj,ak,ck,sjmat,tscale,&
           iblank,jd,kd,im)

      ! BC at the wall
      k  = 1
      k1 = k+1
      do j=jtail1,jtail2
        if(hsand(j).gt.0.0) then
        ar(j,k) = kpp(j)*cr1
        else
        ar(j,k) = ar(j,k1)
        endif
      enddo
      
      ! calculate f_ar
      do j=1,jmax
      do k=1,kmax
        if(ar(j,k).lt.car) then
          f_ar(j,k) = cr2*ar(j,k)**3
        else
          f_ar(j,k) = cr3*(ar(j,k)-car) + cr2*car**3
        endif
      enddo
      enddo


      end subroutine amp_rough

!***********************************************************************
      subroutine amp_rhslhs(ar,sigma_ar,rho,u,v,ug,vg,xx,xy,yx,yy,&
                 vmul,turmu,aj,bjmat,cj,ak,ck,sjmat,tscale,&
                 iblank,jd,kd,im)
!***********************************************************************
      use params_global
!***********************************************************************
      implicit none
!***********************************************************************
      integer jd,kd,im
      real ar(jd,kd),rho(jd,kd), sigma_ar
      real u(jd,kd),v(jd,kd),ug(jd,kd),vg(jd,kd)
      real xx(jd,kd),xy(jd,kd),yx(jd,kd),yy(jd,kd)
      real vmul(jd,kd),turmu(jd,kd)
      real aj(jd,kd),bjmat(jd,kd),cj(jd,kd)
      real ak(jd,kd),ck(jd,kd),sjmat(jd,kd)
      real tscale(jd,kd)
      integer iblank(jd,kd)
      real tscal, dtpseudo_turb
!***********************************************************************
      integer j,k,n,jp,kp,jm1,km1
      real uu,vv,fwd,bck
      real,allocatable :: up(:),um(:),vp(:),vm(:)
      real resar,resmax,relfac
      integer jloc,kloc

      allocate(up(mdim),um(mdim),vp(mdim),vm(mdim))

!***  first executable statement

      do k = 1,kmax
      do j = 1,jmax
          aj(j,k)=0.0
          bjmat(j,k)=0.0
          cj(j,k)=0.0
          ak(j,k)=0.0
          ck(j,k)=0.0
          sjmat(j,k) = 0.
      enddo
      enddo

!...lhs contribution from the convection term

      do k=2,kmax-1
        do j=1,jmax
          uu=xx(j,k)*(u(j,k)-ug(j,k))+xy(j,k)*(v(j,k)-vg(j,k))
          up(j) = 0.5*(uu+abs(uu))
          um(j) = 0.5*(uu-abs(uu))
        enddo

        do j = 2,jmax-1
          jp = j + 1
          jm1 = j - 1

          if(up(j).gt.1.0e-12) then
            fwd=1.0
          else
            fwd=0.0
          endif

          if(um(j).lt.-1.0e-12) then
            bck=1.0
          else
            bck=0.0
          endif

          sjmat(j,k) = sjmat(j,k) - up(j)*(ar(j, k) - ar(jm1,k)) & 
                                  - um(j)*(ar(jp,k) - ar(  j,k))
         
          aj(j,k) = aj(j,k) - up(j)!fwd*(up(jm1)+um(jm1))
          cj(j,k) = cj(j,k) + um(j)!bck*(up(jp)+um(jp))
          bjmat(j,k) = bjmat(j,k) + up(j)- um(j)
        enddo
      enddo

      do j=2,jmax-1
        do k=1,kmax
          vv = yx(j,k)*(u(j,k)-ug(j,k))+yy(j,k)*(v(j,k)-vg(j,k))
          vp(k) = 0.5*(vv+abs(vv))
          vm(k) = 0.5*(vv-abs(vv))
        enddo

        do k = 2,kmax-1
          kp = k + 1
          km1 = k - 1

          if(vp(k).gt.1.0e-12) then
            fwd=1.0
          else
            fwd=0.0
          endif

          if(vm(k).lt.-1.0e-12) then
            bck=1.0
          else
            bck=0.0
          endif

          sjmat(j,k) = sjmat(j,k) - vp(k)*(ar(j, k)- ar(j,km1)) &
                                  - vm(k)*(ar(j,kp)- ar(j,  k))
         
          ak(j,k) = ak(j,k) - vp(k) !fwd*(vp(km1)+vm(km1))
          ck(j,k) = ck(j,k) + vm(k) !bck*(vp(kp)+vm(kp))
          bjmat(j,k) = bjmat(j,k) + vp(k) - vm(k)
        enddo
      enddo


!...rhs contribution from the diffusion term
      call amp_diffus(ar,sigma_ar,rho,vmul,turmu,&
           xx,xy,yx,yy,aj,bjmat,cj,ak,ck,sjmat,jd,kd)


!...invert using DDADI
      do k = 1,kmax
      do j = 1,jmax
        tscal = tscale(j,k)
        aj(j,k) = aj(j,k)*tscal
        cj(j,k) = cj(j,k)*tscal
        ak(j,k) = ak(j,k)*tscal
        ck(j,k) = ck(j,k)*tscal
        bjmat(j,k) = 1.+ bjmat(j,k)*tscal
        sjmat(j,k)  = sjmat(j,k)*tscal
      enddo
      enddo

      call lsolvej(aj,bjmat,cj,sjmat,jd,kd)

      do k = 1,kmax
      do j = 1,jmax
        sjmat(j,k) = sjmat(j,k)*bjmat(j,k)
      enddo
      enddo

      call lsolvek(ak,bjmat,ck,sjmat,jd,kd)                                                         

      relfac = 1.
      resar  = 0.0;  resmax = 0.0;

      do k = 1,kmax
      do j = 1,jmax
        sjmat(j,k) = relfac*sjmat(j,k)
        ar(j,k) = ar(j,k) + sjmat(j,k)*max(iblank(j,k),0)
        ar(j,k) = max(ar(j,k),1e-20)

        resar = resar + sjmat(j,k)**2

        if (resar.gt.resmax) then
          jloc = j
          kloc = k
          resmax = resar
        endif

      enddo
      enddo

      resar = sqrt(resar/jmax/kmax)
      if(mod(istep,npnorm).eq.0) then
         write(1433+im,61) float(istep0),resar
      endif

 61   FORMAT (2(x,E14.6))



 
      end subroutine amp_rhslhs


!***********************************************************************
      subroutine amp_diffus(ar,sigma_ar,rho,vmul,turmu,&
           xx,xy,yx,yy,aj,bjmat,cj,ak,ck,sjmat,jd,kd)
!***********************************************************************
      use params_global
!***********************************************************************
      implicit none
!***********************************************************************
      integer jd,kd
      real ar(jd,kd),rho(jd,kd),vmul(jd,kd),turmu(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real aj(jd,kd),bjmat(jd,kd),cj(jd,kd)
      real ak(jd,kd),ck(jd,kd),sjmat(jd,kd)
      real sigma_ar

!***********************************************************************
      ! local variables
      integer j,k,j1,k1
      real,allocatable :: chp(:,:),dnuhp(:,:)
      real,allocatable :: dmxh(:,:),dmyh(:,:)

      real rei,vnulh,vnuh,dxp,dxm,c2,dcp,dcm,ax,cx,dyp,dym,ay,cy

      allocate(chp(jd,kd),dnuhp(jd,kd))
      allocate(dmxh(jd,kd),dmyh(jd,kd))

!***  first executable statement
      rei = 1./rey
!     compute j direction differences.

!     compute half-point co-efficients
      do k=1,kmax
      do j=1,jmax-1
        dmxh(j,k) = 0.5*(xx(j,k)+xx(j+1,k))
        dmyh(j,k) = 0.5*(xy(j,k)+xy(j+1,k))
      enddo
      enddo

!     j-direction diffusion for ar-equation
      do k=1,kmax
      do j=1,jmax-1
        vnulh    = 0.5*(vmul(j,k)+vmul(j+1,k))
        vnuh     = 0.5*(turmu(j,k)+turmu(j+1,k))
        chp(j,k) = rei*sigma_ar*(vnulh+vnuh)/rho(j,k)
        dnuhp(j,k) =ar(j+1,k)-ar(j,k)
      enddo
      enddo

      do k=1,kmax
      do j=2,jmax-1
        dxp=dmxh(j,k)*xx(j,k)+dmyh(j,k)*xy(j,k)
        dxm=dmxh(j-1,k)*xx(j,k)+dmyh(j-1,k)*xy(j,k)

!       enforce positivity (as suggested by overflow)
        dcp    = dxp*(chp(j,k))
        dcm    = dxm*(chp(j-1,k))
        ax=0.0
        cx=0.0

        if(k.ne.kmax.and.k.ne.1) then
          ax       = max(dcm,0.0)
          cx       = max(dcp,0.0)
        endif

!       compute fluxes.
        sjmat(j,k)=sjmat(j,k)-ax*dnuhp(j-1,k)+cx*dnuhp(j,k)

!       jacobian terms
        aj(j,k) = aj(j,k) - ax
        cj(j,k) = cj(j,k) - cx
        bjmat(j,k) = bjmat(j,k)+ (ax + cx)

      enddo
      enddo

!..   Compute k direction differences.

!     compute half-point co-efficients
      do k=1,kmax-1
      do j=1,jmax
        dmxh(j,k) = 0.5*(yx(j,k)+yx(j,k+1))
        dmyh(j,k) = 0.5*(yy(j,k)+yy(j,k+1))
      enddo
      enddo

!     k-direction diffusion for ar-equation
      do k=1,kmax-1
      do j=1,jmax
        vnulh    = 0.5*(vmul(j,k)+vmul(j,k+1))
        vnuh     = 0.5*(turmu(j,k)+turmu(j,k+1))
        chp(j,k) = rei*sigma_ar*(vnulh+vnuh)/rho(j,k)  
        dnuhp(j,k) = ar(j,k+1)-ar(j,k)
      enddo
      enddo

      do k=2,kmax-1
      do j=1,jmax

        dyp=dmxh(j,k)*yx(j,k)+dmyh(j,k)*yy(j,k)
        dym=dmxh(j,k-1)*yx(j,k)+dmyh(j,k-1)*yy(j,k)

!       enforce positivity (as suggested by overflow)
        dcp    = dyp*(chp(j,k))
        dcm    = dym*(chp(j,k-1))

        ay=0.0
        cy=0.0
        if(j.ne.1.and.j.ne.jmax) then
          ay       = max(dcm,0.0)
          cy       = max(dcp,0.0)
        endif

!       compute fluxes.
        sjmat(j,k)=sjmat(j,k)-ay*dnuhp(j,k-1)+cy*dnuhp(j,k)

!       jacobian terms
        ak(j,k) = ak(j,k) - ay
        ck(j,k) = ck(j,k) - cy
        bjmat(j,k) = bjmat(j,k)+ (ay + cy)

      enddo
      enddo

      return


      end subroutine amp_diffus
!***********************************************************************
      subroutine wall_shear(q,xx,xy,yx,yy,tauw,kpp,vmul,turmu,jd,kd)

! Note: mostly borrow from force routine
!***********************************************************************
      use params_global
!***********************************************************************
      implicit none
!***********************************************************************
      integer jd,kd
      real q(jd,kd,nq)
      real xx(jd,kd),xy(jd,kd),yx(jd,kd),yy(jd,kd)
      real tauw(jd),kpp(jd), vmul(jd,kd), turmu(jd,kd)
!***********************************************************************
      integer j,j1,j2,jp1,jm1,ja,jb, k,k2,k3
      real amu
      real uxi,vxi,ueta,veta
      real xix,xiy,etax,etay
      real rho


      j1 = jtail1
      j2 = jtail2

      k  = 1
      k2 = k+1 
      k3 = k+2

      j = j1
      jp1 = j+1
      amu   = (vmul(j,k) + turmu(j,k))/rey
      uxi = q(jp1,k,2)/q(jp1,k,1)-q(j,k,2)/q(j,k,1)
      vxi = q(jp1,k,3)/q(jp1,k,1)-q(j,k,3)/q(j,k,1)
      ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k2,2)/q(j,k2,1)&
            -.5*q(j,k3,2)/q(j,k3,1)
      veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k2,3)/q(j,k2,1)&
            -.5*q(j,k3,3)/q(j,k3,1)
      xix = xx(j,k)
      xiy = xy(j,k)
      etax = yx(j,k)
      etay = yy(j,k)
      tauw(j)= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))

      j = j2
      jm1 = j-1
      amu   = (vmul(j,k) + turmu(j,k))/rey
      uxi = q(j,k,2)/q(j,k,1)-q(jm1,k,2)/q(jm1,k,1)
      vxi = q(j,k,3)/q(j,k,1)-q(jm1,k,3)/q(jm1,k,1)
      ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k2,2)/q(j,k2,1)&
            -.5*q(j,k3,2)/q(j,k3,1)
      veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k2,3)/q(j,k2,1)&
            -.5*q(j,k3,3)/q(j,k3,1)
      xix = xx(j,k)
      xiy = xy(j,k)
      etax = yx(j,k)
      etay = yy(j,k)
      tauw(j)= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))

      ja = j1+1
      jb = j2-1
      do 110 j = ja,jb
        amu   = (vmul(j,k) + turmu(j,k))/rey
        jp1 = j+1
        jm1 = j-1
        uxi=.5*(q(jp1,k,2)/q(jp1,k,1)-q(jm1,k,2)/q(jm1,k,1))
        vxi=.5*(q(jp1,k,3)/q(jp1,k,1)-q(jm1,k,3)/q(jm1,k,1))
        ueta= -1.5*q(j,k,2)/q(j,k,1)+2.*q(j,k2,2)/q(j,k2,1)&
                    -.5*q(j,k3,2)/q(j,k3,1)
        veta= -1.5*q(j,k,3)/q(j,k,1)+2.*q(j,k2,3)/q(j,k2,1)&
                    -.5*q(j,k3,3)/q(j,k3,1)
        xix = xx(j,k)
        xiy = xy(j,k)
        etax = yx(j,k)
        etay = yy(j,k)
        tauw(j)= amu*((uxi*xiy+ueta*etay)-(vxi*xix+veta*etax))

110     continue

        k=1
        do j=jtail1,jtail2
        rho = q(j,k,1)*q(j,k,nq)
        !kpp(j)=sqrt(tauw(j)/rho)*hsand(j)/(vmul(j,k)+turmu(j,k))*rho*rey
        kpp(j)=sqrt(abs(tauw(j))/rho)*hsand(j)/(vmul(j,k)+turmu(j,k))*rho*rey
        enddo

      end subroutine wall_shear

!***********************************************************************
      end module rough_trans

