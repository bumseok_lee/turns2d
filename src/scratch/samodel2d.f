c***********************************************************************
      subroutine vmu_sa(x,y,q,turmu,xx,xy,yx,yy,ug,vg,jd,kd,vnu,
     >		vnu0,tscale,iblank,im,tau_dim,tau_global,utau_global)
c  turbulent eddy viscosity. model is one equation spalart-
c  allmaras. ref{ aiaa 92-0439}.
c
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      
      integer im,jd,kd
      real q(jd,kd,nq), turmu(jd,kd), tscale(jd,kd)
      real x(jd,kd), y(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real ug(jd,kd), vg(jd,kd) 
      real vnu(jd,kd),vnu0(jd,kd)
      integer iblank(jd,kd)

		! friction variables
		integer tau_dim
		real tau_global(tau_dim),utau_global(tau_dim)

      ! local variables
      real,allocatable :: s(:,:),bjmat(:,:)
      real,allocatable :: aj(:,:),bj(:,:),cj(:,:)
      real,allocatable :: ak(:,:),bk(:,:),ck(:,:)
      real,allocatable :: vmul(:,:),vort(:,:),strain(:,:)
		real,allocatable :: sn(:,:),sn_des(:,:)
      real,allocatable :: u(:,:),v(:,:),rho(:,:)
      real,allocatable :: s1(:,:),s2(:,:)
      real,allocatable :: fv1(:,:),trip(:,:)
      
      
      integer k,j,jloc,kloc,niter,ihuang,logv,nhuang
      real relfac,vnulim,dmaxtur,dl2norm
      real resl2,tscal,dtpseudo_turb

		! rough wall variables
		real hs_rough,d0_rough

      allocate(s(jd,kd),bjmat(jd,kd))
      allocate(aj(jd,kd),bj(jd,kd),cj(jd,kd))
      allocate(ak(jd,kd),bk(jd,kd),ck(jd,kd))
      allocate(vmul(jd,kd),vort(jd,kd),strain(jd,kd))
		allocate(sn(jd,kd))
		allocate(sn_des(jd,kd))
      allocate(u(jd,kd),v(jd,kd),rho(jd,kd))
      allocate(s1(jd,kd),s2(jd,kd))
      allocate(fv1(jd,kd),trip(jd,kd))
c***  first executable statement

	relfac=1.0
	vnulim=1.0e-20
	nturiter=1
	nhuang=1

	! rough wall parameters
	hs_rough = hsand
	d0_rough = 0.03*hs_rough

c.........laminar co-efficient of viscosity calculation
      
	  call lamvis(q,vmul,jd,kd)

c.........for compatibility with turbulence model


	do  k = 1,kmax
        do  j = 1,jmax
         rho(j,k)  = q(j,k,1)*q(j,k,nq)
         u(j,k)  = q(j,k,2)/q(j,k,1)
         v(j,k)  = q(j,k,3)/q(j,k,1)
			 s(j,k)  =0.0
         vort(j,k) = 0.0
         sn(j,k) =1.e10
         sn_des(j,k) =1.e10
	enddo
	enddo

	call c_turm(fv1,turmu,vnu,q,jd,kd)

        call turbc(vnu,u,v,ug,vg,sn,d0_rough,xx,xy,yx,yy,im)

c.........compute vorticity & distance function

	  
	   if(bodyflag(im) .or. flatplate) then 
		 call dist(sn,sn_des,x,y,jd,kd,d0_rough)
	    call vortic(vort,strain,rho,u,v,xx,xy,yx,yy,jd,kd,sn,sn_des,turmu)
      endif
		
 	  call c_fv1(vnu,q,vmul,fv1,jd,kd)
   
  	  call c_turm(fv1,turmu,vnu,q,jd,kd)

	  call ftrip(q,vort,sn,trip,u,v,jd,kd)


        dmaxtur=0.0
        do k = 2,kmax-1
        do j = 2,jmax-1
        if(vnu(j,k).gt.dmaxtur) then
                dmaxtur=vnu(j,k)
                jloc=j
                kloc=k
        endif
        enddo
        enddo
         !write(1235,*) istep0,dmaxtur,jloc,kloc




 	do 30 niter=1,nturiter

!...compute intermittancy factor, if transition model used

        if (itrans.eq.1) then
          call sa_gammatheta(q,turmu,sn,vort,xx,xy,yx,yy,ug,vg, 
     &          jd,kd,tscale,iblank,im)
!        else 
!          q(:,:,nv-1) = 1.
!          q(:,:,nv) = 0.
        endif

        call rhslhs(q,fv1,sn,vort,strain,u,v,vnu,vmul,turmu,trip,s,
     &	xx,xy,yx,yy,ug,vg,x,y,aj,bj,cj,ak,bk,ck,jd,kd,hs_rough,
     &   tau_dim,tau_global,utau_global)	  

 	if(itnmax.gt.1) call newtn(vnu,vnu0,s,q,jd,kd)

c	scale by time-step

	dl2norm=0.0

	
        do k = 1,kmax
        do j = 1,jmax
	dl2norm=dl2norm+s(j,k)*s(j,k)
	tscal = tscale(j,k)

	if(cfltur.gt.1.and.idual_turb.eq.1) then
 	  dtpseudo_turb=cfltur/(bj(j,k)+bk(j,k))
 	  tscal=dtpseudo_turb*float(iblank(j,k))
          tscal=tscal/(1.+tscal/h)
	endif

        if(timeac.eq.1) then
          dtpseudo_turb=1.0
          tscal = max(iblank(j,k),0)*( 1.0 + 0.002*sqrt(q(j,k,nq)))
     <                       /(1.+sqrt(q(j,k,nq)))
          tscal = tscal*dtpseudo_turb
          tscal = tscal/(1.+tscal/h)
        endif

        s(j,k)  = s(j,k)*tscal
        aj(j,k) = aj(j,k)*tscal
        cj(j,k) = cj(j,k)*tscal
        ak(j,k) = ak(j,k)*tscal
        ck(j,k) = ck(j,k)*tscal
        bjmat(j,k) = 1.+(bj(j,k)+bk(j,k))*tscal
        s1(j,k) = 0.
        s2(j,k) = 0.
        enddo
        enddo


	do ihuang=1,nhuang
        if(ihuang.gt.1) then
           do k = 1,kmax
           do j = 1,jmax
           s(j,k)  = bjmat(j,k)*(s(j,k)-s1(j,k))
           enddo
           enddo
        endif
        call lsolvej(aj,bjmat,cj,s,jd,kd)
        do k = 1,kmax
        do j = 1,jmax
        s1(j,k) = s1(j,k)+s(j,k)
        s(j,k)  = (s1(j,k)-s2(j,k))*bjmat(j,k)
        enddo
        enddo
        call lsolvek(ak,bjmat,ck,s,jd,kd)
        do k = 1,kmax
        do j = 1,jmax
        s2(j,k)  = s2(j,k)+s(j,k)
        s(j,k)  = s2(j,k)
        enddo
        enddo

	enddo

	resl2=0.0	
      do k = 2,kmax-1
      do j = 2,jmax-1
         s(j,k) = relfac*s(j,k)
         vnu(j,k) = max( (vnu(j,k) + s(j,k)*max(iblank(j,k),0)),vnulim)
	 resl2=resl2+s(j,k)*s(j,k)
      enddo
      enddo
	resl2=sqrt(resl2/jmax/kmax)
	dl2norm=sqrt(dl2norm/jmax/kmax)
!	if(niter.eq.nturiter) write(1233+im,*)istep0,resl2,dl2norm
	
        call turbc(vnu,u,v,ug,vg,sn,d0_rough,xx,xy,yx,yy,im)

30	continue

 	call c_fv1(vnu,q,vmul,fv1,jd,kd)

	call c_turm(fv1,turmu,vnu,q,jd,kd)

      return
      end


c*************************************************************
	subroutine lamvis(q,vmul,jd,kd)
c
c     calculate laminar viscosity
c
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************

        integer jd,kd
        real q(jd,kd,nq),vmul(jd,kd)
        
        ! local variables
        
        integer k,j
        real gkpr,prtr,dre,c2b,c2bp,ra,uvel,vvel
        real ei,tt

c***  first executable statement
        

	gkpr = gamma/pr
        prtr = pr/0.9
        dre  = .5/rey
        c2b  =198.6/tinf
        c2bp = c2b +1.

        do 10 k = 1,kmax
        do 10 j = 1,jmax
           ra    = 1./q(j,k,1)
           uvel  = q(j,k,2)*ra
           vvel  = q(j,k,3)*ra
           ei    =   q(j,k,4)*ra-0.5*(uvel**2+vvel**2)
           tt    = ggm1*ei
           vmul(j,k)  = c2bp*tt*sqrt(tt)/(c2b + tt)
10      continue

      
	return
        end




c*************************************************************
        subroutine dist(sn,sn_des,x,y,jd,kd,d0_rough)
c
c     calculate distance to solid surface
c     not rigorous now
c     
c*************************************************************
        use params_global
c*************************************************************
        implicit none
		  include 'sadata.h'
c*************************************************************
        integer jd,kd
        real sn(jd,kd),sn_des(jd,kd)
        real x(jd,kd),y(jd,kd)
		  real d0_rough

        ! local variables
        integer j,k
		  real(kind=8) :: s_left,s_right,s_bottom,s_top,dx,dy

c***  first executable statement
	
	if(flatplate) then

		k=1
		do j = jle_flatplate,jmax
			sn(j,k) = 0.0
		enddo

		do k = 1,kmax
		do j = 1,jle_flatplate-1
			sn(j,k) = sqrt((x(j,k)-x(jle_flatplate,1))**2+(y(j,k)-y(jle_flatplate,1))**2)
		enddo
		enddo

		do k = 2,kmax
		do j = jle_flatplate,jmax
			sn(j,k) = sqrt((x(j,k)-x(j,1))**2+(y(j,k)-y(j,1))**2)
		enddo
		enddo

	else

	k=1
	do j=jtail1,jtail2
	sn(j,k)=0.0
	enddo

	do j=jtail1,jtail2
	do k=2,kmax
	sn(j,k)=sqrt((x(j,k)-x(j,1))**2+(y(j,k)-y(j,1))**2)
	enddo
	enddo

      if(jtail1.ne.1) then
	      do j=1,jtail1-1
	      do k=1,kmax
	      sn(j,k)=sqrt((x(j,k)-x(jtail1,1))**2
     & 		+(y(j,k)-y(jtail1,1))**2)
	      enddo
	      enddo
      endif

      if(jtail2.ne.jmax) then
	      do j=jtail2+1,jmax
	      do k=1,kmax
	      sn(j,k)=sqrt((x(j,k)-x(jtail1,1))**2
     & 		+(y(j,k)-y(jtail1,1))**2)
	      enddo
	      enddo
      endif

	if(ides.ne.0) then
		do k = 2,kmax-1
		do j = 2,jmax-1

			dx = x(j,k)-x(j-1,k)
			dy = y(j,k)-y(j-1,k)
			s_left = sqrt(dx*dx+dy*dy)

			dx = x(j+1,k)-x(j,k)
			dy = y(j+1,k)-y(j,k)
			s_right = sqrt(dx*dx+dy*dy)

			dx = x(j,k)-x(j,k-1)
			dy = y(j,k)-y(j,k-1)
			s_bottom = sqrt(dx*dx+dy*dy)

			dx = x(j,k+1)-x(j,k)
			dy = y(j,k+1)-y(j,k)
			s_top = sqrt(dx*dx+dy*dy)

			sn_des(j,k) = c_des*max(s_left,s_right,s_bottom,s_top)

		enddo
		enddo

	endif

	endif

	return
	end


c*************************************************************
	subroutine vortic(vort,strain,rho,u,v,xx,xy,yx,yy,jd,kd,sn,sn_des,turmu)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        integer jd,kd
 	real vort(jd,kd),strain(jd,kd)
	real turmu(jd,kd),sn(jd,kd),sn_des(jd,kd)
	real xx(jd,kd),xy(jd,kd),
     &	yx(jd,kd),yy(jd,kd)
        real rho(jd,kd),u(jd,kd),v(jd,kd)

        ! local variables
        integer j,k
        real dx2,dy2,ux,vx,uy,vy,tx,sxx,syy,sxy,rdes,rdes1,fdes,fdes1

		  real du_dxi,du_deta,dv_dxi,dv_deta,du_dx,du_dy,dv_dx,dv_dy,var1

c**   first executable statement

        dx2     = 0.5
        dy2     = 0.5
	
	do 10  k   = 2,kmax-1
	do 10  j   = 2,jmax-1

	  ux  = (u(j+1,k) - u(j-1,k)) * dx2
          vx  = (v(j+1,k) - v(j-1,k)) * dx2
 
	  uy  = (u(j,k+1) - u(j,k-1)) * dy2
	  vy  = (v(j,k+1) - v(j,k-1)) * dy2

          tx  =  xy(j,k)*ux -xx(j,k)*vx +yy(j,k)*uy -yx(j,k)*vy
 
          vort(j,k) = abs(tx)

			if(use_sarc) then
			sxx = xx(j,k)*ux + yx(j,k)*uy
			syy = xy(j,k)*vx + yy(j,k)*vy
			sxy = 0.5*(xy(j,k)*ux + yy(j,k)*uy + xx(j,k)*vx + yx(j,k)*vy)
			strain(j,k) = sqrt(2.0*(sxx*sxx + syy*syy + 2.0*sxy*sxy))

			vort(j,k) = vort(j,k) + 2.0*min(0.0,strain(j,k)-vort(j,k))
			endif


10	continue

			if(ides==1) then
				fdes = 1
				do j = 2,jmax-1
				do k = 2,kmax-1
					sn(j,k) = sn(j,k) - fdes*max(0.0,sn(j,k) - sn_des(j,k))
				enddo
				enddo

			elseif(ides==2) then

                open(41, file="f_des_data.txt", status="replace",action="write")
				do j = 2,jmax-1
				do k = 2,kmax-1

					du_dxi = 0.5*(u(j+1,k)-u(j-1,k))
					dv_dxi = 0.5*(v(j+1,k)-v(j-1,k))

					du_deta = 0.5*(u(j,k+1)-u(j,k-1))
					dv_deta = 0.5*(v(j,k+1)-v(j,k-1))

					du_dx = du_dxi*xx(j,k) + du_deta*yx(j,k)
					du_dy = du_dxi*xy(j,k) + du_deta*yy(j,k)

					dv_dx = dv_dxi*xx(j,k) + dv_deta*yx(j,k)
					dv_dy = dv_dxi*xy(j,k) + dv_deta*yy(j,k)

					var1 = max(sqrt( du_dx*du_dx + du_dy*du_dy + dv_dx*dv_dx + dv_dy*dv_dy ),1.e-16)

					rdes = (turmu(j,k)/rho(j,k)+1.0)/(var1*0.41*0.41*sn(j,k)*sn(j,k)*rey)
					fdes = 1.0 - tanh((8.0*rdes)**3.0)

					!rdes1 = 1.0/(strain(j,k)*0.41*0.41*sn(j,k)**2.0*rey)
					!fdes1 = 1.0 - tanh((8.0*rdes1)**3.0)

					!write(252,51)k,rdes1,rdes,fdes,fdes1
					!write(253,*)k,strain(j,k),vort(j,k)

					!sn(j,k) = sn(j,k) - fdes*max(0.0,sn(j,k) - sn_des(j,k))
                    if (j==225)then
                        write(41,614)rdes,fdes,0.0
                    endif
				enddo
				enddo
                close(41)
			endif

 51			format(i5,4(x,f16.12))

	return
        end
	  


c*************************************************************
	 subroutine c_fv1(vnu,q,vmul,fv1,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      
      integer jd,kd
      real vnu(jd,kd),q(jd,kd,nq)
      real vmul(jd,kd),fv1(jd,kd)
      
      ! local variables
      integer j,k
      real chi,vnul

      include 'sadata.h'

c***  first executable statement
      
      do 10  k   = 1,kmax
         do 10  j   = 1,jmax
            vnul	=vmul(j,k)/(q(j,k,1)*q(j,k,nq))
            chi	=vnu(j,k)/vnul	
            fv1(j,k)=chi**3/(chi**3+cv1**3)
 10      continue
         
         
      return
      end





c***********************************************************************
      subroutine ftrip(q,vort,sn,trip,u,v,jd,kd)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c*********************************************************************

      integer jd,kd
      real q(jd,kd,nq),u(jd,kd),v(jd,kd)
      real x(jd,kd), y(jd,kd)
      real vort(jd,kd),sn(jd,kd)
      real trip(jd,kd)

      include 'sadata.h'

      ! local variables
      integer j1,j2,j,k
      real dx2,dy2
      
c***  first executable statement
      
      j1      = jtail1
      j2      = jtail2
      dx2     = 0.5
      dy2     = 0.5


        do 10  k   = 1,kmax
        do 10  j   = 1,jmax
	trip(j,k)=0.0
10	continue

	return
	end
	  


c*************************************************************
	subroutine c_turm(fv1,turmu,vnu,q,jd,kd)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        integer jd,kd
        real vnu(jd,kd),q(jd,kd,nq)
        real turmu(jd,kd),fv1(jd,kd)
        ! local variables
        integer j,k
        

        do 10  k   = 1,kmax
        do 10  j   = 1,jmax
	turmu(j,k)=q(j,k,1)*q(j,k,nq)*vnu(j,k)*fv1(j,k)
10      continue

      return
      end


c***********************************************************************
      subroutine rhslhs(q,fv1,sn,vort,strain,u,v,vnu,vmul,turmu,trip,s,
     &	xx,xy,yx,yy,ug,vg,x,y,aj,bj,cj,ak,bk,ck,jd,kd,hs_rough,
     &   tau_dim,tau_global,utau_global)	  
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************

      integer jd,kd
      real q(jd,kd,nq),s(jd,kd)
      real x(jd,kd), y(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real ug(jd,kd), vg(jd,kd)
      real aj(jd,kd),bj(jd,kd),cj(jd,kd)
      real ak(jd,kd),bk(jd,kd),ck(jd,kd)
      real vmul(jd,kd),vort(jd,kd),strain(jd,kd),sn(jd,kd)
      real u(jd,kd),v(jd,kd),vnu(jd,kd),turmu(jd,kd)
      real fv1(jd,kd),trip(jd,kd)
		real hs_rough

		! friction variables
		integer tau_dim
		real tau_global(tau_dim),utau_global(tau_dim)

      ! local variables
      integer j,k

c***  first executable statement

	do k = 1,kmax
        do j = 1,jmax
	aj(j,k)=0.0
	bj(j,k)=0.0
	cj(j,k)=0.0
	ak(j,k)=0.0
	bk(j,k)=0.0
	ck(j,k)=0.0
    	enddo
    	enddo


        call convec(q,vnu,fv1,sn,u,v,s,vmul,
     &  xx,xy,yx,yy,ug,vg,x,y,aj,bj,cj,ak,bk,ck,jd,kd)

        call diffus(q,vnu,fv1,sn,u,v,s,vmul,
     &  xx,xy,yx,yy,x,y,aj,bj,cj,ak,bk,ck,jd,kd)


        call source(q,vnu,turmu,sn,u,v,s,vmul,vort,strain,
     &  xx,xy,yx,yy,x,y,aj,bj,cj,ak,bk,ck,jd,kd,hs_rough,
     &  tau_dim,tau_global,utau_global)

	return
	end




c***********************************************************************
	subroutine convec(q,vnu,fv1,sn,u,v,s,vmul,
     &  xx,xy,yx,yy,ug,vg,x,y,aj,bj,cj,ak,bk,ck,jd,kd)
c***********************************************************************
        use params_global
c***********************************************************************
        implicit none
c***********************************************************************

      integer jd,kd
      real q(jd,kd,nq), s(jd,kd)
      real x(jd,kd), y(jd,kd),ug(jd,kd),vg(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real aj(jd,kd),bj(jd,kd),cj(jd,kd)
      real ak(jd,kd),bk(jd,kd),ck(jd,kd)

      real vmul(jd,kd),sn(jd,kd)
      real u(jd,kd),v(jd,kd),vnu(jd,kd)
      real fv1(jd,kd)

      !local variables
      real,allocatable :: um(:,:),up(:,:),vm(:,:),vp(:,:)

      integer j,k
      real uu,vv,fwd,bck

      allocate(um(jd,kd),up(jd,kd),vm(jd,kd),vp(jd,kd))
c***  first executable statement

      do 10 k=1,kmax
         do 10 j=1,jmax
            uu=xx(j,k)*(u(j,k)-ug(j,k))+xy(j,k)*(v(j,k)-vg(j,k))
            vv=yx(j,k)*(u(j,k)-ug(j,k))+yy(j,k)*(v(j,k)-vg(j,k))
            up(j,k)=0.5*(uu+abs(uu))
            um(j,k)=0.5*(uu-abs(uu))
            vp(j,k)=0.5*(vv+abs(vv))
            vm(j,k)=0.5*(vv-abs(vv))
 10      continue

         do 20 k=2,kmax-1
            do 20 j=2,jmax-1
               s(j,k)=s(j,k)-up(j,k)*(vnu(j,k)-vnu(j-1,k))
     &              -um(j,k)*(vnu(j+1,k)-vnu(j,k))
               s(j,k)=s(j,k)-vp(j,k)*(vnu(j,k)-vnu(j,k-1))
     &              -vm(j,k)*(vnu(j,k+1)-vnu(j,k))
 20         continue




c	j-direction jacobian terms

	do 30 k=1,kmax
	do 30 j=2,jmax-1

	if(up(j+1,k).gt.1.0e-12) then
	  fwd=1.0
	else
	  fwd=0.0
	endif

	if(um(j-1,k).lt.-1.0e-12) then
	  bck=1.0
	else
	  bck=0.0
	endif

	aj(j+1,k)=aj(j+1,k)-fwd*(up(j,k)+um(j,k))
	bj(j,k)=  bj(j,k)+ up(j,k)-um(j,k)
	cj(j-1,k)=cj(j-1,k)+bck*(up(j,k)+um(j,k))
30	continue

c	k-direction jacobian terms

	do 40 k=2,kmax-1
	do 40 j=1,jmax

	if(vp(j,k+1).gt.1.0e-12) then
	  fwd=1.0
	else
	  fwd=0.0
	endif

	if(vm(j,k-1).lt.-1.0e-12) then
	  bck=1.0
	else
	  bck=0.0
	endif

	ak(j,k+1)=ak(j,k+1)-fwd*(vp(j,k)+vm(j,k))
	bk(j,k)=  bk(j,k)+ vp(j,k)-vm(j,k)
	ck(j,k-1)=ck(j,k-1)+bck*(vp(j,k)+vm(j,k))
40	continue

        return
        end

c***********************************************************************
	subroutine diffus(q,vnu,fv1,sn,u,v,s,vmul,
     &  xx,xy,yx,yy,x,y,aj,bj,cj,ak,bk,ck,jd,kd)
c***********************************************************************
        use params_global
c***********************************************************************
        implicit none
        integer jd,kd
        real q(jd,kd,nq), s(jd,kd), vnu(jd,kd)
        real x(jd,kd), y(jd,kd)
        real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
        real aj(jd,kd),bj(jd,kd),cj(jd,kd)
        real ak(jd,kd),bk(jd,kd),ck(jd,kd)
        
        real vmul(jd,kd),sn(jd,kd)
        real u(jd,kd),v(jd,kd)
        real fv1(jd,kd)
        
        include 'sadata.h'

      ! local variables
        real,allocatable :: chp(:,:),dnuhp(:,:)
        real,allocatable :: dmxh(:,:),dmyh(:,:)
        
        integer j,k
        real vnulh,vnuh,dxp,dxm,c2,dcp,dcm,ax,cx,dyp,dym,ay,cy

        allocate(chp(jd,kd),dnuhp(jd,kd))
        allocate(dmxh(jd,kd),dmyh(jd,kd))
c***  first executable statement
      
c	compute j direction differences.

c	compute half-point co-efficients

	do 10 k=1,kmax
	do 10 j=1,jmax-1
		dmxh(j,k) =0.5*(xx(j,k)+xx(j+1,k))
		dmyh(j,k) =0.5*(xy(j,k)+xy(j+1,k))
		vnulh     =0.5*(vmul(j,k)/q(j,k,1)/q(j,k,nq)+
     &			       vmul(j+1,k)/q(j+1,k,1)/q(j+1,k,nq))
		vnuh	 =0.5*(vnu(j,k)+vnu(j+1,k))
		chp(j,k) =(1.+cb2)/sigma/rey*(vnulh+vnuh)	
	      dnuhp(j,k) =vnu(j+1,k)-vnu(j,k)
10	continue

c	compute j direction fluxes

	do 20 k=1,kmax
	do 20 j=2,jmax-1

	dxp=dmxh(j,k)*xx(j,k)+dmyh(j,k)*xy(j,k)
	dxm=dmxh(j-1,k)*xx(j,k)+dmyh(j-1,k)*xy(j,k)

	c2=cb2/sigma/rey
	c2=c2*(vmul(j,k)/q(j,k,1)/q(j,k,nq)+vnu(j,k))

c	enforce positivity (as suggested by overflow)
	
	    dcp    = dxp*(chp(j,k)-c2)
	    dcm    = dxm*(chp(j-1,k)-c2)
	    ax=0.0
	    cx=0.0
	if(k.ne.kmax.and.k.ne.1) then
            ax       = max(dcm,0.0)
            cx       = max(dcp,0.0)
	endif
c        compute fluxes.

	    s(j,k)=s(j,k)-ax*dnuhp(j-1,k)+cx*dnuhp(j,k)

c	 jacobian terms

	    aj(j,k) = aj(j,k) - ax
	    cj(j,k) = cj(j,k) - cx
	    bj(j,k) = bj(j,k)+ax+cx

20	continue

c	boundary terms in jacobians

	do 30 k=1,kmax
	aj(jmax,k)=0.0
	cj(1,k)   =0.0
30	continue


c	compute k direction differences.

c	compute half-point co-efficients

	do 40 k=1,kmax-1
	do 40 j=1,jmax
		dmxh(j,k) =0.5*(yx(j,k)+yx(j,k+1))
		dmyh(j,k) =0.5*(yy(j,k)+yy(j,k+1))
		vnulh     =0.5*(vmul(j,k)/q(j,k,1)/q(j,k,nq)+
     &			       vmul(j,k+1)/q(j,k+1,1)/q(j,k+1,nq))
		vnuh	 =0.5*(vnu(j,k)+vnu(j,k+1))
		chp(j,k) =(1.+cb2)/sigma/rey*(vnulh+vnuh)	
	      dnuhp(j,k) =vnu(j,k+1)-vnu(j,k)
40	continue

c	compute j direction fluxes

	do 50 k=2,kmax-1
	do 50 j=1,jmax

	dyp=dmxh(j,k)*yx(j,k)+dmyh(j,k)*yy(j,k)
	dym=dmxh(j,k-1)*yx(j,k)+dmyh(j,k-1)*yy(j,k)

	c2=cb2/sigma/rey
	c2=c2*(vmul(j,k)/q(j,k,1)/q(j,k,nq)+vnu(j,k))

c	enforce positivity (as suggested by overflow)
	
	   dcp    = dyp*(chp(j,k)-c2)
	   dcm    = dym*(chp(j,k-1)-c2)

	   ay=0.0
	   cy=0.0
	   if(j.ne.1.and.j.ne.jmax) then
           ay       = max(dcm,0.0)
           cy       = max(dcp,0.0)
	   endif

c        compute fluxes.

	s(j,k)=s(j,k)-ay*dnuhp(j,k-1)+cy*dnuhp(j,k)


c	 jacobian terms

	    ak(j,k) = ak(j,k) - ay
	    ck(j,k) = ck(j,k) - cy
	    bk(j,k) = bk(j,k)+ ay+cy

50	continue

c	boundary terms in jacobians

	do 60 k=1,kmax
	ak(jmax,k)=0.0
	ck(1,k)   =0.0
60	continue
	
        return
        end

c***********************************************************************
        subroutine source(q,vnu,turmu,sn,u,v,s,vmul,vort,strain,
     &  xx,xy,yx,yy,x,y,aj,bj,cj,ak,bk,ck,jd,kd,hs_rough,
     &  tau_dim,tau_global,utau_global)  
c***********************************************************************
        use params_global
c***********************************************************************
        implicit none
        integer jd,kd
        real q(jd,kd,nq), s(jd,kd)
		  real vnu(jd,kd),turmu(jd,kd)
        real x(jd,kd), y(jd,kd)
        real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
        real aj(jd,kd),bj(jd,kd),cj(jd,kd)
        real ak(jd,kd),bk(jd,kd),ck(jd,kd)
        
        real vmul(jd,kd),sn(jd,kd)
		  real vort(jd,kd),strain(jd,kd)
        real u(jd,kd),v(jd,kd)
		  real hs_rough
        
		  ! friction variables
		  integer tau_dim
		  real tau_global(tau_dim),utau_global(tau_dim)

        include 'sadata.h'

        ! local variables
        real,allocatable :: chp(:,:),dnuhp(:,:)
        real,allocatable :: dmxh(:,:),dmyh(:,:)
        
        real rcv2,d2min,rmax,stilim,fturf
        real vnul,chi,d,fv1,fv2,fv3,ft2,dchi
        real dfv1,dfv2,dfv3,dft2,d2,stilda,r,g,fw
        real dstild,dr,dg,dfw,pro,des,prod,dest,dpro,ddes
        real tk1,tk2
        integer j,k,isour,jr1,jr2
        
		  real tau_wall, utau_wall,sqrt_rey,inv_u_total,rho
		  real dp_dx,dp_dy,dp_ds
		  real up_wall, uc_wall, var1,akt_new,cw1_new
		  real,allocatable :: p(:,:)
		  integer iprint

        allocate(chp(jd,kd),dnuhp(jd,kd))
        allocate(dmxh(jd,kd),dmyh(jd,kd))
		  allocate(p(jd,kd))
c**   first executable statement

		  sqrt_rey = sqrt(rey)

		  jr1 = j1_rough
		  jr2 = j2_rough

        rcv2=(1./5.)
        d2min=1.e-12
        rmax=10.0
        stilim=1.e-12
        fturf=0.0
        isour = 0

!		  print*,'print vk const?'
!		  read(6,*)iprint

        do k=1,kmax
        do j=1,jmax
          p(j,k)  = gm1*(q(j,k,4) -0.5*(q(j,k,2)**2
     &             +q(j,k,3)**2)/q(j,k,1))*q(j,k,nq)
		  enddo
		  enddo

        do 10 k=2,kmax-1
        do 10 j=2,jmax-1
        vnul = vmul(j,k)/q(j,k,1)/q(j,k,nq)
        chi  = vnu(j,k)/vnul
        d    = sn(j,k)

			!### modification for generalized law of wall ###
!			if(iprint==1) then
			akt_new = akt

			if(iloglaw) then
			rho = q(j,1,1)*q(j,1,nq)
			tau_wall  = tau_global(j)
			utau_wall = utau_global(j)/sqrt_rey
			inv_u_total = 1.0/(sqrt(u(j,k)*u(j,k)+v(j,k)*v(j,k))+1.e-16)
			dp_dx = 0.5*(p(j+1,1)-p(j-1,1))
			!dp_dy = p(j,2)-p(j,1)
			dp_dy = 0.5*(-3.0*p(j,1) + 4.0*p(j,2) - 1.0*p(j,3) )
			dp_ds = (dp_dx*u(j,k) + dp_dy*v(j,k))*inv_u_total
			up_wall = (vnul*abs(dp_ds)/(rey*rho))**(1.0/3.0)
			uc_wall = utau_wall + up_wall + 1.e-20
			var1 = 1.0 / (sign(1.0,tau_wall)*utau_wall/(uc_wall*0.41) + 5.0*sign(1.0,dp_ds)*(up_wall/uc_wall)**3.0)


!var1 = eddy_viscosity(i,j,k)*strain_rate(i,j,k)/(1.5_rdp*abs(tau_wall+1.e-20))
!var1 = min(max(var1**eight-1,zero),one)
!var1 = 0.41_rdp - 0.15_rdp*var1


			var1 = turmu(j,k)*strain(j,k)/(1.5*abs(tau_wall+1.e-20))
			var1 = min(max(var1**8.0-1.0,0.0),1.0)
			akt_new = 0.41 - 0.2*var1
			!akt_new = 0.41 - 0.18*var1

			!cw1 = cb1/(akt*akt) + (1.0 + cb2)/sigma
!			if(k==2) then
!			  write(110,*)p(j-1,1),p(j,1),p(j+1,1)
!			  write(111,*)dp_dx,dp_dy,dp_ds
!			  write(112,*)utau_wall,up_wall,uc_wall
!			  write(113,*)j,var1,akt
!			endif

			endif

!			endif
			!### end modification for generalized law of wall ###

		  if(irough .and. j>=jr1 .and. j<=jr2) then
			  d   = sn(j,k) + 0.03*hs_rough
			  chi = chi + cr1*hs_rough/d
		  endif
        chi  = max(chi,1.e-12)
        fv1  = (chi**3)/(chi**3+cv1**3)
		  d2      = max(d**2,d2min)

        if(isour.eq.0) then

			 if(irough) then

          fv2  = 1.-vnu(j,k)/(vnul+vnu(j,k)*fv1)
          fv3  = 1.0
          ft2  = fturf*ct3*exp(-1.*ct4*chi*chi)
          dchi = 1./vnul
          dfv1 = (3.*cv1**3)*(chi**2)*dchi*(1./(chi**3+cv1**3))**2.0
			 !dfv2 = -vnul/(vnul+vnu(j,k)*fv1)**2.0
			 dfv2 = (vnu(j,k)*vnu(j,k)*dfv1 - vnul) / (vnul + vnu(j,k)*fv1)**2.0
          dfv3 = 0.0

			 else

          fv2  = 1.-chi/(1.+chi*fv1)
          fv3  = 1.0
          ft2  = fturf*ct3*exp(-1.*ct4*chi*chi)
			 dft2	= (-2.*ct4)*chi*dchi*ft2
          dchi = 1./vnul
          dfv1 = (3.*cv1**3)*(chi**2)*dchi*(1./(chi**3+cv1**3))**2.0
          dfv2 = (fv2-1.)/chi/vnul+(1.-fv2)*(1-fv2)*(dfv1+fv1/chi/vnul)
          dfv3 = 0.0

			 endif

			 stilda  = max(vort(j,k) + vnu(j,k)/(d2*akt*akt*rey)*fv2,0.3*vort(j,k))
			 stilda  = max(stilda,stilim)

        else

          fv2  = 1./(1.+chi*rcv2)
          fv2  = fv2**3
          fv3  = (1.+chi*fv1)*(1.-fv2)/chi
          ft2  = fturf*ct3*exp(-1.*ct4*chi*chi)
      	 dft2	= (-2.*ct4)*chi*dchi*ft2
          dchi = 1./vnul
          dfv1 = (3.*cv1**3)*(chi**2)*dchi*(1./(chi**3+cv1**3))**2
          dfv2 = (-3.*rcv2)*fv2*dchi/(1.+chi*rcv2)
          dfv3 = ( (chi*dfv1 + dchi*fv1)*(1.-fv2) 
     &         - (1.+chi*fv1)*dfv2- dchi*fv3)/chi 

!	for new definition of s_{tilda}, refer aiaa-95-0312

	       stilda  = vort(j,k)*fv3 + vnu(j,k)/(d2*akt*akt*rey)*fv2
			 stilda  = max(stilda,stilim)

        endif

		!r	= vnu(j,k)/(d2*akt*akt*rey)/stilda	
		r	= vnu(j,k)/(d2*akt_new*akt_new*rey)/stilda	
		r 	= min(r,rmax)
		g	= r + cw2*(r**6 - r)
		fw	= (1.+cw3**6)/(g**6+cw3**6)
		fw	= g*(fw**(1./6.))

!       dstild = vort(j,k)*dfv3+vnul*(dchi*fv2+chi*dfv2)
!     &	/(d2*akt*akt*rey)
       dstild = vnul*(dchi*fv2+chi*dfv2)
     &	/(d2*akt*akt*rey)
!       dr     = vnul*(dchi-chi*dstild/stilda)
!     &	/stilda/(d2*akt*akt*rey)
       dr     = vnul*(dchi-chi*dstild/stilda)
     &	/stilda/(d2*akt_new*akt_new*rey)
       dg     = dr*(1.+cw2*(6.*r**5.-1.))
	dfw	= ((1.+cw3**6)/(g**6+cw3**6))**(1./6.)
	dfw     =  dfw*dg*(1.- g**6/(g**6+cw3**6))
       
       pro   = cb1*stilda*(1.-ft2)
       des   = (cw1*fw-cb1/(akt*akt)*ft2)/d2/rey

       prod   = pro*vnu(j,k)
       dest   = des*vnu(j,k)*vnu(j,k)

       !prod   = cb1*stilda*(1.-ft2)*vnu(j,k)
       !dest   = (cw1*fw-cb1/(akt*akt)*ft2)*vnu(j,k)*vnu(j,k)/d2/rey

       dpro = pro*dstild/stilda - cb1*stilda*dft2
  
       ddes = (cw1*dfw-cb1/(akt*akt)*dft2)/d2/rey*vnul*chi
       ddes = ddes + des
       ddes = ddes*vnu(j,k)
       dpro = dpro*vnu(j,k)

! transition model
!		  if(j<110) prod = 0.0	! use this for sk fixed transition
       if(itrans.eq.1) prod = q(j,k,nv-1)*prod 


        s(j,k) = s(j,k) + prod-dest
		  tk1=max(des*vnu(j,k)-pro,0.0)
		  tk2=max(ddes-dpro,0.0)
        bk(j,k) = bk(j,k)+tk1+tk2
c       bj(j,k) = bj(j,k)+tk1+tk2

10	continue


		  deallocate(p)

        return
        end

c*************************************************************
        subroutine turbc(vnu,u,v,ug,vg,sn,d0_rough,xx,xy,yx,yy,im)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        real vnu(jmax,kmax),u(jmax,kmax),v(jmax,kmax)
        real ug(jmax,kmax),vg(jmax,kmax)
        real xx(jmax,kmax),xy(jmax,kmax),yx(jmax,kmax),yy(jmax,kmax)
        integer im
		  real sn(jmax,kmax)
		  real d0_rough

c..   local variables
        integer js,je,ks,ke,idir
        integer j,k,ib

        do ib=1,nbc_all(im)
          js = jbcs_all(ib,im)
          je = jbce_all(ib,im)
          ks = kbcs_all(ib,im)
          ke = kbce_all(ib,im)
          if(js.lt.0) js = jmax+js+1
          if(ks.lt.0) ks = kmax+ks+1
          if(je.lt.0) je = jmax+je+1
          if(ke.lt.0) ke = kmax+ke+1
          idir = ibdir_all(ib,im)

c.. crude outflow bc (shivaji)
          if (ibtyp_all(ib,im).eq.53) then
            call turbc_outflow(vnu,js,je,ks,ke,idir)

c.. inviscid wind tunnel wall bc
          elseif (ibtyp_all(ib,im).eq.3) then
            call turbc_extpt(vnu,js,je,ks,ke,idir)

c.. wall bc at l = 1 (only interior portion of wall)
          elseif (ibtyp_all(ib,im).eq.4.or.ibtyp_all(ib,im).eq.5) then
            call turbc_wall(vnu,sn,d0_rough,js,je,ks,ke,idir)

c.. symmetric bc
          elseif (ibtyp_all(ib,im).eq.11) then
            call turbc_sym(vnu,js,je,ks,ke,idir)

c.. periodic bc
          elseif (ibtyp_all(ib,im).eq.22) then
            call turbc_periodic(vnu,js,je,ks,ke,idir)

c.. averaging bc for wake
          elseif (ibtyp_all(ib,im).eq.51) then
            call turbc_wake(vnu,js,je,ks,ke,idir)

c.. averaging bc for wake of O-grid
          elseif (ibtyp_all(ib,im).eq.52) then
            call turbc_wake_ogrid(vnu,js,je,ks,ke,idir)

c.. freesream bc
          elseif (ibtyp_all(ib,im).eq.47) then
            call turbc_out(vnu,u,v,ug,vg,xx,xy,yx,yy,js,je,ks,ke,idir)

          endif
        enddo

        return
        end

c*************************************************************
      subroutine turbc_wall(vnu,sn,d0_rough,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real vnu(jmax,kmax),sn(jmax,kmax)
		real d0_rough
      integer js,je,ks,ke,idir,j1,k1

c.. local variables

      integer j,k,iadd,iadir,jr1,jr2
      real t,scal,radd,hsp

		jr1 = 117
		jr2 = 254

      iadd = sign(1,idir)
      iadir = abs(idir)
		radd = real(iadd,8)

		hsp = 150.0

      t = (float(istep0)-1.)/30.
      if(t.gt.1..or.iread.gt.0) t=1.
      scal = (10.-15.*t+6.*t*t)*t**3

		if(irough) then

				if(iadir.eq.1) then
			  j = js
			  j1 = j + iadd
			  do k=ks,ke
				  vnu(j,k) = vnu(j1,k)*d0_rough/(d0_rough+sn(j1,k))
			  enddo
			elseif(iadir.eq.2) then
			  k = ks
			  k1 = k + iadd
			  do j=jr1,jr2
				 vnu(j,k) = vnu(j,k1)*d0_rough/(d0_rough+sn(j,k1))
!				 vnu(j,k) = (0.377*log(hsp)-0.447)*exp(-hsp/70.0)
!     & 			+ 1.2571e-2*hsp*(1-exp(-hsp/70.0))
!     &			+ max(0.0,log(hsp/10.0))*min(1.0,1.36*exp(-hsp/250.0),25.0*exp(-hsp/100.0))
			  enddo

			  do j=js,jr1-1
				 vnu(j,k)=0.0*scal + (1.-scal)*vnu(j,k)
			  enddo
			  do j=jr2+1,je
				 vnu(j,k)=0.0*scal + (1.-scal)*vnu(j,k)
			  enddo

			endif

	else

			if(iadir.eq.1) then
			  j = js
			  do k=ks,ke
				 vnu(j,k)=0.0*scal + (1.-scal)*vnu(j,k)
			  enddo
			elseif(iadir.eq.2) then
			  k = ks
			  do j=js,je
				 vnu(j,k)=0.0*scal + (1.-scal)*vnu(j,k)
			  enddo
			endif

		endif

!c	far-stream
!
!	nslow=0
!
!         do j=jtail1,jtail2
!	 k=1
!	 vnu(j,k) = 0.0
!         enddo
!
!c	jet flow bc - private communication with philippe spalart.
!c	i still have to include this effect on solving ax=b.
!
!	if(ichoice.eq.0) then
!
!	do j=jtail1,jtail2
!
!        if(j.ge.j_s2.and.j.le.j_e2.and.atime.ge.t1b.and.
!     &          atime.le.t2b) then
!	    if(sin(2*pi*f1b*(atime-t1b)).gt.0.0) then
!	    vnu(j,1)=0.002*abs(vnb)*0.01
!	    else
!	    vnu(j,1)=vnu(j,2)
!	    endif
!	endif
!
!        if(j.ge.j_s1.and.j.le.j_e1.and.atime.ge.t1a.and.
!     &          atime.le.t2a) then
!	    if(sin(2*pi*f1a*(atime-t1a)).gt.0.0) then
!	    vnu(j,1)=0.002*abs(vna)*0.01
!	    else
!	    vnu(j,1)=vnu(j,2)
!	    endif
!	endif
!
!	enddo
!
!	endif
!
!	if(ichoice.eq.1) then
!
!	do j=jtail1,jtail2
!        if(j.ge.j_s2.and.j.le.j_e2.and.atime.ge.t1b.and.
!     &          atime.le.t2b) then
!	    vnu(j,1)=0.002*abs(vnb)*0.01
!	endif
!	enddo
!
!	endif

      return
      end

c*************************************************************
      subroutine turbc_outflow(vnu,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real vnu(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          vnu(j,k) = vnu(j1,k)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          vnu(j,k) = vnu(j,k1)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine turbc_extpt(vnu,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real vnu(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          vnu(j,k) = vnu(j1,k)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          vnu(j,k) = vnu(j,k1)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine turbc_sym(vnu,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real vnu(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          vnu(j,k) = vnu(j1,k)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          vnu(j,k) = vnu(j,k1)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine turbc_periodic(vnu,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real vnu(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jc,jj,jj1,kc,kk,kk1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(idir.eq.1) then
        jj  = je - js + 1
        do j = js,je
           jj1 = j - js
           jc = jmax - 2*jj + jj1
 
           do k = ks,ke
             vnu(j,k) = vnu(jc,k)
           enddo
        enddo

      elseif(idir.eq.-1) then
        jj  = je - js + 1
        do j = js,je
           jj1 = je - j
           jc = 1 + 2*jj - jj1
 
           do k = ks,ke
             vnu(j,k) = vnu(jc,k)
           enddo
        enddo

      elseif(idir.eq.2) then
        kk  = ke - ks + 1
        do k = ks,ke
           kk1 = k - ks
           kc = kmax - 2*kk + kk1
 
           do j = js,je
             vnu(j,k) = vnu(j,kc)
           enddo
        enddo

      elseif(idir.eq.-2) then
        kk  = ke - ks + 1
        do k = ks,ke
           kk1 = ke - k
           kc = 1 + 2*kk - kk1 
 
           do j = js,je
             vnu(j,k) = vnu(j,kc)
           enddo
        enddo

      endif

      return
      end

c*************************************************************
      subroutine turbc_wake(vnu,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real vnu(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jj,j1,k1,iadd,iadir
      real vnuav

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        print*,'idir = ',idir,' is not implemented in turbc_wake'
      elseif(iadir.eq.2) then
        k  = ks
        k1 = k + iadd
        do j=js,je
          jj = jmax - j + 1
          vnuav = 0.5*(vnu(j,k1)+vnu(jj,k1))
          vnu(j,k)  = vnuav
          vnu(jj,k) = vnuav
        enddo
      endif

      return
      end

c*************************************************************
      subroutine turbc_wake_ogrid(vnu,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real vnu(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jj,j1,iadd,iadir
      real vnuav

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j  = js
        j1 = j + iadd
        jj = jmax - j1 + 1
        do k=ks,ke
          vnuav = 0.5*(vnu(j1,k)+vnu(jj,k))
          vnu(j,k)  = vnuav
        enddo
      elseif(iadir.eq.2) then
        print*,'idir = ',idir,' is not implemented in turbc_wake_ogrid'
      endif

      return
      end

c*************************************************************
      subroutine turbc_out(vnu,u,v,ug,vg,xx,xy,yx,yy,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real vnu(jmax,kmax),u(jmax,kmax),v(jmax,kmax)
      real ug(jmax,kmax),vg(jmax,kmax)
      real xx(jmax,kmax),xy(jmax,kmax),yx(jmax,kmax),yy(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir
      real uu

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j  = js
        j1 = j + iadd
        do k=ks,ke
          vnu(j,k)=vnuinf
          uu=   (u(j,k)-ug(j,k)+u(j1,k)-ug(j1,k))*xx(j,k)
          uu=uu+(v(j,k)-vg(j,k)+v(j1,k)-vg(j1,k))*xy(j,k)
          uu=uu*iadd
          if(uu.lt.0.) vnu(j,k)=vnu(j1,k)
        enddo
      elseif(iadir.eq.2) then
        k  = ks
        k1 = k + iadd
        do j=js,je
          vnu(j,k)=vnuinf
          uu=   (u(j,k)-ug(j,k)+u(j,k1)-ug(j,k1))*yx(j,k)
          uu=uu+(v(j,k)-vg(j,k)+v(j,k1)-vg(j,k1))*yy(j,k)
          uu=uu*iadd
          if(uu.lt.0.) vnu(j,k)=vnu(j,k1)
        enddo
      endif

      return
      end

c*************************************************************
	subroutine lsolvej(a,b,c,s,jd,kd)
c*************************************************************
        use params_global
c*************************************************************
        
        integer jd,kd

	real a(jd,kd),b(jd,kd),c(jd,kd)
	real s(jd,kd)

c.. local variables

        integer j,k
        real bb

c***  first executable statement
	
         do k = 2,km
         bb       = 1./b(2,k)
         s(2,k)  = s(2,k)*bb
         c(2,k)  = c(2,k)*bb
	 enddo
 
      do  j = 3,jm
      do  k = 2,km
         bb      = 1./(b(j,k) - a(j,k)*c(j-1,k))
         s(j,k)  = (s(j,k) - a(j,k)*s(j-1,k))*bb
         c(j,k)  = c(j,k)*bb
	 enddo
	 enddo

      do j = jm-1,2,-1
      do k = 2,km
         s(j,k)    = s(j,k) - c(j,k)*s(j+1,k)
	 enddo
	 enddo

	
	return
	end

c*************************************************************
	subroutine lsolvek(a,b,c,s,jd,kd)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        integer jd,kd
	real a(jd,kd),b(jd,kd),c(jd,kd)
	real s(jd,kd)

c.. local variables

        integer j,k
        real bb

c***  first executable statement
	
         do j = 2,jm
         bb       = 1./b(j,2)
         s(j,2)  = s(j,2)*bb
         c(j,2)  = c(j,2)*bb
	 enddo
 
      do  k = 3,km
      do  j = 2,jm
         bb      = 1./(b(j,k) - a(j,k)*c(j,k-1))
         s(j,k)  = (s(j,k) - a(j,k)*s(j,k-1))*bb
         c(j,k)  = c(j,k)*bb
	 enddo
	 enddo

      do k = km-1,2,-1
      do j = 2,jm
         s(j,k)    = s(j,k) - c(j,k)*s(j,k+1)
	 enddo
	 enddo

	
	return
	end

c*************************************************************
	subroutine newtn(vnu,vnu0,s,q,jd,kd)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        integer jd,kd
        real vnu(jd,kd),vnu0(jd,kd),s(jd,kd)
	real q(jd,kd,nq)

        ! local variables

        integer j,k
        real tscal

c***  first executable statement

        tscal = h
	do k = 1,kmax
        do j = 1,jmax
	s(j,k)=s(j,k)+(vnu0(j,k)-vnu(j,k))/tscal
	enddo
	enddo

	return
	end

