c***********************************************************************
      subroutine sa_gamma(q,turmu,sn_d,vort_d,strain_d,x,y,xx,xy,yx,yy,
     &                    ug,vg,jd,kd,tscale,iblank,im)

c Menter's correlation-based transition model (gamma)
c Menter et al, "A One-Equation Local Correlation-Based Transition Model"
c Flow Turbulence Combust (2015) 95:583--619
c
c Note
c
c LHS and RHS are divided by rho
c
c Wall distance and vorticity are calculated again to avoid interferences
c with DDES length scale and vorticity correction
c
c last modified 08/10/19 by bumseok Lee
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      integer im,jd,kd
      real q(jd,kd,nq),turmu(jd,kd)
      real sn_d(jd,kd),vort_d(jd,kd),strain_d(jd,kd)
      real tscale(jd,kd)
      real x(jd,kd), y(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real ug(jd,kd), vg(jd,kd) 
      integer iblank(jd,kd)
c***********************************************************************
      ! local variables
      real,allocatable :: itmc(:,:), sn(:,:), sn_n(:,:,:)
      real,allocatable :: bjmat(:,:),sjmat(:,:)
      real,allocatable :: aj(:,:),cj(:,:)
      real,allocatable :: ak(:,:),ck(:,:)
      real,allocatable :: fon_lim(:,:),rho(:,:),u(:,:),v(:,:)
      real,allocatable :: vmul(:,:),dvdy(:,:),vort(:,:),strain(:,:)
      real,allocatable :: lambda(:,:),rey_tc(:,:),fpg(:,:)
      real,allocatable :: tu_l(:,:)
      
      integer k,j,n,jloc,kloc
      real relfac,resitmc,resitmc1,resmax,oat,tscal,dtpseudo_turb
      real itmclim

      integer lgturb

      allocate(itmc(jd,kd),sn(jd,kd),sn_n(jd,kd,2))
      allocate(bjmat(jd,kd),sjmat(jd,kd))
      allocate(aj(jd,kd),cj(jd,kd))
      allocate(ak(jd,kd),ck(jd,kd))
      allocate(fon_lim(jd,kd),rho(jd,kd),u(jd,kd),v(jd,kd))
      allocate(vmul(jd,kd),dvdy(jd,kd),vort(jd,kd),strain(jd,kd))
      allocate(lambda(jd,kd),rey_tc(jd,kd),fpg(jd,kd))
      allocate(tu_l(jd,kd))

c***  first executable statement

! set global values
      itmclim = 1e-20

c...laminar viscosity
      call lamvis(q,vmul,jd,kd)

c...initiate local working variables
      do  k = 1,kmax
      do  j = 1,jmax
        rho(j,k) = q(j,k,1)*q(j,k,nq)
        u(j,k)   = q(j,k,2)/q(j,k,1)
        v(j,k)   = q(j,k,3)/q(j,k,1)
        itmc(j,k) = q(j,k,nv-1)
        sn(j,k)  = 0.0
        sn_n(j,k,1)  = 0.0
        sn_n(j,k,2)  = 0.0
        tu_l(j,k)  = 0.0
      enddo
      enddo

c...apply boundary condition
      call sa_gamma_bc(q,rho,u,v,ug,vg,xx,xy,yx,yy,im)

c...calculate wall distance and wall normal vectors
      call sa_gamma_dist(sn,sn_n,x,y,jd,kd)

c...compute wall normal velocity, vorticity, and strain
      call sa_gamma_dvdy(q,vort,strain,dvdy,u,v,sn_n,
     & xx,xy,yx,yy,jd,kd)

      call sa_gamma_retheta(vort,strain,dvdy,lambda,rey_tc,fpg,
     &tu_l,rho,u,v,sn,turmu,vmul,jd,kd)

c...compute rhs and lhs

      call sa_gamma_rhslhs(itmc,rey_tc,fon_lim,vmul,turmu,
     &               vort,strain,rho,u,v,ug,vg,xx,xy,yx,yy,sn,
     &               aj,bjmat,cj,ak,ck,sjmat,jd,kd)

c...invert using DDADI

c..set time-accuracy
      oat = 1.0
      if (ntac.eq.-2) oat = 0.5
      if (ntac.ge.2 .and. istep.gt.1) oat = 2./3.
      if (ntac.eq.3 .and. istep.gt.2) oat = 6./11.

      resitmc =0.0
      resitmc1=0.0
      do k = 1,kmax
      do j = 1,jmax

        tscal = tscale(j,k)
        if(timeac.eq.1) then
          dtpseudo_turb=10.0
          tscal = max(iblank(j,k),0)*( 1.0 + 0.002*sqrt(q(j,k,nq)))
     <                       /(1.+sqrt(q(j,k,nq)))
          tscal = tscal*dtpseudo_turb
          tscal = tscal/(1.+tscal/h/oat)
        endif

        aj(j,k) = aj(j,k)*tscal
        cj(j,k) = cj(j,k)*tscal
        ak(j,k) = ak(j,k)*tscal
        ck(j,k) = ck(j,k)*tscal
        bjmat(j,k) = 1.+ bjmat(j,k)*tscal
        sjmat(j,k)  = sjmat(j,k)*tscal
      enddo
      enddo

      !call lsolvej2(aj,bjmat,cj,sjmat,jd,kd)
      call lsolvej(aj,bjmat,cj,sjmat,jd,kd)

      do k = 1,kmax
      do j = 1,jmax
        sjmat(j,k) = sjmat(j,k)*bjmat(j,k)
      enddo
      enddo

      !call lsolvek2(ak,bjmat,ck,sjmat,jd,kd)
      call lsolvek(ak,bjmat,ck,sjmat,jd,kd)

      relfac = 1.
      resmax = 0.
      do k = 1,kmax
      do j = 1,jmax
        sjmat(j,k) = relfac*sjmat(j,k)
        itmc(j,k) = itmc(j,k) + sjmat(j,k)*max(iblank(j,k),0)
        itmc(j,k) = max(itmc(j,k),itmclim)

        resitmc = resitmc + sjmat(j,k)**2

        if (resitmc.gt.resmax) then
          jloc = j
          kloc = k
          resmax = resitmc
        endif

        itmc(j,k) = min(max(itmc(j,k),itmclim),1.0)
        resitmc1 = resitmc1 + (itmc(j,k)-q(j,k,nv-1))**2
      enddo
      enddo

      resitmc = sqrt(resitmc/jmax/kmax)
      resitmc1= sqrt(resitmc1/jmax/kmax)
      resmax = sqrt(resmax/jmax/kmax)
      if( mod(istep,npnorm).eq.0) then
         write(1333+im,61) float(istep0),resitmc,resitmc1
      endif

 61   FORMAT (3(x,E14.6))


!..update the global variables
      do  k = 1,kmax
      do  j = 1,jmax
        q(j,k,nv-1) = itmc(j,k)
      enddo
      enddo

c...apply boundary condition again
      call sa_gamma_bc(q,rho,u,v,ug,vg,xx,xy,yx,yy,im)

c...CBSL
c...calculate additional production term to ensure the generation of 
c...turbulent kinetic energy 
      call sa_gamma_pklim(q,itmc,strain,vort,fon_lim,
     &     turmu,vmul,jd,kd)

      return
      end

c*************************************************************
      subroutine sa_gamma_dist(sn,sn_n,x,y,jd,kd)
c
c     Note
c     The wall distance is calculated again to avoid interference
c     with the modifications of the wall distance in DDES
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real sn(jd,kd),sn_n(jd,kd,2)
      real x(jd,kd),y(jd,kd)
c*************************************************************
      integer j,k

c***  first executable statement

      k=1
      do j=jtail1,jtail2
      sn(j,k)=0.0
      sn_n(j,k,1)=0.0
      sn_n(j,k,2)=0.0
      enddo

      do j=jtail1,jtail2
      do k=2,kmax
        sn(j,k)     = sqrt((x(j,k)-x(j,1))**2+(y(j,k)-y(j,1))**2)
        sn_n(j,k,1) = (x(j,k)-x(j,1))/sn(j,k)
        sn_n(j,k,2) = (y(j,k)-y(j,1))/sn(j,k)
      enddo
      enddo

      if(jtail1.ne.1) then
        do j=1,jtail1-1
        do k=1,kmax
          sn(j,k)=sqrt((x(j,k)-x(jtail1,1))**2
     &                +(y(j,k)-y(jtail1,1))**2)

          sn_n(j,k,1) = (x(j,k)-x(jtail1,1))/sn(j,k)
          sn_n(j,k,2) = (y(j,k)-y(jtail1,1))/sn(j,k)
        enddo
        enddo
      endif

      if(jtail2.ne.jmax) then
        do j=jtail2+1,jmax
        do k=1,kmax
          sn(j,k)=sqrt((x(j,k)-x(jtail2,1))**2
     &                +(y(j,k)-y(jtail2,1))**2)
          sn_n(j,k,1) = (x(j,k)-x(jtail2,1))/sn(j,k)
          sn_n(j,k,2) = (y(j,k)-y(jtail2,1))/sn(j,k)
        enddo
        enddo
      endif

      return
      end


c*************************************************************
      subroutine sa_gamma_pklim(q,itmc,strain,vort,fon_lim,
     & turmu,vmul,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real q(jd,kd,nq),itmc(jd,kd),strain(jd,kd),vort(jd,kd)
      real fon_lim(jd,kd),turmu(jd,kd),vmul(jd,kd)
c*************************************************************
      integer j,k
      real ck, csep, cb1
      real var1, var2, var3, var4

      ck = 1.0; csep= 1.0; cb1=0.1355

      ! var4 is different for turbulence models

      if (iturb.eq.1) then ! SA model
        do j = 2, jd-1
        do k = 2, kd-1
          var1 = max(itmc(j,k)-0.2,0.0)
          var2 = 1.0-itmc(j,k)
          var3 = max(3.0*csep*vmul(j,k)-turmu(j,k),0.0)/rey
          var4 = cb1*sqrt(vort(j,k)*strain(j,k))
          q(j,k,nv) = 5.0*ck*var1*var2*fon_lim(j,k)*var3*var4
        enddo
        enddo

      elseif (iturb.eq.2) then ! SST model
        do j = 2, jd-1
        do k = 2, kd-1
          var1 = max(itmc(j,k)-0.2,0.0)
          var2 = 1.0-itmc(j,k)
          var3 = max(3.0*csep*vmul(j,k)-turmu(j,k),0.0)/rey
          var4 = vort(j,k)*strain(j,k)
          q(j,k,nv) = 5.0*ck*var1*var2*fon_lim(j,k)*var3*var4
        enddo
        enddo
      endif
      return
      end

c*************************************************************
      subroutine sa_gamma_dvdy(q,vort,strain,dvdy,u,v,sn_n,
     & xx,xy,yx,yy,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real q(jd,kd,nq),vort(jd,kd),strain(jd,kd),dvdy(jd,kd)
      real u(jd,kd),v(jd,kd), sn_n(jd,kd,2)
      real xx(jd,kd),xy(jd,kd),yx(jd,kd),yy(jd,kd)

! local variables
      integer j,k,jm1,km1,jp,kp
      real usi,vsi,ueta,veta
      real ux,uy,vx,vy
      real sxx,sxy,syy,tx
      real usi_w, ueta_w, ux_w, uy_w
      real,allocatable :: uwall(:,:)
 
      allocate(uwall(jd,kd))

      do j = 1,jd
        do k = 1,kd
          uwall(j,k)= u(j,k)*sn_n(j,k,1) + v(j,k)*sn_n(j,k,2)
        enddo
      enddo

      do j = 2,jd-1 
        jp = j + 1
        jm1 = j - 1
        do k = 2,kd-1
          kp = k + 1
          km1 = k - 1

          usi  = 0.5*(u(jp,k)-u(jm1,k))
          vsi  = 0.5*(v(jp,k)-v(jm1,k))
          
          ueta = 0.5*(u(j,kp)-u(j,km1))
          veta = 0.5*(v(j,kp)-v(j,km1))
            
          ux = xx(j,k)*usi + yx(j,k)*ueta
          uy = xy(j,k)*usi + yy(j,k)*ueta

          vx = xx(j,k)*vsi + yx(j,k)*veta
          vy = xy(j,k)*vsi + yy(j,k)*veta

          sxx = ux
          sxy = 0.5*(uy + vx)
          syy = vy

          vort(j,k) = abs(uy - vx)
          strain(j,k) = sqrt(2.*(sxx*sxx + 2.*sxy*sxy + syy*syy))

          usi_w  = 0.5*(uwall(jp,k)-uwall(jm1,k))
          ueta_w = 0.5*(uwall(j,kp)-uwall(j,km1))
          ux_w = xx(j,k)*usi_w  + yx(j,k)*ueta_w
          uy_w = xy(j,k)*usi_w  + yy(j,k)*ueta_w
          dvdy(j,k) = ux_w*sn_n(j,k,1) + uy_w*sn_n(j,k,2)

        enddo
      enddo

      return
      end

c*************************************************************
      subroutine sa_gamma_retheta(vort,strain,dvdy,lambda,rey_tc,fpg,
     & tu_l,rho,u,v,sn,turmu,vmul,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real vort(jd,kd),strain(jd,kd),dvdy(jd,kd)
      real lambda(jd,kd),rey_tc(jd,kd),fpg(jd,kd), tu_l(jd,kd)
      real rho(jd,kd),u(jd,kd),v(jd,kd),sn(jd,kd)
      real turmu(jd,kd), vmul(jd,kd)
c*************************************************************
! local variables
      integer j,k
      real tu, lambda_n, lambda_l
      real,allocatable :: kw_k(:,:), kw_w(:,:)

      ! CBSL: move these constants to head file later!
      real ctu1, ctu2, ctu3
      real cpg1, cpg2, cpg3, cpg1_lim, cpg2_lim
      real var1,var2,var3


      cpg1=14.68;   cpg2= -7.34;   cpg3=0.0;
      cpg1_lim=1.5; cpg2_lim=3.0;

      !ctu1=100.0;   ctu2=1000.0;   ctu3=1.0;  ! original
      !ctu1=163.0;   ctu2=1002.25;   ctu3=1.0;  ! Barakos et al.

      ! Linear blending of the original and Barakos
      ! 1) tuinf<=0.51: Barakos
      ! 2) 0.51 < tuinf<2.0 : linear blending
      ! 3) tuinf >= 2.0 original
      tu   = min(max(tuinf,0.51),2.0)
      ctu1 = (100.0-163.0)/(2.0-0.51)*(tu-2.0)+100.0
      ctu2 = (1000.0-1002.25)/(2.0-0.51)*(tu-2.0)+1000.0
      ctu3 = 1.0

      allocate(kw_k(jd,kd),kw_w(jd,kd))

      do j = 2,jd-1 
        do k = 2,kd-1
          kw_w(j,k) = strain(j,k)/0.3 ! Suggested by Menter
          kw_k(j,k) = kw_w(j,k)*turmu(j,k)/(rho(j,k)*rey)
          
          ! local turbulence intensity
          ! CBSL: for now local, need to test constant later
          !tu        = 100.0*sqrt(2.0*kw_k(j,k)/3.0)/(kw_w(j,k)*sn(j,k))
          !tu_l(j,k) = min(tu,100.0)

          ! Constant turbulence intensity
          tu_l(j,k) = tuinf

          lambda_n  = -7.57*1e-3*dvdy(j,k)*sn(j,k)*sn(j,k)
          lambda_l  = lambda_n/vmul(j,k)*rho(j,k)*rey + 0.0128
          lambda(j,k) = min(max(lambda_l,-1.0),1.0)

          if(lambda(j,k).ge.0.0) then
            var1 = 1.0 + cpg1*lambda(j,k)
            fpg(j,k) = min( var1, cpg1_lim )
          else
            var3 = cpg3*min(lambda(j,k)+0.0681, 0.0)
            var2 = 1.0 + cpg2*lambda(j,k) + var3
            fpg(j,k) = min(var2,cpg2_lim)
          endif
          fpg(j,k) = max(0.0, fpg(j,k))

          rey_tc(j,k) = ctu1 + ctu2 * exp(-ctu3*tu_l(j,k)*fpg(j,k))

        enddo
      enddo

      return
      end





c***********************************************************************
      subroutine sa_gamma_rhslhs(itmc,rey_tc,fon_lim,vmul,turmu,
     &               vort,strain,rho,u,v,ug,vg,xx,xy,yx,yy,sn,
     &               aj,bjmat,cj,ak,ck,sjmat,jd,kd)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************

      integer jd,kd
      real itmc(jd,kd), rey_tc(jd,kd), fon_lim(jd,kd)
      real rho(jd,kd), vmul(jd,kd), turmu(jd,kd)
      real vort(jd,kd),strain(jd,kd)
      real u(jd,kd),v(jd,kd),ug(jd,kd),vg(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real sn(jd,kd)
      real aj(jd,kd),bjmat(jd,kd),cj(jd,kd)
      real ak(jd,kd),ck(jd,kd),sjmat(jd,kd)

      ! local variables
      integer j,k,n,jp,kp,jm1,km1
      real uu,vv,fwd,bck
      real,allocatable :: up(:),um(:),vp(:),vm(:)

      allocate(up(mdim),um(mdim),vp(mdim),vm(mdim))

c***  first executable statement

      do k = 1,kmax
      do j = 1,jmax
          aj(j,k)=0.0
          bjmat(j,k)=0.0
          cj(j,k)=0.0
          ak(j,k)=0.0
          ck(j,k)=0.0
          sjmat(j,k) = 0.
      enddo
      enddo

c...lhs contribution from the convection term

      do k=2,kmax-1
        do j=1,jmax
          uu=xx(j,k)*(u(j,k)-ug(j,k))+xy(j,k)*(v(j,k)-vg(j,k))
          up(j) = 0.5*(uu+abs(uu))
          um(j) = 0.5*(uu-abs(uu))
        enddo

        do j = 2,jmax-1
          jp = j + 1
          jm1 = j - 1

          if(up(j).gt.1.0e-12) then
            fwd=1.0
          else
            fwd=0.0
          endif

          if(um(j).lt.-1.0e-12) then
            bck=1.0
          else
            bck=0.0
          endif

          sjmat(j,k) = sjmat(j,k) - up(j)*(itmc(j, k) - itmc(jm1,k)) 
     &                            - um(j)*(itmc(jp,k) - itmc(  j,k))
         
          aj(j,k) = aj(j,k) - up(j)!fwd*(up(jm1)+um(jm1))
          cj(j,k) = cj(j,k) + um(j)!bck*(up(jp)+um(jp))
          bjmat(j,k) = bjmat(j,k) + up(j)- um(j)
        enddo
      enddo

      do j=2,jmax-1
        do k=1,kmax
          vv = yx(j,k)*(u(j,k)-ug(j,k))+yy(j,k)*(v(j,k)-vg(j,k))
          vp(k) = 0.5*(vv+abs(vv))
          vm(k) = 0.5*(vv-abs(vv))
        enddo

        do k = 2,kmax-1
          kp = k + 1
          km1 = k - 1

          if(vp(k).gt.1.0e-12) then
            fwd=1.0
          else
            fwd=0.0
          endif

          if(vm(k).lt.-1.0e-12) then
            bck=1.0
          else
            bck=0.0
          endif

          sjmat(j,k) = sjmat(j,k) - vp(k)*(itmc(j, k)- itmc(j,km1)) 
     &                            - vm(k)*(itmc(j,kp)- itmc(j,  k))
         
          ak(j,k) = ak(j,k) - vp(k) !fwd*(vp(km1)+vm(km1))
          ck(j,k) = ck(j,k) + vm(k) !bck*(vp(kp)+vm(kp))
          bjmat(j,k) = bjmat(j,k) + vp(k) - vm(k)
        enddo
      enddo
   
c...rhs and lhs contribution from the source term
      call sa_gammasource(itmc,rey_tc,fon_lim,
     &      rho,vmul,turmu,vort,strain,sn,
     &      aj,bjmat,cj,ak,ck,sjmat,jd,kd)


c...rhs contribution from the diffusion term
      call sa_gammadiffus(itmc,rho,vmul,turmu,
     &     xx,xy,yx,yy,aj,bjmat,cj,ak,ck,sjmat,jd,kd)

      return
      end

c***********************************************************************
      subroutine sa_gammadiffus(itmc,rho,vmul,turmu,
     &           xx,xy,yx,yy,aj,bjmat,cj,ak,ck,sjmat,jd,kd)
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      integer jd,kd
      real itmc(jd,kd),rho(jd,kd),vmul(jd,kd),turmu(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real aj(jd,kd),bjmat(jd,kd),cj(jd,kd)
      real ak(jd,kd),ck(jd,kd),sjmat(jd,kd)
c***********************************************************************
      ! local variables
      integer j,k,j1,k1
      real,allocatable :: chp(:,:),dnuhp(:,:)
      real,allocatable :: dmxh(:,:),dmyh(:,:)

      real rei,vnulh,vnuh,dxp,dxm,c2,dcp,dcm,ax,cx,dyp,dym,ay,cy
      real sigmar

      allocate(chp(jd,kd),dnuhp(jd,kd))
      allocate(dmxh(jd,kd),dmyh(jd,kd))

c***  first executable statement
      sigmar=1.0

      rei = 1./rey
c     compute j direction differences.

c     compute half-point co-efficients
      do k=1,kmax
      do j=1,jmax-1
        dmxh(j,k) = 0.5*(xx(j,k)+xx(j+1,k))
        dmyh(j,k) = 0.5*(xy(j,k)+xy(j+1,k))
      enddo
      enddo

c     j-direction diffusion for itmc-equation
      do k=1,kmax
      do j=1,jmax-1
        vnulh    = 0.5*(vmul(j,k)+vmul(j+1,k))
        vnuh     = 0.5*(turmu(j,k)+turmu(j+1,k))
        chp(j,k) = rei*(vnulh+vnuh/sigmar)/rho(j,k)
        dnuhp(j,k) =itmc(j+1,k)-itmc(j,k)
      enddo
      enddo

      do k=1,kmax
      do j=2,jmax-1
        dxp=dmxh(j,k)*xx(j,k)+dmyh(j,k)*xy(j,k)
        dxm=dmxh(j-1,k)*xx(j,k)+dmyh(j-1,k)*xy(j,k)

c       enforce positivity (as suggested by overflow)
        dcp    = dxp*(chp(j,k))
        dcm    = dxm*(chp(j-1,k))
        ax=0.0
        cx=0.0

        if(k.ne.kmax.and.k.ne.1) then
          ax       = max(dcm,0.0)
          cx       = max(dcp,0.0)
        endif

c       compute fluxes.
        sjmat(j,k)=sjmat(j,k)-ax*dnuhp(j-1,k)+cx*dnuhp(j,k)

c       jacobian terms
        aj(j,k) = aj(j,k) - ax
        cj(j,k) = cj(j,k) - cx
        bjmat(j,k) = bjmat(j,k)+ (ax + cx)

      enddo
      enddo

!..   Compute k direction differences.

c     compute half-point co-efficients
      do k=1,kmax-1
      do j=1,jmax
        dmxh(j,k) = 0.5*(yx(j,k)+yx(j,k+1))
        dmyh(j,k) = 0.5*(yy(j,k)+yy(j,k+1))
      enddo
      enddo

c     k-direction diffusion for itmc-equation
      do k=1,kmax-1
      do j=1,jmax
        vnulh    = 0.5*(vmul(j,k)+vmul(j,k+1))
        vnuh     = 0.5*(turmu(j,k)+turmu(j,k+1))
        chp(j,k) = rei*(vnulh+vnuh/sigmar)/rho(j,k)  
        dnuhp(j,k) = itmc(j,k+1)-itmc(j,k)
      enddo
      enddo

      do k=2,kmax-1
      do j=1,jmax

        dyp=dmxh(j,k)*yx(j,k)+dmyh(j,k)*yy(j,k)
        dym=dmxh(j,k-1)*yx(j,k)+dmyh(j,k-1)*yy(j,k)

c       enforce positivity (as suggested by overflow)
        dcp    = dyp*(chp(j,k))
        dcm    = dym*(chp(j,k-1))

        ay=0.0
        cy=0.0
        if(j.ne.1.and.j.ne.jmax) then
          ay       = max(dcm,0.0)
          cy       = max(dcp,0.0)
        endif

c       compute fluxes.
        sjmat(j,k)=sjmat(j,k)-ay*dnuhp(j,k-1)+cy*dnuhp(j,k)

c       jacobian terms
        ak(j,k) = ak(j,k) - ay
        ck(j,k) = ck(j,k) - cy
        bjmat(j,k) = bjmat(j,k)+ (ay + cy)

      enddo
      enddo

      return
      end

c***********************************************************************
      subroutine sa_gammasource(itmc,rey_tc,fon_lim,
     &      rho,vmul,turmu,vort,strain,sn,
     &      aj,bjmat,cj,ak,ck,sjmat,jd,kd)

! Note
! CBSL: true Jacobian of production doesn't satisfy the positivity 
! constraints. This results in instability in some 3-D cases. 
! To avoid the issue, the strategy employed in SA model is applied.
c***********************************************************************
        use params_global
c***********************************************************************
        implicit none
c***********************************************************************
        integer jd,kd
        real itmc(jd,kd),rey_tc(jd,kd),fon_lim(jd,kd) 
        real rho(jd,kd), vmul(jd,kd), turmu(jd,kd)
        real vort(jd,kd), strain(jd,kd), sn(jd,kd)
        real aj(jd,kd),bjmat(jd,kd),cj(jd,kd)
        real ak(jd,kd),ck(jd,kd),sjmat(jd,kd)
c***********************************************************************
        ! local variables
        integer j,k
        real f_length, ce2, ca2
        real re_v, r_t, f_turb
        real f_onset,f_onset1,f_onset2,f_onset3
        real pro, des, prod, dest, d_prod, d_dest
        real pro0,pro1,pro2,des0,des1,des2
        real fon_lim1, fon_lim2
        real pos1, pos2
        
c**   first executable statement
        f_length = 100.0; ce2 = 50.0; ca2 = 0.06  

        do k=2,kmax-1
        do j=2,jmax-1

          r_t  = turmu(j,k)/vmul(j,k)
          f_turb=exp(-1.0*(r_t/2.0)**4)
          re_v = rey*rho(j,k)*sn(j,k)*sn(j,k)*strain(j,k)/vmul(j,k)

          f_onset1 = re_v/(2.2*rey_tc(j,k))
          f_onset2 = min(f_onset1,2.0)
          f_onset3 = max(1.0 - (r_t/3.5)**3.0,0.0)
          !f_onset  = min(max(f_onset2 - f_onset3, 0.0),1.0)
          f_onset  = max(f_onset2 - f_onset3, 0.0)

       !======================================!
       !           True Jacobian              !
       !======================================!
          !.. rho is divided in both RHS and LHS
!          pro    = f_length*strain(j,k)*f_onset 
!          prod   = pro*itmc(j,k)*(1.0-itmc(j,k))
!          d_prod = pro*(1.0-2.0*itmc(j,k)) 
!
!          des    = ca2*vort(j,k)*f_turb
!          dest   = des*itmc(j,k)*(ce2*itmc(j,k)-1.0)
!          d_dest = des*(2.0*ce2*itmc(j,k)-1.0)
!          sjmat(j,k) = sjmat(j,k) +   prod -   dest
!          bjmat(j,k) = bjmat(j,k) - d_prod + d_dest

       !======================================!
       !          Enforce positivity          !
       !======================================!
          pro0   =  f_length*strain(j,k)*f_onset 
          pro1   =  pro0*(1.0-itmc(j,k))
          pro2   = -pro0
          prod   =  pro1*itmc(j,k)

          des0   = ca2*vort(j,k)*f_turb
          des1   = des0*(ce2*itmc(j,k)-1.0)   
          des2   = des0*ce2
          dest   = des1*itmc(j,k)

          pos1   = max(des1-pro1,0.0)
          pos2   = max((des2-pro2)*itmc(j,k),0.0)

          sjmat(j,k) = sjmat(j,k)  +  prod  -  dest
          bjmat(j,k) = bjmat(j,k)  +  pos1  +  pos2

          fon_lim1 = max(re_v/(2.2*1100.0)-1.0,0.0)
          fon_lim2 = min(fon_lim1,3.0)
          fon_lim(j,k) = fon_lim2

        enddo
        enddo

!		  deallocate(f_theta,f_on,onset)

        return
        end

c*************************************************************
        subroutine sa_gamma_bc(q,rho,u,v,ug,vg,xx,xy,yx,yy,im)
c*************************************************************
        use params_global
c*************************************************************
        implicit none
c*************************************************************
        real q(jmax,kmax,nq),rho(jmax,kmax)
        real u(jmax,kmax),v(jmax,kmax),ug(jmax,kmax),vg(jmax,kmax)
        real xx(jmax,kmax),xy(jmax,kmax),yx(jmax,kmax),yy(jmax,kmax)
        integer im

c..   local variables
        integer js,je,ks,ke,idir
        integer j,k,ib

        do ib=1,nbc_all(im)
          js = jbcs_all(ib,im)
          je = jbce_all(ib,im)
          ks = kbcs_all(ib,im)
          ke = kbce_all(ib,im)
          if(js.lt.0) js = jmax+js+1
          if(ks.lt.0) ks = kmax+ks+1
          if(je.lt.0) je = jmax+je+1
          if(ke.lt.0) ke = kmax+ke+1
          idir = ibdir_all(ib,im)

c.. outflow bc - crude implementation (shivaji)
        if ((ibtyp_all(ib,im).eq.53). or. (ibtyp_all(ib,im).eq.55)) then
          call sa_gammabc_outflow(q,js,je,ks,ke,idir)

c.. inviscid wind tunnel wall bc
          elseif (ibtyp_all(ib,im).eq.3) then
            call sa_gammabc_extpt(q,js,je,ks,ke,idir)

c.. wall bc at l = 1 (only interior portion of wall)
          elseif (ibtyp_all(ib,im).eq.4 .or. ibtyp_all(ib,im).eq.5) then
            call sa_gammabc_wall(q,js,je,ks,ke,idir)

c.. symmetric bc
          elseif (ibtyp_all(ib,im).eq.11) then
            call sa_gammabc_sym(q,js,je,ks,ke,idir)

c.. periodic bc
          elseif (ibtyp_all(ib,im).eq.22) then
            call sa_gammabc_periodic(q,js,je,ks,ke,idir)

c.. averaging bc for wake
          elseif (ibtyp_all(ib,im).eq.51) then
            call sa_gammabc_wake(q,js,je,ks,ke,idir)

c.. averaging bc for wake of O-grid
          elseif (ibtyp_all(ib,im).eq.52) then
            call sa_gammabc_wake_ogrid(q,js,je,ks,ke,idir)

c.. freesream bc
          elseif (ibtyp_all(ib,im).eq.47) then
            call sa_gammabc_out(q,rho,u,v,ug,vg,xx,xy,yx,yy,js,je,ks,ke,idir)

          endif
        enddo

        return
        end

c*************************************************************
      subroutine sa_gammabc_outflow(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,nv-1) = q(j1,k,nv-1)
          !q(j,k,nv) = q(j1,k,nv)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,nv-1) = q(j,k1,nv-1)
          !q(j,k,nv) = q(j,k1,nv)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammabc_wall(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables
      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,nv-1) =  q(j1,k,nv-1)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,nv-1) =  q(j,k1,nv-1)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammabc_extpt(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,nv-1) = q(j1,k,nv-1)
          !q(j,k,nv) = q(j1,k,nv)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,nv-1) = q(j,k1,nv-1)
          !q(j,k,nv) = q(j,k1,nv)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammabc_sym(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,nv-1) = q(j1,k,nv-1)
          !q(j,k,nv) = q(j1,k,nv)
        enddo
      elseif(iadir.eq.2) then
        k = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,nv-1) = q(j,k1,nv-1)
          !q(j,k,nv) = q(j,k1,nv)
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammabc_periodic(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jc,jj,jj1,kc,kk,kk1,iadd,iadir

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(idir.eq.1) then
        jj  = je - js + 1
        do j = js,je
           jj1 = j - js
           jc = jmax - 2*jj + jj1
 
           do k = ks,ke
             q(j,k,nv-1) = q(jc,k,nv-1)
             !q(j,k,nv) = q(jc,k,nv)
           enddo
        enddo

      elseif(idir.eq.-1) then
        jj  = je - js + 1
        do j = js,je
           jj1 = je - j
           jc = 1 + 2*jj - jj1
 
           do k = ks,ke
             q(j,k,nv-1) = q(jc,k,nv-1)
             !q(j,k,nv) = q(jc,k,nv)
           enddo
        enddo

      elseif(idir.eq.2) then
        kk  = ke - ks + 1
        do k = ks,ke
           kk1 = k - ks
           kc = kmax - 2*kk + kk1
 
           do j = js,je
             q(j,k,nv-1) = q(j,kc,nv-1)
             !q(j,k,nv) = q(j,kc,nv)
           enddo
        enddo

      elseif(idir.eq.-2) then
        kk  = ke - ks + 1
        do k = ks,ke
           kk1 = ke - k
           kc = 1 + 2*kk - kk1 
 
           do j = js,je
             q(j,k,nv-1) = q(j,kc,nv-1)
             !q(j,k,nv) = q(j,kc,nv)
           enddo
        enddo

      endif

      return
      end

c*************************************************************
      subroutine sa_gammabc_wake(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jj,j1,k1,iadd,iadir
      real qav1,qav2

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        print*,'idir = ',idir,' is not implemented in gammathetabc_wake'
      elseif(iadir.eq.2) then
        k  = ks
        k1 = k + iadd
        do j=js,je
          jj = jmax - j + 1
          qav1 = 0.5*(q(j,k1,nv-1)+q(jj,k1,nv-1))
          !qav2 = 0.5*(q(j,k1,nv)+q(jj,k1,nv))
          q(j,k,nv-1)  = qav1
          !q(j,k,nv)  = qav2
          q(jj,k,nv-1) = qav1
          !q(jj,k,nv) = qav2
        enddo
      endif

      return
      end

c*************************************************************
      subroutine sa_gammabc_wake_ogrid(q,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,jj,j1,iadd,iadir
      real qav1,qav2

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j  = js
        j1 = j + iadd
        jj = jmax - j1 + 1
        do k=ks,ke
          qav1 = 0.5*(q(j1,k,nv-1)+q(jj,k,nv-1))
          !qav2 = 0.5*(q(j1,k,nv)+q(jj,k,nv))
          q(j,k,nv-1) = qav1
          !q(j,k,nv) = qav2
        enddo
      elseif(iadir.eq.2) then
      print*,'idir = ',idir,' is not implemented in 
     c                              gammathetabc_wake_ogrid'
      endif

      return
      end

c*************************************************************
      subroutine sa_gammabc_out(q,rho,u,v,ug,vg,xx,xy,yx,yy,js,je,ks,ke,idir)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      real q(jmax,kmax,nq),rho(jmax,kmax),u(jmax,kmax),v(jmax,kmax)
      real ug(jmax,kmax),vg(jmax,kmax)
      real xx(jmax,kmax),xy(jmax,kmax),yx(jmax,kmax),yy(jmax,kmax)
      integer js,je,ks,ke,idir

c.. local variables

      integer j,k,j1,k1,iadd,iadir
      real uu

      iadd = sign(1,idir)
      iadir = abs(idir)

      if(iadir.eq.1) then
        j  = js
        j1 = j + iadd
        do k=ks,ke
          q(j,k,nv-1)=itmcinf!*rho(j,k)
          !q(j,k,nv)=retinf!*rho(j,k)
          uu=   (u(j,k)-ug(j,k)+u(j1,k)-ug(j1,k))*xx(j,k)
          uu=uu+(v(j,k)-vg(j,k)+v(j1,k)-vg(j1,k))*xy(j,k)
          uu=uu*iadd
          if(uu.lt.0.) then
            q(j,k,nv-1)=q(j1,k,nv-1)
            !q(j,k,nv)=q(j1,k,nv)
          endif
        enddo
      elseif(iadir.eq.2) then
        k  = ks
        k1 = k + iadd
        do j=js,je
          q(j,k,nv-1)=itmcinf!*rho(j,k)
          !q(j,k,nv)=retinf!*rho(j,k)
          uu=   (u(j,k)-ug(j,k)+u(j,k1)-ug(j,k1))*yx(j,k)
          uu=uu+(v(j,k)-vg(j,k)+v(j,k1)-vg(j,k1))*yy(j,k)
          uu=uu*iadd
          if(uu.lt.0.) then
            q(j,k,nv-1)=q(j,k1,nv-1)
            !q(j,k,nv)=q(j,k1,nv)
          endif
        enddo
      endif

      return
      end

c*************************************************************
