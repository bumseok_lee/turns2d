c created by James Lankford June 30, 2014
c GRID KINEMATIC
c***********************************************************************
      subroutine grid_kine(x,y,xg,yg,xt2,yt2,xx,xy,ug,yx,yy,vg,
     >                 yx0,yy0,yt0,jd,kd,ii,init)
c
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      integer jd,kd,ii
      real x(jd,kd), y(jd,kd)
      real xg(jd,kd),yg(jd,kd)
      real xt2(jd,kd),yt2(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real ug(jd,kd), vg(jd,kd)
      real yx0(jd), yy0(jd), yt0(jd)
      logical init

      ! local variables
      integer j, k
      real xnew, ynew, y_tran, x_tran
      real omega_t, theta
c***********************************************************************
c store old metrics at grid surface
      do j = 1,jmax
        yx0(j) = yx(j,1)
        yy0(j) = yy(j,1)
        yt0(j) = -ug(j,1)*yx(j,1)-vg(j,1)*yy(j,1)
      enddo

      do j = 1,jmax
        do k = 1,kmax
          ug(j,k) = 0.0
          vg(j,k) = 0.0
        enddo
      enddo

      ! define the variable pi
      pi = 4.0*atan(1.0)

c define variables for kinematics
      omega_t = rf * totime

c AVIAN-like Kinematics
      if (kine_type .eq. 1) then

        ! calculate pitch angle of blade to output in terminal (in degrees)
        theta_tot = ((theta_knot * pi/180.0) + (theta_amp*pi/180.0) *
     &          sin(omega_t)) * (180.0/pi)    

        ! define theta to use in calculation of new x and y values
        ! if statement to prevent code from shift grid if intiated from a restart
        theta = (theta_amp*pi/180.0) * sin(omega_t)
        y_tran = plunge_amp * ( sin(omega_t + phase*pi/180.0) - 1.0 )     
        

        !update the grid and recalculate the grid velocities
        do k = 1,kmax
          do j = 1,jmax
            xnew = cos(theta)*(xg(j,k)-x_rot)
     &             + -sin(theta)*(yg(j,k)-y_rot)
     &             + x_rot

            ynew = sin(theta)*(xg(j,k)-x_rot)
     &             + cos(theta)*(yg(j,k)-y_rot)
     &             + y_rot
     &             + y_tran

            if(ntac.eq.1.or.istep.eq.1) then
              ug(j,k)=(xnew-x(j,k))/dt
              vg(j,k)=(ynew-y(j,k))/dt
            else
              ug(j,k)=(1.5*xnew-2*x(j,k)+0.5*xt2(j,k))/dt
              vg(j,k)=(1.5*ynew-2*y(j,k)+0.5*yt2(j,k))/dt
            endif
           
            ! store old x and y values
            xt2(j,k) = x(j,k)
            yt2(j,k) = y(j,k)

            ! update x and y
            x(j,k) = xnew
            y(j,k) = ynew
          enddo
        enddo

c INSECT-like Kinematics
      elseif (kine_type .eq. 2) then

        ! calculate pitch angle of blade to output in terminal (in degrees)
        theta_tot = ((theta_knot * pi/180.0) + (theta_amp*pi/180.0) *
     &          sin(omega_t)) * (180.0/pi)    

        ! define theta to use in calculation of new x and y values
        ! if statement to prevent code from shift grid if intiated from a restart
        theta = (theta_amp*pi/180.0) * sin(omega_t)
        x_tran = plunge_amp * ( sin(omega_t + phase*pi/180.0) - 1.0 )     
        

        !update the grid and recalculate the grid velocities
        do k = 1,kmax
          do j = 1,jmax
            xnew = cos(theta)*(xg(j,k)-x_rot)
     &             + -sin(theta)*(yg(j,k)-y_rot)
     &             + x_rot
     &             + x_tran

            ynew = sin(theta)*(xg(j,k)-x_rot)
     &             + cos(theta)*(yg(j,k)-y_rot)
     &             + y_rot

            if(ntac.eq.1.or.istep.eq.1) then
              ug(j,k)=(xnew-x(j,k))/dt
              vg(j,k)=(ynew-y(j,k))/dt
            else
              ug(j,k)=(1.5*xnew-2*x(j,k)+0.5*xt2(j,k))/dt
              vg(j,k)=(1.5*ynew-2*y(j,k)+0.5*yt2(j,k))/dt
            endif
           
            ! store old x and y values
            xt2(j,k) = x(j,k)
            yt2(j,k) = y(j,k)

            ! update x and y
            x(j,k) = xnew
            y(j,k) = ynew
          enddo
        enddo

      endif

 ! define grid velocities if solution is initializing
      if (init) then
        ug(j,k) = 0.0
        vg(j,k) = 0.0 
      endif

      end subroutine grid_kine
c***********************************************************************


C created by asitav April 30, 2009
c***********************************************************************
      subroutine pitch(x,y,xg,yg,xt2,yt2,xx,xy,ug,yx,yy,vg,
     >                 yx0,yy0,yt0,jd,kd,ii,init)
c
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      integer jd,kd,ii
      real x(jd,kd), y(jd,kd)
      real xg(jd,kd),yg(jd,kd)
      real xt2(jd,kd),yt2(jd,kd)
      real xx(jd,kd), xy(jd,kd), yx(jd,kd), yy(jd,kd)
      real ug(jd,kd), vg(jd,kd)
      real yx0(jd), yy0(jd), yt0(jd)
      logical init

      ! local variables
      real xac,yac,theta,sn,cn,xnew,ynew
      integer j,k,n!,step
c***********************************

      pi = 4.0*atan(1.0)
c     
      xac=0.25
      yac=0.0
      
      !step = istep0-steadysteps

Ca      theta=theta0-angmax*cos(rf*step*dt+phase_pitch*pi/180.)

      theta=theta0-angmax*cos(rf*totime+phase_pitch*pi/180.)

      !test....
      !theta=theta0+angmax*sin(rf*totime+phase_pitch*pi/180.)
      if(init) theta = theta_init

      theta_col=theta

      sn = sin(theta*pi/180.)
      cn = cos(theta*pi/180.)

c..store old metrics at surface

      do 450 j = 1,jmax
        yx0(j) = yx(j,1)
        yy0(j) = yy(j,1)
        yt0(j) = -ug(j,1)*yx(j,1)-vg(j,1)*yy(j,1)
  450 continue
c
C**************************************************************
C
C pitches Xg,Yg by absolute pitch angle to Xnew,Ynew; 
C X,Y are previously stored grids resulting from such pitching of 
C previous Xg,Yg; Xg,Yg changes only due to rigid slat motion
C
C**************************************************************

      do k = 1,kmax
       do j = 1,jmax
        xnew=(xg(j,k)-xac)*cn+(yg(j,k)-yac)*sn+xac
        ynew=-(xg(j,k)-xac)*sn+(yg(j,k)-yac)*cn+yac

        if(ntac.eq.1.or.istep.eq.1) then
          ug(j,k)=(xnew-x(j,k))/dt
          vg(j,k)=(ynew-y(j,k))/dt
        else
          ug(j,k)=(1.5*xnew-2*x(j,k)+0.5*xt2(j,k))/dt
          vg(j,k)=(1.5*ynew-2*y(j,k)+0.5*yt2(j,k))/dt
        endif
 100   format (4f15.10)
        xt2(j,k) = x(j,k)
        yt2(j,k) = y(j,k)
        x(j,k)=xnew
        y(j,k)=ynew
       end do
      end do

C asitav (for steady case)
      if(init) then
       ug = 0.; vg = 0.
      end if
C

      return
      end
c***********************************************************************
C created by asitav April 30, 2009
      subroutine rigid_slat_twodof(xg,yg,jd,kd)
c     moves the 'base slat xg,yg' rigidly
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      integer jd,kd
      real xg(jd,kd),yg(jd,kd)

      !Local variables
      !---------------
      integer :: j,k
      real    :: dx,dy,dtheta,x0,y0,deltax0,deltay0,dx0,dy0,xc,yc
      real    :: xnew,ynew

      pi = 4.*atan(1.)
      jtail2 = jmax-jtail1+1
      x0 = slatx0 !0.5/24.
      y0 = slaty0 !1.0/24.
      deltax0 = slatdx0 !0.45/24.!main element 24"
      deltay0 = slatdy0 !0.45/24.

      dtheta=-slat_angmax*pi/180.
     >        *(cos(rf_slat*(istep)*dt+ phase_slat*pi/180)
     >         -cos(rf_slat*(istep-1)*dt+ phase_slat*pi/180))
      theta_slat=theta_slat-dtheta*180./pi

      !translate (linear translation)
      !------------------------------
      dx0 = deltax0
     >      *(cos(rf_slat*(istep)*dt+ phase_slat*pi/180)
     >       -cos(rf_slat*(istep-1)*dt+ phase_slat*pi/180))

      dy0 = deltay0
     >      *(cos(rf_slat*(istep)*dt+ phase_slat*pi/180)
     >       -cos(rf_slat*(istep-1)*dt+ phase_slat*pi/180))
      xg = xg + dx0
      yg = yg + dy0
      !point of rotation also translates
      xc = x0 + deltax0*cos(rf_slat*(istep)*dt+ phase_slat*pi/180)
      yc = y0 + deltay0*cos(rf_slat*(istep)*dt+ phase_slat*pi/180)

      !rotate
      !------
      do j=1,jd
       do k=1,kd
        xnew = xc + (xg(j,k)-xc)*cos(dtheta) - (yg(j,k)-yc)*sin(dtheta)
        ynew = yc + (xg(j,k)-xc)*sin(dtheta) + (yg(j,k)-yc)*cos(dtheta)
        
        xg(j,k) = xnew 
        yg(j,k) = ynew 
       end do
      end do
      
      end !rigid_slat_twodof

c***********************************************************************
C created by asitav April 30, 2009
      subroutine rigid_slat_twodof_nh(xg,yg,jd,kd)
c     moves the 'base slat xg,yg' rigidly
c***********************************************************************
      use params_global
c***********************************************************************
      implicit none
c***********************************************************************
      integer jd,kd
      real xg(jd,kd),yg(jd,kd)

      !Local variables
      !---------------
      integer :: j,k,ihar
      real    :: dxc,dyc,dtheta,x0,y0,deltax0,deltay0,dx0,dy0,xc,yc
      real    :: xnew,ynew,psi_rot,psi_rot_old

      pi = 4.*atan(1.)
      jtail2 = jmax-jtail1+1
      x0 = slatx0 !0.5/24.
      y0 = slaty0 !1.0/24.
      !deltax0 = slatdx0 !0.45/24.!main element 24"
      !deltay0 = slatdy0 !0.45/24.

!      dtheta=-slat_angmax*pi/180.
!     >        *(cos(rf_slat*(istep)*dt+ phase_slat*pi/180)
!     >         -cos(rf_slat*(istep-1)*dt+ phase_slat*pi/180))
!      theta_slat=theta_slat-dtheta*180./pi

      psi_rot = rf_slat*totime
      psi_rot_old = rf_slat*(totime-dt)

      dtheta = 0.
      do ihar=1,nharmSlat
       dtheta=dtheta+ampSlat(ihar)*(cos(ihar*psi_rot-phiSlat(ihar))
     >                          -   cos(ihar*psi_rot_old-phiSlat(ihar)))
      enddo
      theta_slat=theta_slat+dtheta!*180./pi
      write(6,'(A,F12.5)') "Slat angle ", theta_slat*180./pi

      !translate (linear translation)
      !------------------------------
!      dx0 = deltax0
!     >      *(cos(rf_slat*(istep)*dt+ phase_slat*pi/180)
!     >       -cos(rf_slat*(istep-1)*dt+ phase_slat*pi/180))
!
!      dy0 = deltay0
!     >      *(cos(rf_slat*(istep)*dt+ phase_slat*pi/180)
!     >       -cos(rf_slat*(istep-1)*dt+ phase_slat*pi/180))

      dx0 = 0.; dy0 = 0.
      do ihar=1,nharmSlat
       dx0 = dx0 + slatdx0_nh(ihar)*(cos(ihar*psi_rot-phiSlat(ihar))
     >                          - cos(ihar*psi_rot_old-phiSlat(ihar)))
       dy0 = dy0 + slatdy0_nh(ihar)*(cos(ihar*psi_rot-phiSlat(ihar))
     >                          - cos(ihar*psi_rot_old-phiSlat(ihar)))
      end do

      xg = xg + dx0
      yg = yg + dy0

      !point of rotation also translates
!      xc = x0 + deltax0*cos(rf_slat*(istep)*dt+ phase_slat*pi/180)
!      yc = y0 + deltay0*cos(rf_slat*(istep)*dt+ phase_slat*pi/180)

      dxc = 0; dyc = 0.
      do ihar=1,nharmSlat
       dxc = dxc + slatdx0_nh(ihar)*cos(ihar*psi_rot-phiSlat(ihar))
       dyc = dyc + slatdy0_nh(ihar)*cos(ihar*psi_rot-phiSlat(ihar))
      end do
      xc = x0 + dxc
      yc = y0 + dyc

      !rotate
      !------
      do j=1,jd
       do k=1,kd
        xnew = xc + (xg(j,k)-xc)*cos(dtheta) - (yg(j,k)-yc)*sin(dtheta)
        ynew = yc + (xg(j,k)-xc)*sin(dtheta) + (yg(j,k)-yc)*cos(dtheta)
        
        xg(j,k) = xnew 
        yg(j,k) = ynew 
       end do
      end do
      
      end !rigid_slat_twodof_nh
