!********************************************************************
module octree
  implicit none
!********************************************************************

  integer :: lvlmin,lvlmax
  integer,private :: nbox
  real,pointer :: xs(:,:),xe(:,:),scale(:,:)
  logical,pointer :: empty_flag(:,:,:)
  integer,pointer :: istart(:,:,:,:),iend(:,:,:,:)

  integer,allocatable,private :: check_alloc(:)

  private :: init_octree,interleave
contains

!********************************************************************
  subroutine init_octree(ngrids,llmin,llmax)
   
    integer :: ngrids,llmin,llmax
  
    lvlmin = llmin
    lvlmax = llmax
    nbox = 2**(2*lvlmax)
   
    nullify(xs,xe,scale,empty_flag,istart,iend)
    if(allocated(check_alloc)) deallocate(check_alloc)

    allocate(xs(2,ngrids),xe(2,ngrids),scale(2,ngrids))
    allocate(empty_flag(lvlmax,nbox,ngrids))
    allocate(istart(lvlmax,nbox,2,ngrids),iend(lvlmax,nbox,2,ngrids))

    allocate(check_alloc(1))
  
  end subroutine init_octree

!********************************************************************
  subroutine make_octree(x,y,jmax,kmax,ngrids,llmin,llmax,init)

!********************************************************************

    integer,optional :: init
    integer :: ngrids,llmin,llmax
    integer :: jmax(:),kmax(:)
    real    :: x(:,:,:),y(:,:,:)

!.. local variables
    integer :: j,k,n
    integer :: lvl,nout
    real    :: eps_x,eps_y
    real    :: xbar(2)

!...allocate variables under user request

    if(present(init)) then
      if (init.eq.1) call init_octree(ngrids,llmin,llmax)
    endif

!...allocate variables if not initiated yet

    if (.not.allocated(check_alloc)) call init_octree(ngrids,llmin,llmax)

    empty_flag = .true.
    istart = 1
    iend = 1

!... scaling

    do n = 1,ngrids
      xs(1,n) = minval(x(:,:,n))
      xe(1,n) = maxval(x(:,:,n))

      xs(2,n) = minval(y(:,:,n))
      xe(2,n) = maxval(y(:,:,n))

      eps_x=(xe(1,n)-xs(1,n))*0.0001
      eps_y=(xe(2,n)-xs(2,n))*0.0001
      
      xs(1,n) = xs(1,n) - eps_x
      xs(2,n) = xs(2,n) - eps_y
      xe(1,n) = xe(1,n) + eps_x
      xe(2,n) = xe(2,n) + eps_y

      scale(1,n) = xe(1,n) - xs(1,n)
      scale(2,n) = xe(2,n) - xs(2,n)

      do lvl = lvlmin,lvlmax
        do j=1,jmax(n)
        do k=1,kmax(n)
          xbar(1) = (x(j,k,n)-xs(1,n))/scale(1,n)
          xbar(2) = (y(j,k,n)-xs(2,n))/scale(2,n)
          call boxindex(xbar,lvl,nout,2)
          if (empty_flag(lvl,nout+1,n)) then
            istart(lvl,nout+1,1,n) = j 
            istart(lvl,nout+1,2,n) = k 
          endif
          empty_flag(lvl,nout+1,n) = .false.
          iend(lvl,nout+1,1,n) = j 
          iend(lvl,nout+1,2,n) = k 
        enddo
        enddo
      enddo
    enddo

  end subroutine make_octree

!*********************************************************************
  subroutine boxindex(x,l,n,d)
! finding the box index given a point
!*********************************************************************
    integer :: n,l,d
    real :: x(d)

!.. local variables
    integer :: dim
    integer :: ncoord(d)

    do dim = 1,d
      ncoord(dim) = (ishft(1,l)*x(dim))
    enddo
    call interleave(ncoord,n,d)

    end subroutine boxindex

!*********************************************************************
  subroutine interleave(n,nout,d)
! subroutine for interleaving
!*********************************************************************
    integer :: nout,d
    integer :: n(d)

!.. local variables
    integer :: ntemp(d)
    integer :: j,dim,count,bit

    ntemp = n
    nout = 0
    count = 0

    do while (maxval(ntemp).ne.0)
      do dim = d,1,-1
        bit = ntemp(dim) - 2*ishft(ntemp(dim),-1)
        nout = nout + ishft(bit,count)
        count = count+1
        ntemp(dim) = ishft(ntemp(dim),-1)
      enddo
    enddo

  end subroutine interleave

end module octree

!********************************************************************
