
      module neural_network
      use params_global
      implicit none
     
      public :: neural_forward

      integer,private :: nHidden, nWeight, nLayers, nNeurons
      real,public :: beta_nn
      real,allocatable,public :: beta_ml(:,:)

      integer,allocatable,private :: check_alloc(:)
      real,allocatable,private :: w_low(:), weights(:,:,:)
      integer,allocatable,private :: num_nodes(:), num_inputs(:)


      contains
!***********************************************************************
      subroutine init_neural_network
!***********************************************************************
      integer i,k,j,l

      if(.not.allocated(check_alloc)) then
        allocate(check_alloc(1))

        ! Set variables
        nHidden  = 3
        nLayers  = nHidden + 2
        nNeurons = 20
        nWeight = (4+1)*nNeurons + (nNeurons*nNeurons)*2 + nNeurons
  
        allocate(num_nodes(nLayers),num_inputs(nLayers))
        allocate(weights(nLayers,nNeurons,nNeurons))
        allocate(beta_ml(jmax,kmax))

        weights = 0.0

        ! Hard coding for now because there are only 5 layers
        num_nodes(1) = nNeurons ! not use this value
        num_nodes(2) = nNeurons
        num_nodes(3) = nNeurons
        num_nodes(4) = nNeurons
        num_nodes(5) = 1
  
        num_inputs(1) = 4+1
        num_inputs(2) = nNeurons
        num_inputs(3) = nNeurons
        num_inputs(4) = nNeurons
        num_inputs(5) = nNeurons ! not use this value
  
  
        allocate(w_low(nWeight))
  
        ! Read Jon' weights
        open(unit=21,file='beta_fiml.dat',status='old',form='formatted')
        do j=1,nWeight
          read(21,*) w_low(j)
        enddo
        close(21)
  
        ! make array structure
        i=0
        do j=2,nLayers
        do k=1,num_inputs(j-1)
        do l=1,num_nodes(j)
          i=i+1
          weights(j,k,l) = w_low(i)
        enddo
        enddo
        enddo
  
      endif

      end subroutine init_neural_network

!***********************************************************************
      subroutine neural_forward(prod,dest,chi,vort_m,strain_m,turmu_m,&
                jd,kd)
!***********************************************************************
      integer jd,kd
      real prod,dest,chi
      real vort_m,strain_m,turmu_m
!***********************************************************************
      ! local variables
      integer j,k,l
      integer iLayer, iNode, iInput
      real delta, tau_w, f1, f2, f3, f4
      real,allocatable :: ai(:,:), inputs_nn(:,:)
      logical filter_shield, filter_point
      real l1,l2,l3,l4,inv_l1,inv_l2,inv_l3,inv_l4

      allocate(ai(nLayers,nNeurons),inputs_nn(nLayers,nNeurons))
      ai=0.0; inputs_nn=0.0; 


! CBSL
! Jon calculated local tau_w using turbulent flat plate equation. Also, he didn't include 1/2
! This should be modified later with actual local tau_w and new weights
! Need to check non-dimensionalizations of SU2 solver: rinf, fsmach**2, turmu_m     
      tau_w = 0.027/rey**(1.0/7.0)*rinf*fsmach**2
      delta = turmu_m*strain_m/(1.5*tau_w)/rey

      f1 = prod/dest
      f2 = chi
      f3 = delta
      f4 = strain_m/vort_m

      ! Box-Cox scaling (L2472): harcoded as Jon did in his code 
      ! For now, other scalings are not considered
      filter_shield = .true.
      l1 = -0.09146872915503056
      l2 = -0.049611844047549786
      l3 = -0.3085491501297445
      l4 = 0.3506666390043312
      inv_l1 = 1.0/l1
      inv_l2 = 1.0/l2
      inv_l3 = 1.0/l3
      inv_l4 = 1.0/l4
      
      filter_point  = .false.
      if ( (f3.lt.0.25) .or. (f4.gt.2.0) ) then
        filter_point = .true. !True means don't this point JRH 07182019
      endif
       
      ! Apply Filter (L2527)
      if ( .not.filter_point )  then 
      ! Box-Cox scaling (L2637)
        f1 = (f1**l1-1.0)*inv_l1;
        f2 = (f2**l2-1.0)*inv_l2;
        f3 = (f3**l3-1.0)*inv_l3;
        f4 = (f4**l4-1.0)*inv_l4;
  
        ! set initial values (L2656)
        ai(1,1) = 1.0
        ai(1,2) = f1
        ai(1,3) = f2
        ai(1,4) = f3
        ai(1,5) = f4
  
        inputs_nn(1,1) = 1.0
        inputs_nn(1,2) = f1
        inputs_nn(1,3) = f2
        inputs_nn(1,4) = f3
        inputs_nn(1,5) = f4
  
  
        ! Forward Propagation (L2667)
        do iLayer = 2,nLayers
          do iNode = 1,num_nodes(iLayer)
            ai(iLayer,iNode) = 0.0
  
            ! Apply Weights to Inputs
            do iInput = 1, num_inputs(iLayer-1)
              ai(iLayer,iNode) = ai(iLayer,iNode) + &
              weights(iLayer,iInput,iNode)*inputs_nn(iLayer-1,iInput)
            enddo
  
            if (iLayer.lt.nLayers) then
              ! tanh Activation Function
              !inputs_nn(iLayer,iNode) = (exp(2.0*ai(iLayer,iNode))-1.0)/ &
              !                          (exp(2.0*ai(iLayer,iNode))+1.0)
              inputs_nn(iLayer,iNode) = tanh(ai(iLayer,iNode))
            else
              inputs_nn(iLayer,iNode) = ai(iLayer,iNode)
            endif
  
          enddo
  
          ! Bias node !CBSL: first node is overwitten now. This should be fixed later!
          if (iLayer.lt.nLayers) inputs_nn(iLayer,1) = 1.0;
  
        enddo
        beta_nn = inputs_nn(nLayers,1)+1.0
      
      else  ! does not pass filter?
        beta_nn = 1.0
      endif 

      end subroutine neural_forward


      end module neural_network
