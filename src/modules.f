c*******************************************************************
c..   global parameters module
c*******************************************************************
      module params_global

c..   pi

      real :: pi

c  parameters that define critical points for c-h mesh


      integer half,jle,jtail1,jtail2,jle_flatplate
      
c..parameters for space and time
c..dx1,dy1,dz1 always set to 1
c..h,hd calculated based on input in initia

      real :: dx1, dy1, dz1, h, hd !dels

c  parameters that define the flow
c..alf,fsmach,rey set by input
c..gamma,gm1,ggm1,pr,tinf set to fixed values in initia

      real ::  alf, gm1, gamma, ggm1, fsmach, pr, rey, tinf

c  parameters that define the freestream values, set in initia

      real ::  einf, htinf, pinf, rinf, uinf, vinf, ainf, vnuinf, 
     c         tkeinf, tomegainf, itmcinf, retinf, vmulinf,
     c         tuinf,flen_global,alpha_global

c  parameters that define equivalent sand grain roughness height and extent		
!		real    :: hsand
      integer :: j1_rough, j2_rough

c  parameters that define perturbation flow

      integer :: ipert,initpe
      real    :: xvor, yvor, rcore, vorgam

c  parameters that define grid spatial dimensions and time steps
c..jmax,kmax,nq,nv,nsteps set by input
c..istep0 is initial time step read in with q-file
c..jm,km calculated based on input in initia

      integer :: jm,jmax,km,kmax,nq,nv,nmv,istep0,nsteps

c  parameters for type of limiting and pressure bc
c..ilim set by input
c..ibcwp set to 1 in initia

      integer ::   ilim,ibcwp
c..parameters for LHS
      integer ::   ilhs,idual,idual_turb,iconstant_cfl
      real    ::   dtpseudo(100),cfltur,dualtime
c..parameters for preconditioning
      logical ::   iprecon
      real    ::   Mp
c  parameters used for grid coarsening
c..jint,kint set by input
      integer :: jint,kint

c  parameters used for spatial order of accuracy
c..irhsy set by input

      integer :: irhsy

c  parameters for describing rotor conditions
c..totime set by input
c..rf set by input
c..angmax set by input
c..dang is current angle of attack difference

      real ::  totime,rf,angmax,dang,phase_pitch

c  parameters for restart, storing out restart and writing residual
c..iread,nrest and npnorm set by input

      integer:: iread, nrest, npnorm

c  parameters for damping left-hand-side
c..epse set by input

      real :: epse

c  parameters for time 
c..dt,timeac,iunst,ntac,itnmax set by input
c..cnbr set to 1 in initia (not used)
c..istep is current time step in the run

      real    :: cnbr, dt, timeac
      integer :: iunst, ntac, itnmax, istep, itn

c  parameters for viscous flow
c..invisc,lamin set by input
c..iturb set by input
c..rmue set in initia
c..nturiter set in vmu_sa

      real    :: rmue
      logical :: invisc, lamin, use_sarc,irough,iloglaw
      integer :: iturb, itrans,ides
      integer :: nturiter

C..Parameter for boundary conditions
      real    :: jtail(6)
      integer :: nbc_all(6),ibtyp_all(25,6),ibdir_all(25,6)
      integer :: jbcs_all(25,6),kbcs_all(25,6)
      integer :: jbce_all(25,6),kbce_all(25,6)
      logical :: bodyflag(6)
		logical :: flatplate
      logical :: ogridairfoil

      real    :: thetan,thetao
      integer :: nmovie
      integer :: num_grids	
      real    :: angmaxle,phasestart,xitm,xism,angnewle
      real    :: angplung,iplung,ipp,plungk,plungalf
      external flush
      
      real    :: theta_col,theta0,theta_init
C..Asitav
      !slat parameters
      integer :: islat,nharmSlat
      real    :: theta_slat0,theta_slat,slat_angmax,phase_slat
      real    :: slatx0,slaty0,rf_slat,slatdx0,slatdy0
      real    :: ampSlat(5),phiSlat(5),slatdx0_nh(5),slatdy0_nh(5)
C pivot point variables
      real    :: slatxc,slatyc

C..Asitav TEF variables
      integer :: iteflap,nharmflap
      real    :: pxc,dela,rf_tef,theta_f0,theta_finit
      real    :: ampFlap(5),phiFlap(5)

C..Asitav VDLE variables
      integer :: ivdle,nharmvdle
      real    :: pxc_vdle,dela_vdle,rf_vdle,theta_vdle0,theta_vdleinit
      real    :: ampvdle(5),phivdle(5)

		real    :: x_cm

      ! dimension values
      integer :: jdim,kdim,mdim,isdim,iqdim,igrd,iadim

      ! Synthetic jet parameters

C asitav (if BackGround mesh exists: BG=.TRUE., usually 2nd mesh)
      !mainqc = .FALSE. = use effective quarter chord (default)
      !       = .TRUE. = use main element quarter chord 
      !mainch = .FALSE. = use effective chord (default)
      !       = .TRUE. = use main element chord (=1.0 )
      logical :: BG,onlyslat,mainqc,mainch
      integer :: J_s1,J_e1,J_s2,J_e2,ICHOICE,INTERN_FLO,O_GRID
      real    :: vna,vnb,ATIME
      real    :: T1a,T2a,T3a,T4a,T1b,T2b,T3b,T4b
      real    :: ak1a,ak2a,ak1b,ak2b
      real    :: f1a,f1b,f2a,f2b,angle_a,angle_b,angle_1,angle_2

C ..parameters to save a specific interval of solution files (Added by Lankford)
      integer :: nstart,nstop,ninterval

C ..parameters to use the new grid kinematic subroutine grid_kine (Added by Lankford)
      integer :: kine_type
      real    :: theta_tot, theta_knot, theta_amp, plunge_amp, phase
      real    :: x_rot, y_rot

C ..parameters to switch between hover and forward flight simulation
      integer :: flight_mode
      real    :: fmtip
      
      data flight_mode, fmtip /0,0.0/
      data kine_type, x_rot, y_rot /0,0.0,0.0/
      data theta_tot, theta_knot, theta_amp, plunge_amp, phase /0.0,0.0,0.0,0.0,0.0/
      data nstart, nstop, ninterval /0,-1,1/
      data dx1,dy1 / 2*1.0 /
      data gamma,pr,rmue,tinf /1.4,0.72,1.0,400./
      data ibcwp /1/
      data cnbr /1./
      data iread /0/
      data num_grids /1/
      data BG,onlyslat,mainqc,mainch /.FALSE.,.FALSE.,.FALSE.,.FALSE./
      data jmax,kmax,jtail1,half /109,36,19,0/
      data nq,nv,nsteps,nrest,npnorm /5,4,500,1000,25/
      data fsmach,alfa,rey,invisc,lamin,iturb,itrans,use_sarc,flatplate,irough,iloglaw,ides
     <     /0.16,0.0,3900000.,.true.,.true.,1,0,.true.,.false.,.false.,.false.,0/
      data ogridairfoil /.FALSE./
      data iunst,ntac,itnmax,dt,timeac /0,1,1,.1,1./
      data epse,irhsy,ilhs,ilim,totime,rf,angmax /0.01,-3,1,0,0.,0.,0./
      data jint,kint /1,1/
      data ipert,initpe,xvor,yvor,rcore,vorgam /0,1,-5.,-0.26,.05,0./
      data nmovie /50/
      data isin,phaseend,r1,pulseang,iod/0.,0.,0.,0.0,1/
      data ideform,xdef /0,0.0/
      data angmaxle,phasestart /0.0,0.0/
      data ipp,plungk,plungalf /0,.01,0./
      data alphameani,alpha0,pulseang2 /0.,0.,0./
      data idual,idual_turb,dualtime,cfltur,iconstant_cfl/0,0,1.0,1.0,0/
      data iprecon,Mp/.false.,1.0/ 
      data INTERN_FLO,O_GRID/0,0/
      data ICHOICE/100/
		data TUINF,flen_global,alpha_global/0.2,0.1,0.55/
		data x_cm/0.25/
		data vnuinf,vmulinf/0.1,1.0/
!		data hsand/0.002/
		data j1_rough,j2_rough/0,-1/

      namelist/inputs/ iread,o_grid,num_grids,bg,onlyslat,mainqc,mainch,ibcwp,
     & jmax,kmax,half,nsteps,nrest,npnorm,steadysteps,iteflap,ivdle,!jtail1, 
     & fsmach,alfa,rey,invisc,lamin,iturb,itrans,iloglaw,ides,use_sarc,iunst,tuinf,flatplate,
     & flen_global,alpha_global,ogridairfoil,
     & ntac,itnmax,dt,dtpseudo,cfltur,timeac,irough,
     & epse,irhsy,ilim,ilhs,idual,idual_turb,iconstant_cfl,iprecon,Mp,
     & totime,rf,angmax,jint,kint,
     & ipert,initpe,xvor,yvor,rcore,vorgam,
     & nmovie,
     & isin,phaseend,r1,pulseang,iod,ideform,xdef,
     & angmaxle,phasestart,
     & ipp,plungk,plungalf,
     & alpha0,pulseang2,
     & ichoice,j_s1,j_e1,j_s2,j_e2,ak1a,ak2a,ak1b,ak2b,
     & t1b,t2b,t1a,t2a,vna,vnb,angle_1,angle_2,
     & theta0,theta_init,islat,phase_pitch,x_cm,vnuinf,
!     & hsand,j1_rough,j2_rough,
     & j1_rough,j2_rough,
     & nstart, nstop, ninterval,
     & theta_knot, theta_amp, phase, plunge_amp,
     & x_rot, y_rot, kine_type, flight_mode, fmtip
      end module params_global

c**************************************************************************
c     module for the octree of background mesh
c**************************************************************************
      module bg_octree
      
      integer max_sor,mbox,lvl
      
      real, allocatable:: xs1(:),ys1(:),zs1(:)
      real, allocatable:: sboxs(:)
      real, allocatable:: xcls(:),ycls(:),zcls(:)
      integer, allocatable::lks(:,:),ncls(:),inxcbs(:,:)
      integer, allocatable::kns(:),nboxs(:),lboxs(:),nsups(:)
      integer, allocatable::pindex(:)
      
      end module bg_octree

