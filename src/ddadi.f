c*************************************************************
      subroutine lsolvej(a,b,c,s,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      integer jd,kd
      real a(jd,kd),b(jd,kd),c(jd,kd)
      real s(jd,kd)

c.. local variables

      integer j,k
      real bb

c***  first executable statement

      do k = 2,km
        bb       = 1./b(2,k)
        s(2,k)  = s(2,k)*bb
        c(2,k)  = c(2,k)*bb
      enddo

      do  j = 3,jm
      do  k = 2,km
       bb      = 1./(b(j,k) - a(j,k)*c(j-1,k))
       s(j,k)  = (s(j,k) - a(j,k)*s(j-1,k))*bb
       c(j,k)  = c(j,k)*bb
      enddo
      enddo

      do j = jm-1,2,-1
      do k = 2,km
        s(j,k)    = s(j,k) - c(j,k)*s(j+1,k)
      enddo
      enddo

      return
      end

c*************************************************************
      subroutine lsolvek(a,b,c,s,jd,kd)
c*************************************************************
      use params_global
c*************************************************************
      implicit none
c*************************************************************
      integer jd,kd
      real a(jd,kd),b(jd,kd),c(jd,kd)
      real s(jd,kd)

c.. local variables

      integer j,k
      real bb

c***  first executable statement

      do j = 2,jm
        bb       = 1./b(j,2)
        s(j,2)  = s(j,2)*bb
        c(j,2)  = c(j,2)*bb
      enddo

      do  k = 3,km
      do  j = 2,jm
        bb      = 1./(b(j,k) - a(j,k)*c(j,k-1))
        s(j,k)  = (s(j,k) - a(j,k)*s(j,k-1))*bb
        c(j,k)  = c(j,k)*bb
      enddo
      enddo

      do k = km-1,2,-1
      do j = 2,jm
         s(j,k)    = s(j,k) - c(j,k)*s(j,k+1)
      enddo
      enddo


      return
      end

