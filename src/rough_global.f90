
      module roughness
      use params_global
      implicit none

      public :: init_roughness

     
      real,allocatable,public :: hsand(:)
      integer,allocatable,private :: check_alloc(:)


      contains
!***********************************************************************
      subroutine init_roughness(x,jd,kd)
!***********************************************************************
      integer jd,kd
      real x(jd,kd)
!***********************************************************************
      integer nrough, i,j,k,l
      logical file_exists
      real,allocatable :: xs(:), xe(:), ks(:)

      if(.not.allocated(check_alloc)) then
        allocate(check_alloc(1))
        allocate(hsand(jd))

        inquire(file="rough.inp", exist=file_exists)

        if (file_exists) then
          open(unit=10,file='rough.inp',status='unknown')
          read(10,*); read(10,*); read(10,*)
          read(10,*) nrough
          allocate(xs(nrough),xe(nrough),ks(nrough))

          read(10,*); read(10,*); read(10,*)
          do i = 1,nrough
            read(10,*) xs(i),xe(i),ks(i)
          enddo
          close(10)
        else
          write(6,*) 'Warning! Roughness input not found!'
        endif       

        hsand= 0.0

        do i=1,nrough
          do j=1,jmax 
            if(x(j,1).ge.xs(i).and.x(j,1).le.xe(i)) then
            hsand(j)=ks(i)
            endif
          enddo
        enddo 
        print*, "Roughness heights are allocated"

        open(unit=10,file='height.dat',status='unknown')
        write(10,*) 'variables="x","ks"'
        do j=1,jmax
          write(10,*) x(j,1),hsand(j)
        enddo
        close(10)

      endif

      end subroutine init_roughness


      end module roughness
