C     ******************************************************************
C     Stripped off of 2d rigid_flap code: 

      subroutine rigid_vdle(x,y,jd,kd,init)
C     x->xg
C     y->yg
C
C***********************************************************************

      use params_global

      implicit none

      integer jd,kd
      real x(jd,kd), y(jd,kd)
      logical init

c ..   local variables

       integer j,k,ihar
       real cs,ss,xtmp,ytmp,xo,yo

C .....Asitav
       real theta_fnew,theta_fold
       real::f1,f2,f3,val,yplane,angle,angle0,theta,dist,xc,yc
       real ttef11,ttef12,ttef13,ttef14,ttef31,ttef32,ttef33,ttef34
       real::dtheta_f1,dtheta_f2,y1,y2,delx1,delx2,xt1,xt2,yt1,yt2,
     $       xsurf(jmax),ysurf(jmax),psi_rot!,psi_rot_old

       real,parameter::rmax=2.5, rmin=0.2

       real, save :: theta_prev

       pi = 4.0*atan(1.0)

       theta_fnew=theta_vdle0

       psi_rot = rf_vdle*totime

       do ihar=1,nharmvdle
          theta_fnew=theta_fnew+ampvdle(ihar)
     &         *cos(ihar*psi_rot-phivdle(ihar))
       enddo

       if (init) then
          dtheta_f2=theta_vdleinit !theta_fnew
          dtheta_f1=0.0D0
       else
          dtheta_f2=theta_fnew
          dtheta_f1=-theta_prev
       endif
       write(6,'(A,F12.5)') "VDLE angle ", theta_fnew*180./pi

       !do k = 1,kmax
C.... test
        xt1 = x(jtail1,1); yt1 = y(jtail1,1)
        xt2 = x(jtail2,1); yt2 = y(jtail2,1)
        do j=1,jmax
         xsurf(j) = x(j,1); ysurf(j) = y(j,1);
        end do

        xc = (pxc_vdle*(x(jtail1,1) - x(jle,1)) + x(jle,1))

C       locate hinge
C       ------------
       hinge: do j=jtail1,jtail2
        delx1 = x(j,1)-xc
        delx2 = x(j+1,1)-xc
        if( (delx1*delx2 < 0.0) .and. (j < (jtail1+jtail2)/2 ) ) then
         y1 = 0.5*(y(j,1)+y(j+1,1))
        else if( (delx1*delx2 < 0.0) .and. (j > (jtail1+jtail2)/2 ) ) then
         y2 = 0.5*(y(j,1)+y(j+1,1))
         exit hinge
        else
         cycle hinge
        end if
       end do hinge
c
c check jaina
c
        y1=(y1+y2)*0.5
        y2=y1

C     ------------
        f3 = 1.0

        do k = 1,kd
         do j = 1,jd
C........ Point of rotation
          if(j<(jtail1+jtail2)/2) then
           yc = y1
          else if(j>(jtail1+jtail2)/2) then
           yc = y2
          end if

          dist=sqrt((x(j,k)-xsurf(j))**2+(y(j,k)-ysurf(j))**2);
          if (j > jtail2) then
           dist=sqrt((x(j,k)-xsurf(jtail2))**2+(y(j,k)-ysurf(jtail2))**2);
          else if (j < jtail1) then
           dist=sqrt((x(j,k)-xsurf(jtail1))**2+(y(j,k)-ysurf(jtail1))**2);
          end if

          if (dist > rmax) then
           f1=0.0;
          else if (dist < rmin) then
           f1=1.0;
          else
           val=dist-rmin;
           val=val/(rmax-rmin)*pi;
           f1=(1+cos(val))*0.5;
          end if

          angle =atan2( (y(j,k)-yc), (x(j,k)-xc) );
          angle0=atan2( (y(jtail1,1)-yc), (x(jtail1,1)-xc) );
          angle = angle - angle0;

          if (abs(angle) > pi*0.5+dela_vdle) then
           f2=1.0
          elseif (abs(angle)<pi*0.5-dela_vdle)  then
           f2=0.0
          else
           val=abs(angle)-pi*0.5+dela_vdle
           val=pi*val/(2.0*dela_vdle)
           f2=1.-(1+cos(val))*0.5
          end if

          cs=cos( (dtheta_f1+dtheta_f2) *f3*f2*f1)
          ss=sin( (dtheta_f1+dtheta_f2) *f3*f2*f1)

C  .......The t-matrix for te flap
          ttef11 = cs
          ttef12 = 0.0
          ttef13 = ss
          ttef14 = xc*(1. - cs) - yc*ss

          ttef31 = -ss
          ttef32 = 0.0
          ttef33 = cs
          ttef34 = xc*ss + yc*(1-cs)
C  .......move grid
          xo=x(j,k) 
          yo=y(j,k) 
          x(j,k) = xo*ttef11+yo*ttef13 + ttef14
          y(j,k) = xo*ttef31+yo*ttef33 + ttef34

         end do
        end do


       theta_prev=theta_fnew
      end subroutine rigid_vdle
